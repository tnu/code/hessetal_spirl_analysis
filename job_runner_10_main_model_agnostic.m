function [] = job_runner_10_main_model_agnostic(EULER)
% [] = job_runner_10_main_model_agnostic(EULER)
%
% Run model-agnostic analysis of main data set.
%
% INPUT
%   EULER        binary           Binary indicator variable
%   j            integer          Integer for iteration of 100 fam sim runs
%
%   OPTIONAL:
%
% OUTPUT    
%   argout       type
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

%% setup path
saveDir = spirl_setup_paths(EULER);

%% load analysis specifications
options = load(fullfile(saveDir, 'results', ['options']));
rng(123, 'twister');
options.rng.settings = rng;

%% load pilot priors
pilot = load(fullfile(saveDir, 'results', 'pilots', ['pilot_priors']));

%% load model space for analysis of the main data set
main = load(fullfile(saveDir, 'results', 'main', ['model_space']));

%% load main data set
% (preprocessed data, checked for meeting analysis inclusion criteria)
tmp = load(fullfile(pwd, 'data', options.main.filename));
main.SPIRL = tmp.SPIRL;
main.logRT_mat = tmp.logRT_mat;
main.trials = tmp.trials;
main.traj = tmp.traj;

%% model-agnostic RT analysis

% create vectors needed as inputs for anova
main.phases.vec = pilot.phases.vec;

% matrix with avg logRT per phase for each subject (20 x 3)
main.phases.avg_logRT = NaN(options.main.nS,3);
for j=1:3
    main.phase(j).logRT_mat = main.logRT_mat(main.phases.vec==j,:);
    main.phases.avg_logRT(:,j) = mean(main.phase(j).logRT_mat, 1, 'omitnan')';
end

%% 1-way ANOVA on avg logRT over sub (factor: phase)
[main.anova.p, main.anova.tbl, main.anova.stats] = ...
    anova1(main.phases.avg_logRT,...
    {'stable', 'volatile', 'unpredictable'}... levels of factor phase
    )

%% save model space as struct
save_path = fullfile(saveDir, 'results', 'main', ['model_space']);
save(save_path, '-struct', 'main', '-v7.3');

disp('model space ready for fitting of the main data.')

end
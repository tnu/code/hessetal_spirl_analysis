function [] = job_runner_1_pilots_model_agnostic(EULER)
% [] = job_runner_1_pilots_model_agnostic(EULER)
%
% Runs model agnostic logRT analysis on pilot data set. Additionally, a
% struct containing the model space for the model-based analysis of the
% pilot data is created.
%
% INPUT
%   EULER        binary           Binary indicator variable
%
%   OPTIONAL:
%
% OUTPUT    
%   argout       type
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

%% setup path
saveDir = spirl_setup_paths(EULER);

%% analysis specifications
options = spirl_specs();

%% create results folder
spirl_create_results_dir(options);

%% load preprocessed data (checked for meeting analysis inclusion criteria)
pilot = load(fullfile(pwd, 'data', options.pilots.filename));

%% create input for anova

% vector indication phase
pilot.phases.vec = NaN(size(pilot.traj.block.code));
for i = 1:size(pilot.phases.vec,1)
    if ismember(pilot.traj.block.code(i), [1,6,8,13])
        pilot.phases.vec(i) = 1;
    elseif ismember(pilot.traj.block.code(i), [2,3,4,5,9,10,11,12])
        pilot.phases.vec(i) = 2;
    elseif ismember(pilot.traj.block.code(i), [7])
        pilot.phases.vec(i) = 3;
    end
end

% matrix with avg logRT per phase for each subject (20 x 3)
pilot.phases.avg_logRT = NaN(options.pilots.nS,3);
for j=1:3
    pilot.phase(j).logRT_mat = pilot.logRT_mat(pilot.phases.vec==j,:);
    pilot.phases.avg_logRT(:,j) = mean(pilot.phase(j).logRT_mat, 1, 'omitnan')';
end

%% 1-way ANOVA on avg logRT over sub (factor: phase)
[pilot.anova.p, pilot.anova.tbl, pilot.anova.stats] = ...
    anova1(pilot.phases.avg_logRT,...
    {'stable', 'volatile', 'unpredictable'}... levels of factor phase
    );

disp('model-agnostic analysis of the pilot data set completed.')

%% setup model space
pilot.ModSpace = spirl_setup_model_space();

% set avg logRT across pilot sub to prior mean of beta_0 in logRT GLM of
% response models
avg_logRT_pilots = mean(reshape(pilot.logRT_mat,160*20,1), 'omitnan');
for m = 1:size(pilot.ModSpace, 2)
    pilot.ModSpace(m).obs_config.beta0mu = avg_logRT_pilots;
    pilot.ModSpace(m).obs_config.priormus(2) = avg_logRT_pilots;
    if m == 7
        pilot.ModSpace(m).obs_config.beta1mu = avg_logRT_pilots;
        pilot.ModSpace(m).obs_config.priormus(3) = avg_logRT_pilots;
    end
end

%% save results, model space and options as struct

% options
save_path1 = fullfile(saveDir, 'results', ['options']);
save(save_path1, '-struct', 'options');

% pilot data
save_path = fullfile(saveDir, 'results', 'pilots', ['init_model_space']);
save(save_path, '-struct', 'pilot');

disp('model space ready for fitting of the pilot data.')

end
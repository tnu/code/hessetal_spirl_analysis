function [] = job_runner_revisions(EULER)
% [] = job_runner_analysis_for_reviews(EULER)
%
% Runs additional anylses and creates figures based on the feedback from
% reviews.
%
% INPUT
%   EULER        binary           Binary indicator variable
%
%   OPTIONAL:
%
% OUTPUT    
%   argout       type
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2024 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

%% setup path
saveDir = spirl_setup_paths(EULER);

%% load analysis specifications
options = load(fullfile(saveDir, 'results', ['options']));
rng(123, 'twister');
options.rng.settings = rng;

%% load final results
res = load(fullfile(saveDir, 'results', 'main', ['main_results']));

%% specify colors
options.col.idx = ones(1,length(res.main.trials));
for i = 1:length(res.main.trials)
    if ismember(res.main.traj.block.code(i),[1,6,8,13]) 
        options.col.idx(i) = 2;
    elseif ismember(res.main.traj.block.code(i),[7])
        options.col.idx(i) = 3;
    end
end
options.col.map = [options.col.tnub; options.col.wh; options.col.gry];

options.col.cb = [161,217,155; 65,171,93; 0,90,50]/(2^8); % green
    ...[247,104,161; 197,27,138; 122,1,119]/(2^8); % magenta
    ...[253,141,60; 230,85,13; 166,54,3]/(2^8); % red
    ...[253,190,133; 253,141,60; 217,71,1]/(2^8); % red
    ...[186,228,179; 116,196,118; 35,139,69]/(2^8); % green
% [150,150,150; 99,99,99; 37,37,37]/(2^8);
% [253,224,221; 250,159,181; 197,27,138]/(2^8);


%% get screensize & hardcode figure sizes
scrsz = get(0,'screenSize');
% hard code figure sizes
outerpos1 = [0.275*scrsz(3),0.4*scrsz(4),700,250];
outerpos2 = [0.275*scrsz(3),0.4*scrsz(4),400,400];

%% plot settings
% dock all figures
set(0,'DefaultFigureWindowStyle','docked')


%% collect prior pred dens for all models
for m = 1:7
    ep(m).mu1hat_sim = NaN(size(res.main.traj.u,1),options.main.nS);
    ep(m).mu2hat_sim = NaN(size(res.main.traj.u,1),options.main.nS);
    ep(m).mu3hat_sim = NaN(size(res.main.traj.u,1),options.main.nS);
    ep(m).sa1hat_sim = NaN(size(res.main.traj.u,1),options.main.nS);
    ep(m).sa2hat_sim = NaN(size(res.main.traj.u,1),options.main.nS);
    ep(m).sa3hat_sim = NaN(size(res.main.traj.u,1),options.main.nS);
    ep(m).ybin_sim = NaN(size(res.main.traj.u,1),options.main.nS);
    ep(m).logRT_sim = NaN(size(res.main.traj.u,1),options.main.nS);
    for n = 1:options.sim.indiv.nS
        ep(m).mu1hat_sim(:,n) = res.sim.sub(n,m).data.traj.muhat(:,1);
        ep(m).mu2hat_sim(:,n) = res.sim.sub(n,m).data.traj.muhat(:,2);
        ep(m).mu3hat_sim(:,n) = res.sim.sub(n,m).data.traj.muhat(:,3);
        ep(m).sa1hat_sim(:,n) = res.sim.sub(n,m).data.traj.sahat(:,1);
        ep(m).sa2hat_sim(:,n) = res.sim.sub(n,m).data.traj.sahat(:,2);
        ep(m).sa3hat_sim(:,n) = res.sim.sub(n,m).data.traj.sahat(:,3);
        ep(m).ybin_sim(:,n) = res.sim.sub(n,m).data.y(:,1);
        ep(m).logRT_sim(:,n) = res.sim.sub(n,m).data.y(:,2);
    end
end


%% FIG 5B: prior pred densities under empirical priors, mu1hat (M1)
m=1;

% mh1 point with max var
[~,mh1_maxidx] = max(var(ep(m).mu1hat_sim,[],2))

% mh1 points with median and min var
[~,mh1_minidx] = mink(var(ep(m).mu1hat_sim,[],2),res.main.trials(end));
mh1_minidx(81)
mh1_minidx(2)

ep(m).avg_mu1hat_sim = mean(ep(m).mu1hat_sim, 2, 'omitnan');
ep(m).avg_ybin_sim = mean(ep(m).ybin_sim, 2, 'omitnan');

% create figure
figure(...
    'OuterPosition', outerpos1,...
    'Name', 'M1 mu1hat (prior pred dens)');
% mu1hat
plot(ep(m).mu1hat_sim, 'color', [options.col.tnub 0.1])
hold on
plot(ep(m).avg_mu1hat_sim, 'color', options.col.tnub, 'LineWidth', 1.5)
plot(ep(m).avg_ybin_sim, 'color', options.col.tnuy, 'LineWidth', 1)
% probability trajectory
plot(res.main.traj.probs_green, 'Color', [0 0 0], 'LineWidth',1)
ylim([-0.1 1.1])
% inputs
plot(res.main.traj.u, '.', 'Color', 'k') % (1=green card, 0=yellow)
% vertical lines for max, median, min variance of simulated values
xline(mh1_maxidx, 'LineWidth', 1.5, 'Color', options.col.cb(1,:))
xline(mh1_minidx(81), 'LineWidth', 1.5, 'Color', options.col.cb(2,:))
xline(mh1_minidx(2), 'LineWidth', 1.5, 'Color', options.col.cb(3,:))
% color code different phases
ax = axis;
fill([res.main.trials, fliplr(res.main.trials)],...
    [ax(3)*ones(1,length(res.main.trials)), ax(4)*ones(1,length(res.main.trials))],...
    [options.col.idx, fliplr(options.col.idx)], 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
colormap(options.col.map)
hold off

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig5B_empirical_prior_pred_dens_mu1hat_M1_rev']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

%% Fig 5B summary metric m1
m=1;
bw5B = 0.02;

figure
histogram(ep(m).mu1hat_sim(mh1_maxidx,:), 'BinWidth', bw5B,...
    'FaceColor', options.col.cb(1,:), 'EdgeColor', options.col.cb(1,:))
hold on
histogram(ep(m).mu1hat_sim(mh1_minidx(81),:), 'BinWidth', bw5B,...
    'FaceColor', options.col.cb(2,:), 'EdgeColor', options.col.cb(2,:))
histogram(ep(m).mu1hat_sim(mh1_minidx(2),:), 'BinWidth', bw5B,...
    'FaceColor', options.col.cb(3,:), 'EdgeColor', options.col.cb(3,:))
xlim([0 1])
ylim([0 100])
ax = gca; ax.FontSize = 20;
legend('max var$(\hat{\mu}_{1})$','median var$(\hat{\mu}_{1})$','min var$(\hat{\mu}_{1})$',...
    'Interpreter','latex',...
    'Location', 'NorthEast',...
    'FontSize', 14)
%     'Position', [0.72 0.74 0.03 0.07])

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig5B_empirical_prior_pred_dens_mu1hat_M1_rev_hist']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;


%% FIG 5C: prior pred densities under empirical priors, log(yhat_rt) (M1)
m=1;
% mh1 point with max var
[~,rt_maxidx] = max(var(ep(m).logRT_sim,[],2))
% mh1 points with median and min var
[~,rt_minidx] = mink(var(ep(m).logRT_sim,[],2),res.main.trials(end));
rt_minidx(81)
rt_minidx(2)

ep(m).avg_logRT_sim = mean(ep(m).logRT_sim, 2, 'omitnan');

% create figure
figure(...
    'OuterPosition', outerpos1,...
    'Name', 'M1 log(yhat_rt) (prior pred dens)');
plot(ep(m).logRT_sim, 'color', [options.col.tnub 0.1])
hold on
plot(1:160, ones(160,1)*log(1700), '--k')
plot(1:160, ones(160,1)*log(100), '--k')
plot(ep(m).avg_logRT_sim, 'color', options.col.tnub, 'LineWidth', 1.5)
ylim([2 11])
% vertical lines for max, median, min variance of simulated values
xline(rt_maxidx, 'LineWidth', 1.5, 'Color', options.col.cb(1,:))
xline(rt_minidx(81), 'LineWidth', 1.5, 'Color', options.col.cb(2,:))
xline(rt_minidx(2), 'LineWidth', 1.5, 'Color', options.col.cb(3,:))
% color code different phases
ax = axis;
fill([res.main.trials, fliplr(res.main.trials)],...
    [ax(3)*ones(1,length(res.main.trials)), ax(4)*ones(1,length(res.main.trials))],...
    [options.col.idx, fliplr(options.col.idx)], 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
colormap(options.col.map)
hold off

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig5C_empirical_prior_pred_dens_log_yhat_rt_M1_rev']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

%% Fig 5C summary metric M1
m=1;
bw5C = 0.25;%1;

figure
histogram(ep(m).logRT_sim(rt_maxidx,:), 'BinWidth', bw5C,...
    'FaceColor', options.col.cb(1,:), 'EdgeColor', options.col.cb(1,:))
hold on
histogram(ep(m).logRT_sim(rt_minidx(81),:), 'BinWidth', bw5C,...
    'FaceColor', options.col.cb(2,:), 'EdgeColor', options.col.cb(2,:))
histogram(ep(m).logRT_sim(rt_minidx(2),:), 'BinWidth', bw5C,...
    'FaceColor', options.col.cb(3,:), 'EdgeColor', options.col.cb(3,:))
% xlim([0 12])
xlim([1.5 10.5])
ylim([0 100])
ax = gca; ax.FontSize = 20;
legend('max var(log$(\hat{y}_{rt})$)','median var(log$(\hat{y}_{rt})$)','min var(log$(\hat{y}_{rt})$)',...
    'Interpreter','latex',...
    'Location', 'NorthEast',...
    'FontSize', 18)

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig5C_empirical_prior_pred_dens_log_yhat_rt_M1_rev_hist']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;


%% Fig 8B: Posterior Predictive Checks for M1 - logRT
m=1;

res.main.ppc.mod(m).mu1hat_sim = NaN(size(res.main.traj.u,1),options.main.nS);
res.main.ppc.mod(m).mu2hat_sim = NaN(size(res.main.traj.u,1),options.main.nS);
res.main.ppc.mod(m).mu3hat_sim = NaN(size(res.main.traj.u,1),options.main.nS);
res.main.ppc.mod(m).sa1hat_sim = NaN(size(res.main.traj.u,1),options.main.nS);
res.main.ppc.mod(m).sa2hat_sim = NaN(size(res.main.traj.u,1),options.main.nS);
res.main.ppc.mod(m).sa3hat_sim = NaN(size(res.main.traj.u,1),options.main.nS);
res.main.ppc.mod(m).ybin_sim = NaN(size(res.main.traj.u,1),options.main.nS);
res.main.ppc.mod(m).logRT_sim = NaN(size(res.main.traj.u,1),options.main.nS);


% collect logLL as measure for goodness of fit
for n = 1:options.main.nS
    for m = 1:size(res.main.ModSpace, 2)
        % log ll matrix
        res.main.Ll(n,m) = -res.main.est(m,n).optim.negLl;
        Llsplit = sum(res.main.est(m,n).optim.trialLogLlsplit, 'omitnan');
        res.main.Ll_bin(n,m) = Llsplit(1,1);
        res.main.Ll_rt(n,m) = Llsplit(1,2);      
    end
end
% return largest elements
m=1;
[maxval, maxidx] = maxk(res.main.Ll(:,m),options.main.nS);
% select best/avg/worst fitting subjects
ppc_sub = [maxidx(3); maxidx(30); maxidx(options.main.nS)];
% get adjusted correctness for each subject
n_max = 3;
for s = 1:n_max
    n = ppc_sub(s);
    adj_corr{s,1} = res.main.ppc.mod(m).sub(n).adj_correct';
    
    for n = 1:options.main.ppc.nDraws_per_sub
%         res.main.ppc.mod(m).ppc_sub(s).mu1hat_sim(:,n) = res.main.ppc.mod(m).sub(ppc_sub(s),n).sim.traj.muhat(:,1);
%         res.main.ppc.mod(m).ppc_sub(s).mu2hat_sim(:,n) = res.main.ppc.mod(m).sub(ppc_sub(s),n).sim.traj.muhat(:,2);
%         res.main.ppc.mod(m).ppc_sub(s).mu3hat_sim(:,n) = res.main.ppc.mod(m).sub(ppc_sub(s),n).sim.traj.muhat(:,3);
%         res.main.ppc.mod(m).ppc_sub(s).sa1hat_sim(:,n) = res.main.ppc.mod(m).sub(ppc_sub(s),n).sim.traj.sahat(:,1);
%         res.main.ppc.mod(m).ppc_sub(s).sa2hat_sim(:,n) = res.main.ppc.mod(m).sub(ppc_sub(s),n).sim.traj.sahat(:,2);
%         res.main.ppc.mod(m).ppc_sub(s).sa3hat_sim(:,n) = res.main.ppc.mod(m).sub(ppc_sub(s),n).sim.traj.sahat(:,3);
%         res.main.ppc.mod(m).ppc_sub(s).ybin_sim(:,n) = res.main.ppc.mod(m).sub(ppc_sub(s),n).sim.y(:,1);
%         res.main.ppc.mod(m).ppc_sub(s).logRT_sim(:,n) = res.main.ppc.mod(m).sub(ppc_sub(s),n).sim.y(:,2);
%         % get idx of ppc_sub with min/median/max var
%         [~,res.main.ppc.mod(m).ppc_sub(s).mh1_maxkidx] = maxk(var(res.main.ppc.mod(m).ppc_sub(s).mu1hat_sim,[],2),res.main.trials(end));
%         [~,res.main.ppc.mod(m).ppc_sub(s).mh2_maxkidx] = maxk(var(res.main.ppc.mod(m).ppc_sub(s).mu2hat_sim,[],2),res.main.trials(end));
%         [~,res.main.ppc.mod(m).ppc_sub(s).mh3_maxkidx] = maxk(var(res.main.ppc.mod(m).ppc_sub(s).mu3hat_sim,[],2),res.main.trials(end));
%         [~,res.main.ppc.mod(m).ppc_sub(s).sh1_maxkidx] = maxk(var(res.main.ppc.mod(m).ppc_sub(s).sa1hat_sim,[],2),res.main.trials(end));
%         [~,res.main.ppc.mod(m).ppc_sub(s).sh2_maxkidx] = maxk(var(res.main.ppc.mod(m).ppc_sub(s).sa2hat_sim,[],2),res.main.trials(end));
%         [~,res.main.ppc.mod(m).ppc_sub(s).sh3_maxkidx] = maxk(var(res.main.ppc.mod(m).ppc_sub(s).sa3hat_sim,[],2),res.main.trials(end));
%         [~,res.main.ppc.mod(m).ppc_sub(s).ybin_maxkidx] = maxk(var(res.main.ppc.mod(m).ppc_sub(s).ybin_sim,[],2),res.main.trials(end));
%         [~,res.main.ppc.mod(m).ppc_sub(s).logRT_maxkidx] = maxk(var(res.main.ppc.mod(m).ppc_sub(s).logRT_sim,[],2),res.main.trials(end));
    end
    [~,res.main.ppc.mod(m).ppc_sub(s).logRT_maxkidx] = maxk(var(res.main.ppc.mod(m).sub(ppc_sub(s),1).logRT_mat,[],2,'omitnan'),res.main.trials(end));
end

% create figure
for s = 1:n_max
    n = ppc_sub(s);
    figure('OuterPosition', outerpos1,...
    'Name', 'Probability Trajectory & Inputs');        
    % plot empirical logRT traj of post_sub n
    a=plot(res.main.est(m,res.main.ppc.ppc_sub(n)).y(:,2), 'r', 'LineWidth', 2);
    hold on
    % plot logRT PREDICTIONS generated using MAP values
    plot(res.main.ppc.mod(m).sub(n).map_sim.yhat(:,2),...
        'LineWidth', 2, 'color', options.col.tnub)
    % plot simulated logRTs from draws from posterior
    for i = 1:options.main.ppc.nDraws_per_sub
        try
            plot(res.main.ppc.mod(m).sub(n,i).sim.y(:,2), 'color', [options.col.tnub 0.1]) % alpha=0.1
        end
    end
    % RT limits by experiment
    plot(1:160, ones(160,1)*log(1700), '--k')
    plot(1:160, ones(160,1)*log(100), '--k')
    hold off
    ylim([4 8])
    uistack(a, 'top') % get empirical RTs to top
    ax = gca; ax.FontSize = 12;
    
    xline(res.main.ppc.mod(m).ppc_sub(s).logRT_maxkidx(1), 'LineWidth', 1.5, 'Color', options.col.cb(1,:))
    xline(res.main.ppc.mod(m).ppc_sub(s).logRT_maxkidx(80), 'LineWidth', 1.5, 'Color', options.col.cb(2,:))
    xline(res.main.ppc.mod(m).ppc_sub(s).logRT_maxkidx(160), 'LineWidth', 1.5, 'Color', options.col.cb(3,:))

    
    figdir = fullfile(saveDir, 'figures', 'paper',...
        ['fig8B_ppc_M1_logrt_sub_', num2str(n), '_rev']);
    print(figdir, '-dpng');
    print(figdir, '-dsvg');
    close;
end

% get simulated logRT from posterior predictive distr for M1
for s = 1:size(ppc_sub,1)
    n = ppc_sub(s);
    logRT_sim = NaN(size(res.main.traj.u,1),options.main.ppc.nDraws_per_sub);
    for i = 1:options.main.ppc.nDraws_per_sub
        try
            logRT_sim(:,i) = (res.main.ppc.mod(m).sub(n,i).sim.y(:,2)); %(res.main.est(m,n).y(:,2))
        end
    end

    % create figure
    figure()
    % histogram
    bwppc = 0.3;
    histogram(res.main.ppc.mod(m).sub(ppc_sub(s),1).logRT_mat(res.main.ppc.mod(m).ppc_sub(s).logRT_maxkidx(1),:),... 
        'BinWidth', bwppc,...
        'FaceColor', options.col.cb(1,:))
    hold on
    histogram(res.main.ppc.mod(m).sub(ppc_sub(s),1).logRT_mat(res.main.ppc.mod(m).ppc_sub(s).logRT_maxkidx(80),:),... 
        'BinWidth', bwppc,...
        'FaceColor', options.col.cb(2,:))
    histogram(res.main.ppc.mod(m).sub(ppc_sub(s),1).logRT_mat(res.main.ppc.mod(m).ppc_sub(s).logRT_maxkidx(160),:),... 
        'BinWidth', bwppc,...
        'FaceColor', options.col.cb(3,:))
    ax = gca; ax.FontSize = 28;
    ylim([0 100])
    xlim([2 10])
    legend('max var(log($\hat{y}_{rt}$))','median var(log($\hat{y}_{rt}$))','min var(log($\hat{y}_{rt}$))',...
        'Interpreter','latex', 'FontSize', 26,...
        'Location', 'NorthEast')

    figdir = fullfile(saveDir, 'figures', 'paper',...
        ['fig8B_sim_logRT_hist_M1_ppc_sub',num2str(n),'_rev']);
    print(figdir, '-dpng');
    print(figdir, '-dsvg');
    close;
end



%__________________________________________________________________________
%% SUPPL FIGS

% find points with highest(lowest var in empirical prior pred dens
for m = 1:7
    [~,ep(m).mh1_maxkidx] = maxk(var(ep(m).mu1hat_sim,[],2),res.main.trials(end));
    [~,ep(m).mh2_maxkidx] = maxk(var(ep(m).mu2hat_sim,[],2),res.main.trials(end));
    [~,ep(m).mh3_maxkidx] = maxk(var(ep(m).mu3hat_sim,[],2),res.main.trials(end));
    [~,ep(m).sh1_maxkidx] = maxk(var(ep(m).sa1hat_sim,[],2),res.main.trials(end));
    [~,ep(m).sh2_maxkidx] = maxk(var(ep(m).sa2hat_sim,[],2),res.main.trials(end));
    [~,ep(m).sh3_maxkidx] = maxk(var(ep(m).sa3hat_sim,[],2),res.main.trials(end));
    [~,ep(m).ybin_maxkidx] = maxk(var(ep(m).ybin_sim,[],2),res.main.trials(end));
    [~,ep(m).logRT_maxkidx] = maxk(var(ep(m).logRT_sim,[],2),res.main.trials(end));
end


%% S2b SIM indiv: plot ehgf prior pred dens MUHAT (empirical priors)
% def histogram binwidth
bwm5 = 0.03;
bwm3 = 0.15;
bwm1 = 0.3;

for m = 1:size(res.main.ModSpace,2)
    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    for j = 1:3
        % calc avg values
        traj_sim = NaN(size(res.main.traj.u,1),options.main.nS);
        for n = 1:options.sim.indiv.nS
            traj_sim(:,n) = res.sim.sub(n,m).data.traj.muhat(:,j);
        end
        avg_traj_sim = mean(traj_sim, 2, 'omitnan');
        if j == 1
            ybin_sim = NaN(size(res.main.traj.u,1),options.main.nS);
            for n = 1:options.sim.indiv.nS
                ybin_sim(:,n) = res.sim.sub(n,m).data.y(:,1);
            end
            avg_ybin_sim = mean(ybin_sim, 2, 'omitnan');
        end

        % plot muhat 1-3
        subplot(3,2,7-2*j)
        plot(res.sim.sub(1,m).data.traj.muhat(:,j), 'color', [options.col.tnub 0.1])
        hold on
        for n = 2:100
            plot(res.sim.sub(n,m).data.traj.muhat(:,j), 'color', [options.col.tnub 0.1])
        end
        plot(avg_traj_sim, 'color', options.col.tnub, 'LineWidth', 1.5)
        if j == 1
            plot(avg_ybin_sim, 'color', options.col.tnuy)
        end
        ytxt = ['$\hat{\mu}_{', num2str(j), '}$'];
        ylabel(ytxt, 'Interpreter', 'Latex')
        if j == 1
            xlabel('trials')
            xline(ep(m).mh1_maxkidx(1), 'LineWidth', 1.5, 'Color', options.col.cb(1,:))
            xline(ep(m).mh1_maxkidx(80), 'LineWidth', 1.5, 'Color', options.col.cb(2,:))
            xline(ep(m).mh1_maxkidx(159), 'LineWidth', 1.5, 'Color', options.col.cb(3,:))
        elseif j == 2
            xline(ep(m).mh2_maxkidx(1), 'LineWidth', 1.5, 'Color', options.col.cb(1,:))
            xline(ep(m).mh2_maxkidx(80), 'LineWidth', 1.5, 'Color', options.col.cb(2,:))
            xline(ep(m).mh3_maxkidx(159), 'LineWidth', 1.5, 'Color', options.col.cb(3,:))
        elseif j == 3
            xline(ep(m).mh3_maxkidx(1), 'LineWidth', 1.5, 'Color', options.col.cb(1,:))
            xline(ep(m).mh3_maxkidx(80), 'LineWidth', 1.5, 'Color', options.col.cb(2,:))
            xline(ep(m).mh3_maxkidx(159), 'LineWidth', 1.5, 'Color', options.col.cb(3,:))
        end
        ax = gca; ax.FontSize = 16;
        
        % plot muhat 1-3 histogram
        subplot(3,2,8-2*j)
        if j == 1
            histogram(ep(m).mu1hat_sim(ep(m).mh1_maxkidx(1),:), 'BinWidth', bwm5,...
                'FaceColor', options.col.cb(1,:))
            hold on
            histogram(ep(m).mu1hat_sim(ep(m).mh1_maxkidx(80),:), 'BinWidth', bwm5,...
                'FaceColor', options.col.cb(2,:))
            histogram(ep(m).mu1hat_sim(ep(m).mh1_maxkidx(159),:), 'BinWidth', bwm5,...
                'FaceColor', options.col.cb(3,:))
            ylim([0 100])
            xlim([0 1])
            ylabel('counts')
            xtxt = ['$\hat{\mu}_{1}$'];
            xlabel(xtxt, 'Interpreter', 'Latex')
            ax = gca; ax.FontSize = 16;
            legend('max var$(\hat{\mu}_{1})$','median var$(\hat{\mu}_{1})$','min var$(\hat{\mu}_{1})$',...
                'Interpreter','latex', 'FontSize', 9,...
                'Location', 'NorthWest')
        elseif j == 2
            histogram(ep(m).mu2hat_sim(ep(m).mh2_maxkidx(1),:), 'BinWidth', bwm3,...
                'FaceColor', options.col.cb(1,:))
            hold on
            histogram(ep(m).mu2hat_sim(ep(m).mh2_maxkidx(80),:), 'BinWidth', bwm3,...
                'FaceColor', options.col.cb(2,:))
            histogram(ep(m).mu2hat_sim(ep(m).mh2_maxkidx(159),:), 'BinWidth', bwm3,...
                'FaceColor', options.col.cb(3,:))
            ylim([0 100])
            ylabel('counts')
            xtxt = ['$\hat{\mu}_{2}$'];
            xlabel(xtxt, 'Interpreter', 'Latex')
            ax = gca; ax.FontSize = 16;
            legend('max var$(\hat{\mu}_{2})$','median var$(\hat{\mu}_{2})$','min var$(\hat{\mu}_{2})$',...
                'Interpreter','latex', 'FontSize', 9,...
                'Location', 'NorthWest')
        elseif j == 3 
            histogram(ep(m).mu3hat_sim(ep(m).mh3_maxkidx(1),:), 'BinWidth', bwm1,...
                'FaceColor', options.col.cb(1,:))
            hold on
            histogram(ep(m).mu3hat_sim(ep(m).mh3_maxkidx(80),:), 'BinWidth', bwm1,...
                'FaceColor', options.col.cb(2,:))
            histogram(ep(m).mu3hat_sim(ep(m).mh3_maxkidx(159),:), 'BinWidth', bwm1,...
                'FaceColor', options.col.cb(3,:))
            ylim([0 100])
            ylabel('counts')
            xtxt = ['$\hat{\mu}_{3}$'];
            xlabel(xtxt, 'Interpreter', 'Latex')
            ax = gca; ax.FontSize = 16;
            legend('max var$(\hat{\mu}_{3})$','median var$(\hat{\mu}_{3})$','min var$(\hat{\mu}_{3})$',...
                'Interpreter','latex', 'FontSize', 9,...
                'Location', 'NorthWest')
        end
    end
    figdir = fullfile(saveDir, 'figures', 'paper',...
        ['figS2b_empirical_prior_pred_dens_m', num2str(m), '_muhat_rev']);
    print(figdir, '-dpng');
    print(figdir, '-dsvg');
    close;
end

%% S2b SIM indiv: plot ehgf prior pred dens SAHAT (empirical priors)
% def histogram binwidth
bwm6 = 0.01;
bwm4 = 0.5;
bwm2 = 5;%20;

for m = 1:size(res.main.ModSpace,2)
    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    for j = 1:3
        % calc avg
        traj_sim = NaN(size(res.main.traj.u,1),options.main.nS);
        for n = 1:options.sim.indiv.nS
            traj_sim(:,n) = res.sim.sub(n,m).data.traj.sahat(:,j);
        end
        avg_traj_sim = mean(traj_sim, 2, 'omitnan');
        % plot sahat 1-3
        subplot(3,2,7-2*j)
        plot(res.sim.sub(1,m).data.traj.sahat(:,j), 'color', [options.col.tnub 0.1])
        hold on
        for n = 2:100
            plot(res.sim.sub(n,m).data.traj.sahat(:,j), 'color', [options.col.tnub 0.1])
        end
        plot(avg_traj_sim, 'color', options.col.tnub, 'LineWidth', 1.5)
        ytxt = ['$\hat{\sigma}_{', num2str(j), '}$'];
        ylabel(ytxt, 'Interpreter', 'Latex')
        if j == 1
            xlabel('trials')
            xline(ep(m).sh1_maxkidx(1), 'LineWidth', 1.5, 'Color', options.col.cb(1,:))
            xline(ep(m).sh1_maxkidx(80), 'LineWidth', 1.5, 'Color', options.col.cb(2,:))
            xline(ep(m).sh1_maxkidx(159), 'LineWidth', 1.5, 'Color', options.col.cb(3,:))
        elseif j == 2
            xline(ep(m).sh2_maxkidx(1), 'LineWidth', 1.5, 'Color', options.col.cb(1,:))
            xline(ep(m).sh2_maxkidx(80), 'LineWidth', 1.5, 'Color', options.col.cb(2,:))
            xline(ep(m).sh3_maxkidx(159), 'LineWidth', 1.5, 'Color', options.col.cb(3,:))
        elseif j == 3
            xline(ep(m).sh3_maxkidx(1), 'LineWidth', 1.5, 'Color', options.col.cb(1,:))
            xline(ep(m).sh3_maxkidx(80), 'LineWidth', 1.5, 'Color', options.col.cb(2,:))
            xline(ep(m).sh3_maxkidx(159), 'LineWidth', 1.5, 'Color', options.col.cb(3,:))
        end
        ax = gca; ax.FontSize = 16;
        
        % plot sahat 1-3 histogram
        subplot(3,2,8-2*j)
        if j == 1
            histogram(ep(m).sa1hat_sim(ep(m).sh1_maxkidx(1),:), 'BinWidth', bwm6,...
                'FaceColor', options.col.cb(1,:))
            hold on
            histogram(ep(m).sa1hat_sim(ep(m).sh1_maxkidx(80),:), 'BinWidth', bwm6,...
                'FaceColor', options.col.cb(2,:))
            histogram(ep(m).sa1hat_sim(ep(m).sh1_maxkidx(159),:), 'BinWidth', bwm6,...
                'FaceColor', options.col.cb(3,:))
            ylim([0 100])
            xlim([0 0.4])
            ylabel('counts')
            xtxt = ['$\hat{\sigma}_{1}$'];
            xlabel(xtxt, 'Interpreter', 'Latex')
            ax = gca; ax.FontSize = 16;
            legend('max var$(\hat{\sigma}_{1})$','median var$(\hat{\sigma}_{1})$','min var$(\hat{\sigma}_{1})$',...
                'Interpreter','latex', 'FontSize', 9,...
                'Location', 'NorthEast')
        elseif j == 2
            histogram(ep(m).sa2hat_sim(ep(m).sh2_maxkidx(1),:), 'BinWidth', bwm4,...
                'FaceColor', options.col.cb(1,:))
            hold on
            histogram(ep(m).sa2hat_sim(ep(m).sh2_maxkidx(80),:), 'BinWidth', bwm4,...
                'FaceColor', options.col.cb(2,:))
            histogram(ep(m).sa2hat_sim(ep(m).sh2_maxkidx(159),:), 'BinWidth', bwm4,...
                'FaceColor', options.col.cb(3,:))
            ylim([0 100])
            ylabel('counts')
            xtxt = ['$\hat{\sigma}_{2}$'];
            xlabel(xtxt, 'Interpreter', 'Latex')
            ax = gca; ax.FontSize = 16;
            legend('max var$(\hat{\sigma}_{2})$','median var$(\hat{\sigma}_{2})$','min var$(\hat{\sigma}_{2})$',...
                'Interpreter','latex', 'FontSize', 9,...
                'Location', 'NorthEast')
        elseif j == 3 
            histogram(ep(m).sa3hat_sim(ep(m).sh3_maxkidx(1),:), 'BinWidth', bwm2,...
                'FaceColor', options.col.cb(1,:))
            hold on
            histogram(ep(m).sa3hat_sim(ep(m).sh3_maxkidx(80),:), 'BinWidth', bwm2,...
                'FaceColor', options.col.cb(2,:))
            histogram(ep(m).sa3hat_sim(ep(m).sh3_maxkidx(159),:), 'BinWidth', bwm2,...
                'FaceColor', options.col.cb(3,:))
            ylim([0 100])
            ylabel('counts')
            xtxt = ['$\hat{\sigma}_{3}$'];
            xlabel(xtxt, 'Interpreter', 'Latex')
            ax = gca; ax.FontSize = 16;
            legend('max var$(\hat{\sigma}_{3})$','median var$(\hat{\sigma}_{3})$','min var$(\hat{\sigma}_{3})$',...
                'Interpreter','latex', 'FontSize', 9,...
                'Location', 'NorthEast')
        end
    end
    figdir = fullfile(saveDir, 'figures', 'paper',...
        ['figS2b_empirical_prior_pred_dens_m', num2str(m), '_sahat_rev']);
    print(figdir, '-dpng');
    print(figdir, '-dsvg');
    close;
end


%% S2b: empirical prior pred distr (log RT)

% M1-4
figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
for m = 1:4
    % calc avg
    ep(m).avg_logRT_sim = mean(ep(m).logRT_sim, 2, 'omitnan');

    % create figure
    subplot(4,2,(2*m)-1)
    
    % plot logRT over time
    plot(ep(m).avg_logRT_sim, 'color', options.col.tnub, 'LineWidth', 1.5)
    hold on
    plot(1:160, ones(160,1)*log(1700), '--k')
    plot(ep(m).logRT_sim, 'color', [options.col.tnub 0.1])    
    plot(1:160, ones(160,1)*log(100), '--k')
    ylim([2 10]);%ylim([log(100) log(1700)]);
    % color code different phases
    ax = axis;
    fill([res.main.trials, fliplr(res.main.trials)],...
        [ax(3)*ones(1,length(res.main.trials)), ax(4)*ones(1,length(res.main.trials))],...
        [options.col.idx, fliplr(options.col.idx)], 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
    colormap(options.col.map)
    hold off
    ylabel('log($y_{rt,sim}$)', 'Interpreter', 'Latex')
%     if m == 4
%         xlabel('trials')
%     end
    ax = gca; ax.FontSize = 12;
    txt = ['model ' num2str(m)];
    title(txt)
    xline(ep(m).logRT_maxkidx(1), 'LineWidth', 1.5, 'Color', options.col.cb(1,:))
    xline(ep(m).logRT_maxkidx(80), 'LineWidth', 1.5, 'Color', options.col.cb(2,:))
    xline(ep(m).logRT_maxkidx(159), 'LineWidth', 1.5, 'Color', options.col.cb(3,:))
    
    % plot logRT histogram
    subplot(4,2,(2*m))
    histogram(ep(m).logRT_sim(ep(m).logRT_maxkidx(1),:),... 'BinWidth', bwm2,...
        'FaceColor', options.col.cb(1,:))
    hold on
    histogram(ep(m).logRT_sim(ep(m).logRT_maxkidx(80),:),... 'BinWidth', bwm3,...
        'FaceColor', options.col.cb(2,:))
    histogram(ep(m).logRT_sim(ep(m).logRT_maxkidx(159),:),... 'BinWidth', bwm4,...
        'FaceColor', options.col.cb(3,:))
    ylim([0 100])
    xlim([0 16])
    ylabel('counts')
%     if m == 4
%         xtxt = ['log($y_{rt,sim}$)'];
%         xlabel(xtxt, 'Interpreter', 'Latex')
%     end
    ax = gca; ax.FontSize = 12;
    legend('max var(log($y_{rt,sim}$))','median var(log($y_{rt,sim}$))','min var(log($y_{rt,sim}$))',...
        'Interpreter','latex', 'FontSize', 9,...
        'Location', 'NorthEast')
    title(txt)
end
figdir = fullfile(saveDir, 'figures', 'paper',...
    ['figS2b_empirical_prior_pred_distr_log_yhat_rt_m1_m4_rev']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

% M5-7
figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
f_idx = 1;
for m = 5:size(res.main.ModSpace,2)
    % calc avg
    ep(m).avg_logRT_sim = mean(ep(m).logRT_sim, 2, 'omitnan');

    % create figure
    subplot(4,2,(2*f_idx)-1)
    
    % plot logRT over time
    plot(ep(m).avg_logRT_sim, 'color', options.col.tnub, 'LineWidth', 1.5)
    hold on
    plot(1:160, ones(160,1)*log(1700), '--k')
    plot(ep(m).logRT_sim, 'color', [options.col.tnub 0.1])    
    plot(1:160, ones(160,1)*log(100), '--k')
    ylim([2 10]);%ylim([log(100) log(1700)]);
    % color code different phases
    ax = axis;
    fill([res.main.trials, fliplr(res.main.trials)],...
        [ax(3)*ones(1,length(res.main.trials)), ax(4)*ones(1,length(res.main.trials))],...
        [options.col.idx, fliplr(options.col.idx)], 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
    colormap(options.col.map)
    hold off
    ylabel('log($y_{rt,sim}$)', 'Interpreter', 'Latex')
    if m == 7
        xlabel('trials')
    end
    ax = gca; ax.FontSize = 12;
    txt = ['model ' num2str(m)];
    title(txt)
    xline(ep(m).logRT_maxkidx(1), 'LineWidth', 1.5, 'Color', options.col.cb(1,:))
    xline(ep(m).logRT_maxkidx(80), 'LineWidth', 1.5, 'Color', options.col.cb(2,:))
    xline(ep(m).logRT_maxkidx(159), 'LineWidth', 1.5, 'Color', options.col.cb(3,:))
    
    % plot logRT histogram
    subplot(4,2,(2*f_idx))
    histogram(ep(m).logRT_sim(ep(m).logRT_maxkidx(1),:),... 'BinWidth', bwm2,...
        'FaceColor', options.col.cb(1,:))
    hold on
    histogram(ep(m).logRT_sim(ep(m).logRT_maxkidx(80),:),... 'BinWidth', bwm3,...
        'FaceColor', options.col.cb(2,:))
    histogram(ep(m).logRT_sim(ep(m).logRT_maxkidx(159),:),... 'BinWidth', bwm4,...
        'FaceColor', options.col.cb(3,:))
    ylim([0 100])
    xlim([0 16])
    ylabel('counts')
    if m == 7
        xtxt = ['log($y_{rt,sim}$)'];
        xlabel(xtxt, 'Interpreter', 'Latex')
    end
    ax = gca; ax.FontSize = 12;
    legend('max var(log($y_{rt,sim}$))','median var(log($y_{rt,sim}$))','min var(log($y_{rt,sim}$))',...
        'Interpreter','latex', 'FontSize', 9,...
        'Location', 'NorthEast')
    title(txt)
    f_idx = f_idx+1;
end
figdir = fullfile(saveDir, 'figures', 'paper',...
    ['figS2b_empirical_prior_pred_distr_log_yhat_rt_m5_m7_rev']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;



%% ____________________________________


%% S2c: INITIAL prior pred distr

% create synthetic data using initial priors

% pre-allocate variables
sim = struct();
input = struct();

% loop over synthetic subjects & model space
for n = 1:options.sim.indiv.nS
   
    for m = 1:size(res.pilot.ModSpace,2)
        
        % sample free parameter values
        input.prc.transInp = res.pilot.ModSpace(m).prc_config.priormus;
        input.obs.transInp = res.pilot.ModSpace(m).obs_config.priormus;
        for j = 1:size(res.pilot.ModSpace(m).prc_idx,2)
            input.prc.transInp(res.pilot.ModSpace(m).prc_idx(j)) = ...
                normrnd(res.pilot.ModSpace(m).prc_config.priormus(res.pilot.ModSpace(m).prc_idx(j)),...
                abs(sqrt(res.pilot.ModSpace(m).prc_config.priorsas(res.pilot.ModSpace(m).prc_idx(j)))));
        end
        for k = 1:size(res.pilot.ModSpace(m).obs_idx,2)
            input.obs.transInp(res.pilot.ModSpace(m).obs_idx(k)) = ...
                normrnd(res.pilot.ModSpace(m).obs_config.priormus(res.pilot.ModSpace(m).obs_idx(k)),...
                abs(sqrt(res.pilot.ModSpace(m).obs_config.priorsas(res.pilot.ModSpace(m).obs_idx(k)))));
        end
        
        % create simulation input vectors (native space)
        c.c_prc = res.pilot.ModSpace(m).prc_config;
        input.prc.nativeInp = res.pilot.ModSpace(m).prc_config.transp_prc_fun(c, input.prc.transInp);
        c.c_obs = res.pilot.ModSpace(m).obs_config;
        input.obs.nativeInp = res.pilot.ModSpace(m).obs_config.transp_obs_fun(c, input.obs.transInp);
        
        % simulate responses
        stable = 0;
        while stable == 0
            me = [];
            try
                sim.sub(n,m).data = tapas_simModel(res.pilot.traj.u,...
                    res.pilot.ModSpace(m).prc,...
                    input.prc.nativeInp,...
                    res.pilot.ModSpace(m).obs,...
                    input.obs.nativeInp,...
                    options.rng.settings.State(options.rng.idx, 1));
                stable = 1;
            catch me
                fprintf('simulation failed for Model %1.0f, synth. Sub %1.0f \n', [m, n]);
                fprintf('Prc Param Values: \n');
                input.prc.nativeInp
                fprintf('Obs Param Values: \n');
                input.obs.nativeInp
                % re-sample prc param values
                for j = 1:size(res.pilot.ModSpace(m).prc_idx,2)
                    input.prc.transInp(res.pilot.ModSpace(m).prc_idx(j)) = ...
                        normrnd(res.pilot.ModSpace(m).prc_config.priormus(res.pilot.ModSpace(m).prc_idx(j)),...
                        abs(sqrt(res.pilot.ModSpace(m).prc_config.priorsas(res.pilot.ModSpace(m).prc_idx(j)))));
                end
                input.prc.nativeInp = res.pilot.ModSpace(m).prc_config.transp_prc_fun(c, input.prc.transInp);
            end
        end

        % save sim input
        sim.sub(n,m).input = input;
        
        % Update the rng state idx
        options.rng.idx = options.rng.idx+1;
        if options.rng.idx == (length(options.rng.settings.State)+1)
            options.rng.idx = 1;
        end
    end
end

% reset rng idx
options.rng.idx = 1;

% save in struct
res.pilot.sim = sim;


%% collect prior pred dens for all models
for m = 1:7
    ip(m).mu1hat_sim = NaN(size(res.pilot.traj.u,1),options.main.nS);
    ip(m).mu2hat_sim = NaN(size(res.pilot.traj.u,1),options.main.nS);
    ip(m).mu3hat_sim = NaN(size(res.pilot.traj.u,1),options.main.nS);
    ip(m).sa1hat_sim = NaN(size(res.pilot.traj.u,1),options.main.nS);
    ip(m).sa2hat_sim = NaN(size(res.pilot.traj.u,1),options.main.nS);
    ip(m).sa3hat_sim = NaN(size(res.pilot.traj.u,1),options.main.nS);
    ip(m).ybin_sim = NaN(size(res.pilot.traj.u,1),options.main.nS);
    ip(m).logRT_sim = NaN(size(res.pilot.traj.u,1),options.main.nS);
    for n = 1:options.sim.indiv.nS
        ip(m).mu1hat_sim(:,n) = res.pilot.sim.sub(n,m).data.traj.muhat(:,1);
        ip(m).mu2hat_sim(:,n) = res.pilot.sim.sub(n,m).data.traj.muhat(:,2);
        ip(m).mu3hat_sim(:,n) = res.pilot.sim.sub(n,m).data.traj.muhat(:,3);
        ip(m).sa1hat_sim(:,n) = res.pilot.sim.sub(n,m).data.traj.sahat(:,1);
        ip(m).sa2hat_sim(:,n) = res.pilot.sim.sub(n,m).data.traj.sahat(:,2);
        ip(m).sa3hat_sim(:,n) = res.pilot.sim.sub(n,m).data.traj.sahat(:,3);
        ip(m).ybin_sim(:,n) = res.pilot.sim.sub(n,m).data.y(:,1);
        ip(m).logRT_sim(:,n) = res.pilot.sim.sub(n,m).data.y(:,2);
    end
end

% find points with highest(lowest var in empirical prior pred dens
for m = 1:7
    [~,ip(m).mh1_maxkidx] = maxk(var(ip(m).mu1hat_sim,[],2),res.pilot.trials(end));
    [~,ip(m).mh2_maxkidx] = maxk(var(ip(m).mu2hat_sim,[],2),res.pilot.trials(end));
    [~,ip(m).mh3_maxkidx] = maxk(var(ip(m).mu3hat_sim,[],2),res.pilot.trials(end));
    [~,ip(m).sh1_maxkidx] = maxk(var(ip(m).sa1hat_sim,[],2),res.pilot.trials(end));
    [~,ip(m).sh2_maxkidx] = maxk(var(ip(m).sa2hat_sim,[],2),res.pilot.trials(end));
    [~,ip(m).sh3_maxkidx] = maxk(var(ip(m).sa3hat_sim,[],2),res.pilot.trials(end));
    [~,ip(m).ybin_maxkidx] = maxk(var(ip(m).ybin_sim,[],2),res.pilot.trials(end));
    [~,ip(m).logRT_maxkidx] = maxk(var(ip(m).logRT_sim,[],2),res.pilot.trials(end));
end


%% S2c SIM indiv: plot ehgf prior pred dens MUHAT (initial priors)
% def histogram binwidth
bwm5 = 0.03;
bwm3 = 0.15;
bwm1 = 0.3;

for m = 1:size(res.pilot.ModSpace,2)
    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    for j = 1:3
        % calc avg values
        traj_sim = NaN(size(res.pilot.traj.u,1),options.main.nS);
        for n = 1:options.sim.indiv.nS
            traj_sim(:,n) = res.pilot.sim.sub(n,m).data.traj.muhat(:,j);
        end
        avg_traj_sim = mean(traj_sim, 2, 'omitnan');
        if j == 1
            ybin_sim = NaN(size(res.pilot.traj.u,1),options.main.nS);
            for n = 1:options.sim.indiv.nS
                ybin_sim(:,n) = res.pilot.sim.sub(n,m).data.y(:,1);
            end
            avg_ybin_sim = mean(ybin_sim, 2, 'omitnan');
        end

        % plot muhat 1-3
        subplot(3,2,7-2*j)
        plot(res.pilot.sim.sub(1,m).data.traj.muhat(:,j), 'color', [options.col.tnub 0.1])
        hold on
        for n = 2:100
            plot(res.pilot.sim.sub(n,m).data.traj.muhat(:,j), 'color', [options.col.tnub 0.1])
        end
        plot(avg_traj_sim, 'color', options.col.tnub, 'LineWidth', 1.5)
        if j == 1
            plot(avg_ybin_sim, 'color', options.col.tnuy)
        end
        ytxt = ['$\hat{\mu}_{', num2str(j), '}$'];
        ylabel(ytxt, 'Interpreter', 'Latex')
        if j == 1
            xlabel('trials')
            xline(ip(m).mh1_maxkidx(1), 'LineWidth', 1.5, 'Color', options.col.cb(1,:))
            xline(ip(m).mh1_maxkidx(80), 'LineWidth', 1.5, 'Color', options.col.cb(2,:))
            xline(ip(m).mh1_maxkidx(159), 'LineWidth', 1.5, 'Color', options.col.cb(3,:))
        elseif j == 2
            xline(ip(m).mh2_maxkidx(1), 'LineWidth', 1.5, 'Color', options.col.cb(1,:))
            xline(ip(m).mh2_maxkidx(80), 'LineWidth', 1.5, 'Color', options.col.cb(2,:))
            xline(ip(m).mh3_maxkidx(159), 'LineWidth', 1.5, 'Color', options.col.cb(3,:))
        elseif j == 3
            xline(ip(m).mh3_maxkidx(1), 'LineWidth', 1.5, 'Color', options.col.cb(1,:))
            xline(ip(m).mh3_maxkidx(80), 'LineWidth', 1.5, 'Color', options.col.cb(2,:))
            xline(ip(m).mh3_maxkidx(159), 'LineWidth', 1.5, 'Color', options.col.cb(3,:))
        end
        ax = gca; ax.FontSize = 16;
        
        % plot muhat 1-3 histogram
        subplot(3,2,8-2*j)
        if j == 1
            histogram(ip(m).mu1hat_sim(ip(m).mh1_maxkidx(1),:), 'BinWidth', bwm5,...
                'FaceColor', options.col.cb(1,:))
            hold on
            histogram(ip(m).mu1hat_sim(ip(m).mh1_maxkidx(80),:), 'BinWidth', bwm5,...
                'FaceColor', options.col.cb(2,:))
            histogram(ip(m).mu1hat_sim(ip(m).mh1_maxkidx(159),:), 'BinWidth', bwm5,...
                'FaceColor', options.col.cb(3,:))
            ylim([0 100])
            xlim([0 1])
            ylabel('counts')
            xtxt = ['$\hat{\mu}_{1}$'];
            xlabel(xtxt, 'Interpreter', 'Latex')
            ax = gca; ax.FontSize = 16;
            legend('max var$(\hat{\mu}_{1})$','median var$(\hat{\mu}_{1})$','min var$(\hat{\mu}_{1})$',...
                'Interpreter','latex', 'FontSize', 9,...
                'Location', 'NorthWest')
        elseif j == 2
            histogram(ip(m).mu2hat_sim(ip(m).mh2_maxkidx(1),:), 'BinWidth', bwm3,...
                'FaceColor', options.col.cb(1,:))
            hold on
            histogram(ip(m).mu2hat_sim(ip(m).mh2_maxkidx(80),:), 'BinWidth', bwm3,...
                'FaceColor', options.col.cb(2,:))
            histogram(ip(m).mu2hat_sim(ip(m).mh2_maxkidx(159),:), 'BinWidth', bwm3,...
                'FaceColor', options.col.cb(3,:))
            ylim([0 100])
            ylabel('counts')
            xtxt = ['$\hat{\mu}_{2}$'];
            xlabel(xtxt, 'Interpreter', 'Latex')
            ax = gca; ax.FontSize = 16;
            legend('max var$(\hat{\mu}_{2})$','median var$(\hat{\mu}_{2})$','min var$(\hat{\mu}_{2})$',...
                'Interpreter','latex', 'FontSize', 9,...
                'Location', 'NorthWest')
        elseif j == 3 
            histogram(ip(m).mu3hat_sim(ip(m).mh3_maxkidx(1),:), 'BinWidth', bwm1,...
                'FaceColor', options.col.cb(1,:))
            hold on
            histogram(ip(m).mu3hat_sim(ip(m).mh3_maxkidx(80),:), 'BinWidth', bwm1,...
                'FaceColor', options.col.cb(2,:))
            histogram(ip(m).mu3hat_sim(ip(m).mh3_maxkidx(159),:), 'BinWidth', bwm1,...
                'FaceColor', options.col.cb(3,:))
            ylim([0 100])
            ylabel('counts')
            xtxt = ['$\hat{\mu}_{3}$'];
            xlabel(xtxt, 'Interpreter', 'Latex')
            ax = gca; ax.FontSize = 16;
            legend('max var$(\hat{\mu}_{3})$','median var$(\hat{\mu}_{3})$','min var$(\hat{\mu}_{3})$',...
                'Interpreter','latex', 'FontSize', 9,...
                'Location', 'NorthWest')
        end
    end
    figdir = fullfile(saveDir, 'figures', 'paper',...
        ['figS2c_initial_prior_pred_dens_m', num2str(m), '_muhat_rev']);
    print(figdir, '-dpng');
    print(figdir, '-dsvg');
    close;
end

%% S2c SIM indiv: plot ehgf prior pred dens SAHAT (initial priors)
% def histogram binwidth
bwm6 = 0.01;
bwm4 = 0.5;
bwm2 = 5;

for m = 1:size(res.pilot.ModSpace,2)
    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    for j = 1:3
        % calc avg
        traj_sim = NaN(size(res.pilot.traj.u,1),options.main.nS);
        for n = 1:options.sim.indiv.nS
            traj_sim(:,n) = res.pilot.sim.sub(n,m).data.traj.sahat(:,j);
        end
        avg_traj_sim = mean(traj_sim, 2, 'omitnan');
        % plot sahat 1-3
        subplot(3,2,7-2*j)
        plot(res.pilot.sim.sub(1,m).data.traj.sahat(:,j), 'color', [options.col.tnub 0.1])
        hold on
        for n = 2:100
            plot(res.pilot.sim.sub(n,m).data.traj.sahat(:,j), 'color', [options.col.tnub 0.1])
        end
        plot(avg_traj_sim, 'color', options.col.tnub, 'LineWidth', 1.5)
        ytxt = ['$\hat{\sigma}_{', num2str(j), '}$'];
        ylabel(ytxt, 'Interpreter', 'Latex')
        if j == 1
            xlabel('trials')
            xline(ip(m).sh1_maxkidx(1), 'LineWidth', 1.5, 'Color', options.col.cb(1,:))
            xline(ip(m).sh1_maxkidx(80), 'LineWidth', 1.5, 'Color', options.col.cb(2,:))
            xline(ip(m).sh1_maxkidx(159), 'LineWidth', 1.5, 'Color', options.col.cb(3,:))
        elseif j == 2
            xline(ip(m).sh2_maxkidx(1), 'LineWidth', 1.5, 'Color', options.col.cb(1,:))
            xline(ip(m).sh2_maxkidx(80), 'LineWidth', 1.5, 'Color', options.col.cb(2,:))
            xline(ip(m).sh3_maxkidx(159), 'LineWidth', 1.5, 'Color', options.col.cb(3,:))
        elseif j == 3
            xline(ip(m).sh3_maxkidx(1), 'LineWidth', 1.5, 'Color', options.col.cb(1,:))
            xline(ip(m).sh3_maxkidx(80), 'LineWidth', 1.5, 'Color', options.col.cb(2,:))
            xline(ip(m).sh3_maxkidx(159), 'LineWidth', 1.5, 'Color', options.col.cb(3,:))
        end
        ax = gca; ax.FontSize = 16;
        
        % plot sahat 1-3 histogram
        subplot(3,2,8-2*j)
        if j == 1
            histogram(ip(m).sa1hat_sim(ip(m).sh1_maxkidx(1),:), 'BinWidth', bwm6,...
                'FaceColor', options.col.cb(1,:))
            hold on
            histogram(ip(m).sa1hat_sim(ip(m).sh1_maxkidx(80),:), 'BinWidth', bwm6,...
                'FaceColor', options.col.cb(2,:))
            histogram(ip(m).sa1hat_sim(ip(m).sh1_maxkidx(159),:), 'BinWidth', bwm6,...
                'FaceColor', options.col.cb(3,:))
            ylim([0 100])
            xlim([0 0.4])
            ylabel('counts')
            xtxt = ['$\hat{\sigma}_{1}$'];
            xlabel(xtxt, 'Interpreter', 'Latex')
            ax = gca; ax.FontSize = 16;
            legend('max var$(\hat{\sigma}_{1})$','median var$(\hat{\sigma}_{1})$','min var$(\hat{\sigma}_{1})$',...
                'Interpreter','latex', 'FontSize', 9,...
                'Location', 'NorthEast')
        elseif j == 2
            histogram(ip(m).sa2hat_sim(ip(m).sh2_maxkidx(1),:), 'BinWidth', bwm4,...
                'FaceColor', options.col.cb(1,:))
            hold on
            histogram(ip(m).sa2hat_sim(ip(m).sh2_maxkidx(80),:), 'BinWidth', bwm4,...
                'FaceColor', options.col.cb(2,:))
            histogram(ip(m).sa2hat_sim(ip(m).sh2_maxkidx(159),:), 'BinWidth', bwm4,...
                'FaceColor', options.col.cb(3,:))
            ylim([0 100])
            ylabel('counts')
            xtxt = ['$\hat{\sigma}_{2}$'];
            xlabel(xtxt, 'Interpreter', 'Latex')
            ax = gca; ax.FontSize = 16;
            legend('max var$(\hat{\sigma}_{2})$','median var$(\hat{\sigma}_{2})$','min var$(\hat{\sigma}_{2})$',...
                'Interpreter','latex', 'FontSize', 9,...
                'Location', 'NorthEast')
        elseif j == 3 
            histogram(ip(m).sa3hat_sim(ip(m).sh3_maxkidx(1),:), 'BinWidth', bwm2,...
                'FaceColor', options.col.cb(1,:))
            hold on
            histogram(ip(m).sa3hat_sim(ip(m).sh3_maxkidx(80),:), 'BinWidth', bwm2,...
                'FaceColor', options.col.cb(2,:))
            histogram(ip(m).sa3hat_sim(ip(m).sh3_maxkidx(159),:), 'BinWidth', bwm2,...
                'FaceColor', options.col.cb(3,:))
            ylim([0 100])
            ylabel('counts')
            xtxt = ['$\hat{\sigma}_{3}$'];
            xlabel(xtxt, 'Interpreter', 'Latex')
            ax = gca; ax.FontSize = 16;
            legend('max var$(\hat{\sigma}_{3})$','median var$(\hat{\sigma}_{3})$','min var$(\hat{\sigma}_{3})$',...
                'Interpreter','latex', 'FontSize', 9,...
                'Location', 'NorthEast')
        end
    end
    figdir = fullfile(saveDir, 'figures', 'paper',...
        ['figS2c_initial_prior_pred_dens_m', num2str(m), '_sahat_rev']);
    print(figdir, '-dpng');
    print(figdir, '-dsvg');
    close;
end


%% S2c: initial prior pred distr (log RT)
bwrt = 5;

% M1-4
figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
for m = 1:4
    % calc avg
    ip(m).avg_logRT_sim = mean(ip(m).logRT_sim, 2, 'omitnan');

    % create figure
    subplot(4,2,(2*m)-1)
    
    % plot logRT over time
    plot(ip(m).avg_logRT_sim, 'color', options.col.tnub, 'LineWidth', 1.5)
    hold on
    plot(1:160, ones(160,1)*log(1700), '--k')
    plot(ip(m).logRT_sim, 'color', [options.col.tnub 0.1])    
    plot(1:160, ones(160,1)*log(100), '--k')
    ylim([-50 50]);%ylim([log(100) log(1700)]);
    % color code different phases
    ax = axis;
    fill([res.pilot.trials, fliplr(res.pilot.trials)],...
        [ax(3)*ones(1,length(res.pilot.trials)), ax(4)*ones(1,length(res.pilot.trials))],...
        [options.col.idx, fliplr(options.col.idx)], 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
    colormap(options.col.map)
    hold off
    ylabel('log($y_{rt,sim}$)', 'Interpreter', 'Latex')
%     if m == 4
%         xlabel('trials')
%     end
    ax = gca; ax.FontSize = 12;
    txt = ['model ' num2str(m)];
    title(txt)
    xline(ip(m).logRT_maxkidx(1), 'LineWidth', 1.5, 'Color', options.col.cb(1,:))
    xline(ip(m).logRT_maxkidx(80), 'LineWidth', 1.5, 'Color', options.col.cb(2,:))
    xline(ip(m).logRT_maxkidx(159), 'LineWidth', 1.5, 'Color', options.col.cb(3,:))

    
    % plot logRT histogram
    subplot(4,2,(2*m))
    histogram(ip(m).logRT_sim(ip(m).logRT_maxkidx(1),:), 'BinWidth', bwrt,...
        'FaceColor', options.col.cb(1,:))
    hold on
    histogram(ip(m).logRT_sim(ip(m).logRT_maxkidx(80),:), 'BinWidth', bwrt,...
        'FaceColor', options.col.cb(2,:))
    histogram(ip(m).logRT_sim(ip(m).logRT_maxkidx(159),:), 'BinWidth', bwrt,...
        'FaceColor', options.col.cb(3,:))
    ylim([0 100])
    xlim([-50 50])
    ylabel('counts')
%     if m == 4
%         xtxt = ['log($y_{rt,sim}$)'];
%         xlabel(xtxt, 'Interpreter', 'Latex')
%     end
    ax = gca; ax.FontSize = 12;
    legend('max var(log($y_{rt,sim}$))','median var(log($y_{rt,sim}$))','min var(log($y_{rt,sim}$))',...
        'Interpreter','latex', 'FontSize', 9,...
        'Location', 'NorthEast')
    title(txt)
end
figdir = fullfile(saveDir, 'figures', 'paper',...
    ['figS2c_initial_prior_pred_distr_log_yhat_rt_m1_m4_rev']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

% M5-7
figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
f_idx = 1;
for m = 5:size(res.pilot.ModSpace,2)
    % calc avg
    ip(m).avg_logRT_sim = mean(ip(m).logRT_sim, 2, 'omitnan');

    % create figure
    subplot(4,2,(2*f_idx)-1)
    
    % plot logRT over time
    plot(ip(m).avg_logRT_sim, 'color', options.col.tnub, 'LineWidth', 1.5)
    hold on
    plot(1:160, ones(160,1)*log(1700), '--k')
    plot(ip(m).logRT_sim, 'color', [options.col.tnub 0.1])    
    plot(1:160, ones(160,1)*log(100), '--k')
    ylim([-50 50]);%ylim([log(100) log(1700)]);
    % color code different phases
    ax = axis;
    fill([res.pilot.trials, fliplr(res.pilot.trials)],...
        [ax(3)*ones(1,length(res.pilot.trials)), ax(4)*ones(1,length(res.pilot.trials))],...
        [options.col.idx, fliplr(options.col.idx)], 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
    colormap(options.col.map)
    hold off
    ylabel('log($y_{rt,sim}$)', 'Interpreter', 'Latex')
    if m == 7
        xlabel('trials')
    end
    ax = gca; ax.FontSize = 12;
    txt = ['model ' num2str(m)];
    title(txt)
    xline(ip(m).logRT_maxkidx(1), 'LineWidth', 1.5, 'Color', options.col.cb(1,:))
    xline(ip(m).logRT_maxkidx(80), 'LineWidth', 1.5, 'Color', options.col.cb(2,:))
    xline(ip(m).logRT_maxkidx(159), 'LineWidth', 1.5, 'Color', options.col.cb(3,:))
    
    % plot logRT histogram
    subplot(4,2,(2*f_idx))
    histogram(ip(m).logRT_sim(ip(m).logRT_maxkidx(1),:), 'BinWidth', bwrt,...
        'FaceColor', options.col.cb(1,:))
    hold on
    histogram(ip(m).logRT_sim(ip(m).logRT_maxkidx(80),:), 'BinWidth', bwrt,...
        'FaceColor', options.col.cb(2,:))
    histogram(ip(m).logRT_sim(ip(m).logRT_maxkidx(159),:), 'BinWidth', bwrt,...
        'FaceColor', options.col.cb(3,:))
    ylim([0 100])
    xlim([-50 50])
    ylabel('counts')
    if m == 7
        xtxt = ['log($y_{rt,sim}$)'];
        xlabel(xtxt, 'Interpreter', 'Latex')
    end
    ax = gca; ax.FontSize = 12;
    legend('max var(log($y_{rt,sim}$))','median var(log($y_{rt,sim}$))','min var(log($y_{rt,sim}$))',...
        'Interpreter','latex', 'FontSize', 9,...
        'Location', 'NorthEast')
    title(txt)
    f_idx = f_idx+1;
end
figdir = fullfile(saveDir, 'figures', 'paper',...
    ['figS2c_initial_prior_pred_distr_log_yhat_rt_m5_m7_rev']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;





%__________________________________________________________________________
%% S6 (i): subject-level LME differences (M1-M3)
figure
bar(res.main.F(1,:)-res.main.F(3,:), 'k')
camroll(90)
% xlabel('subject')
% ylabel('approximate LME difference (M1-M3)')
ax = gca; ax.FontSize = 12;
figdir = fullfile(saveDir, 'figures', 'paper',...
    ['figS6A_indiv_LME_diff_M1_M3']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;


%% S6 (ii): repeat RFX BMS without M1

% remove M1
res.main.F6 = res.main.F(2:7,:);
size(res.main.F6)
options.fam6.bmc_opt = options.fam.bmc_opt;
options.fam6.bmc_opt.families = {[1 2 3]  [4 5 6 ]};
options.fam6.bmc_opt.families

% perform family level rfx BMS
[res.main.fam6.posterior, res.main.fam6.out] = VBA_groupBMC(res.main.F6, options.fam6.bmc_opt); % run family-level 
res.main.fam6.out.families.ep
res.main.fam6.out.families.Ef

% rfx BMS (individual model level)
[res.main.indiv6.posterior, res.main.indiv6.out] = VBA_groupBMC(res.main.F6, res.rec.bmc.opt);
res.main.indiv6.out.pxp
res.main.indiv6.out.Ef

% FIG 7A (6 models): family level rfx BMS (RQ1)
% XP (exceedance probabilities)
figure
bar(res.main.fam6.out.families.ep, 'k')
ax1 = gca;
ax1.XTick = [1 2];
ax1.XTickLabel = {};%{'M2-M4' 'M5-M7'};
% ylabel('XP')
ylim([0 1])
ax = gca; ax.FontSize = 20;
figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig7A_RQ1_fam_rfx_bms_XP_6mod_rev']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;
% Ef (expected frequencies)
figure
bar(res.main.fam6.out.families.Ef, 'k')
ax1 = gca;
ax1.XTick = [1 2];
ax1.XTickLabel = {};%{'M2-M4' 'M5-M7'};
% ylabel('Ef')
ylim([0 1])
ax = gca; ax.FontSize = 20;
figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig7A_RQ1_fam_rfx_bms_Ef_6mod_rev']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

% FIG 7B: indiv model level RFX BMS (RQ2)
% PXP
figure
bar(res.main.indiv6.out.pxp, 'k')
ax1 = gca;
ax1.XTick = [1 2 3 4 5 6];
ax1.XTickLabel = {'M2' 'M3' 'M4' 'M5' 'M6' 'M7'};
% ylabel('PXP')
ylim([0 1])
ax = gca; ax.FontSize = 20;
figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig7B_RQ2_rfx_bms_PXP_6mod_rev']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;
% Ef
figure
bar(res.main.indiv6.out.Ef, 'k')
ax1 = gca;
ax1.XTick = [1 2 3 4 5 6];
ax1.XTickLabel = {'M2' 'M3' 'M4' 'M5' 'M6' 'M7'};
% ylabel('Ef')
ylim([0 1])
ax = gca; ax.FontSize = 20;
figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig7B_RQ2_rfx_bms_Ef_6mod_rev']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;




%__________________________________________________________________________
%% save & disp

% save prior pred distr stats
res.sim.ep = ep;
res.pilot.sim.ip = ip;

save_path = fullfile(saveDir, 'results', 'main', ['main_results_rev']);
save(save_path, '-struct', 'res', '-v7.3');


disp('all paper figures created & saved.')

end
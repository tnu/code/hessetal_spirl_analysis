This document contains a summary of the code review performed by Jakob Heinzle.

The goals of the code review were defined by Alex Hess and Jakob Heinzle.
The first round of code review was finished on 25.09.2023 by Jakob Heinzle. 
See results below.

# Code review overview

## Basics

-   The code review was based on the instructions in README.md of the main branch
at commit e85be42d8234ed6fe82367b7a4c1e2a8e67fb601.

## Code Setup for Code Review

1.   Created a new branch codeReview where all changes related to the code review are stored.
     Use that branch for all code review activities.
2.   Install relevant software on Euler

## Running Code

Run the analysis on Euler according to description from Alex Hess.

In this code review the following parts of the Code are reviewed:
1. Complete run of analysis
2. Sanity checks for analysis simulations
3. Sanity checks for analysis of participant data.

## Objectives for part 1 (Checkpoints)

1. Does pipeline run through for different user on fresh copy of data?
    :white_check_mark: Ran through.
2. Are the final results the same as in the paper draft.
    :white_check_mark: Went through it with Alex Hess.

## Objectives for part 2 (Checkpoints)
1. Indexing of models correct?
    Compared models in table of draft with mX_logrt_linear_binary.
    Could not spot any error in indexing of models. :white_check_mark:
    Checked model order and parameters. All is in line with the newest version of the table.
2. Indexing of parameters correct?
    Looks good. consistent across simulation and main data. In line with paper table for winning model M1. :white_check_mark:
3. Correct parameters for parameter recovery?
    Checked file job_runner_6_rec_analysis.m: All indexing looks correct. :white_check_mark:




## Objectives for part 3 (Checkpoints)
1. Check data: :white_check_mark:
    - Is the correct data used? Compare with raw data before preprocessing.
    :white_check_mark: Checked choice and RT data for two subjects (Nr 5 and Nr58 of final data set. Data are equal.)
2. Check final F matrix and RFX routine.
    :white_check_mark:  Looks all good.
3. Check statistical tests for parameters.
    :white_check_mark: Looks good.

Other points: None.

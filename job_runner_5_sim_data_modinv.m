function [] = job_runner_5_sim_data_modinv(EULER, n, m, i)
% [] = job_runner_5_sim_data_modinv(EULER, n, m, i)
%
% Inverts model i on simulated subject n created with model m under the
% pilot priors.
%
% INPUT
%   EULER        binary           Binary indicator variable
%   n            integer          Integer indicating synthetic subject idx
%   m            integer          Integer for model used to create data
%   i            integer          Integer for model used to fit data
%
%   OPTIONAL:
%
% OUTPUT    
%   argout       type
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

%% setup path
saveDir = spirl_setup_paths(EULER);

%% load analysis specifications
options = load(fullfile(saveDir, 'results', ['options']));
rng(123, 'twister');
options.rng.settings = rng;

%% load model space for analysis of the main data set
main = load(fullfile(saveDir, 'results', 'main', ['model_space']));

%% load simulated data
sim = load(fullfile(saveDir, 'results', 'sim', 'indiv', ['simulated_data']));

%% create res struct
res = struct();
res.main = main;
res.sim = sim;

%% model inversion
% seed for multistart optim
options.opt_config.seedRandInit = options.rng.settings.State(options.rng.idx, 1);
% fit model
est = tapas_fitModel(res.sim.sub(n,m).data.y,... % responses
    res.sim.sub(n,m).data.u,... % input sequence
    res.main.ModSpace(i).prc_config,... %Prc fitting model
    res.main.ModSpace(i).obs_config,... %Obs fitting model
    options.opt_config); %opt algo

%% save model fit as struct
save_path = fullfile(saveDir, 'results', 'sim', 'indiv', ['sub', num2str(n)],...
    ['sim_mod', num2str(m), '_est_mod', num2str(i)]);
save(save_path, '-struct', 'est');

end
function [] = job_runner_7_family_power(EULER)
% [] = job_runner_7_family_power(EULER)
%
% Create synthetic data for family-level recovery analysis.
%
% INPUT
%   EULER        binary           Binary indicator variable
%
%   OPTIONAL:
%
% OUTPUT    
%   argout       type
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

%% setup path
saveDir = spirl_setup_paths(EULER);

%% load analysis specifications
options = load(fullfile(saveDir, 'results', ['options']));
rng(123, 'twister');
options.rng.settings = rng;

%% load model space for analysis of the main data set
main = load(fullfile(saveDir, 'results', 'main', ['model_space']));

%% load recovery analysis results
rec = load(fullfile(saveDir, 'results', 'sim', 'indiv', ['recovery_analysis']));

%% create res struct
res = struct();
res.main = main;
res.rec = rec;

%% get approx LME values from indiv model level recovery analysis 
% (format: nMod x nSub)
fam_opts_tab = struct2table(options.sim.fam.opts);
for m = 1:size(res.main.ModSpace, 2)
    fam.m(m).F(1:fam_opts_tab.size_families(1,1),:) = res.rec.model(m).LME(:,1:fam_opts_tab.size_families(1,1))';
    fam.m(m).F(sum(fam_opts_tab.size_families(1,:))-fam_opts_tab.size_families(1,2)+1:sum(fam_opts_tab.size_families(1,:)),:) = res.rec.model(m).LME(:,size(res.main.ModSpace,2)-fam_opts_tab.size_families(1,2)+1:size(res.main.ModSpace,2))';
end
% prepare vector of simSub idx
modA = NaN(ceil(max(fam_opts_tab.n_subject)*max(fam_opts_tab.freq_family_B)), max(fam_opts_tab.n_jobs_sim)); %.n_simulation));
modB = NaN(ceil(max(fam_opts_tab.n_subject)*max(fam_opts_tab.freq_family_B)), max(fam_opts_tab.n_jobs_sim)); %.n_simulation));
subA = NaN(ceil(max(fam_opts_tab.n_subject)*max(fam_opts_tab.freq_family_B)), max(fam_opts_tab.n_jobs_sim)); %.n_simulation));
subB = NaN(ceil(max(fam_opts_tab.n_subject)*max(fam_opts_tab.freq_family_B)), max(fam_opts_tab.n_jobs_sim)); %.n_simulation));
for sub = 1:ceil(max(fam_opts_tab.n_subject)*max(fam_opts_tab.freq_family_B))
    for sim = 1:max(fam_opts_tab.n_jobs_sim) %.n_simulation)
        modA(sub,sim) = randi(fam_opts_tab.size_families(1,1));
        modB(sub,sim) = fam_opts_tab.size_families(1,1) + randi(fam_opts_tab.size_families(1,2));
        subA(sub,sim) = randi(options.sim.indiv.nS);
        subB(sub,sim) = randi(options.sim.indiv.nS);
    end
end

%% prepare logpp matrices for different simulation parameters
for i = 1:numel(options.sim.fam.opts)
    % options of family level recovery analysis
    opt = options.sim.fam.opts(i);
    for j = 1 : opt.n_jobs_sim %n_simulation
        % generate groups of F values according to family freq
        nA = opt.n_subject * (1 - opt.freq_family_B); %def nS belonging to fam A (true model)
        for iS = 1 : opt.n_subject
            % get idx of winning fam & model for sub iS
            if iS <= nA %famA
                modIdx = modA(iS,j);
                randSub = subA(iS,j);
            else %famB
                modIdx = modB(ceil(iS-nA),j);
                randSub = subB(ceil(iS-nA),j);
            end
            % get F values of randomly sampled sub from winning model
            logpp = fam.m(modIdx).F(:,randSub); % F for all models for this sub
            F(:,iS) = logpp;
        end
        % store F
        fam.elem(i,j).F = F;
        % clear vars
        clear F;
    end
end

%% save family power specs as struct
save_path = fullfile(saveDir, 'results', 'sim', 'family', ['family_power_setup']);
save(save_path, '-struct', 'fam');

disp('family power setup successfully created.')

end
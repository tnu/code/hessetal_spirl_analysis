function [] = job_runner_2_pilots_modinv(EULER, n, m)
% [] = job_runner_2_pilots_modinv(EULER, n, m)
%
% Inverts model m on data from participant n of the pilot data set.
%
% INPUT
%   EULER        binary           Binary indicator variable
%   n            integer          Integer indicating participant index
%   m            integer          Integer indicating model index
%
%   OPTIONAL:
%
% OUTPUT    
%   argout       type
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

%% setup path
saveDir = spirl_setup_paths(EULER);

%% load analysis specifications
options = load(fullfile(saveDir, 'results', ['options']));
rng(123, 'twister');
options.rng.settings = rng;

%% load pilot data & model space
pilot = load(fullfile(saveDir, 'results', 'pilots', ['init_model_space']));

%% model inversion
% seed for multistart optim
options.opt_config.seedRandInit = options.rng.settings.State(options.rng.idx, 1);
est = tapas_fitModel(pilot.SPIRL(n).y,... % responses
    pilot.SPIRL(n).u,... % input sequence
    pilot.ModSpace(m).prc_config,... %Prc fitting model
    pilot.ModSpace(m).obs_config,... %Obs fitting model
    options.opt_config); %opt algo

%% save model fit as struct
save_path = fullfile(saveDir, 'results', 'pilots', ['sub', num2str(n)],...
    ['est_mod', num2str(m)]);
save(save_path, '-struct', 'est');

end
function [] = job_runner_3_pilots_priors(EULER)
% [] = job_runner_3_pilots_priors(EULER)
%
% Estimates sufficient statistics of pilot prior distributions based on MAP
% estimates obtained from model inversion on held-out (pilot) data set.
%
% INPUT
%   EULER        binary           Binary indicator variable
%
%   OPTIONAL:
%
% OUTPUT    
%   argout       type
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

%% setup path
saveDir = spirl_setup_paths(EULER);

%% load analysis specifications
options = load(fullfile(saveDir, 'results', ['options']));
rng(123, 'twister');
options.rng.settings = rng;

%% load data & init model space
pilot = load(fullfile(saveDir, 'results', 'pilots', ['init_model_space']));

%% load results from model inversion
model = struct();
for n = 1:options.pilots.nS
    for m = 1:size(pilot.ModSpace, 2)
        fprintf('current iteration: n=%1.0f, m=%1.0f \n', n,m);
        pilot.est(m,n) = load(fullfile(saveDir, 'results',...
            'pilots', ['sub', num2str(n)], ['est_mod', num2str(m)]));
        
        % prc model
        for j = 1:size(pilot.ModSpace(m).prc_idx,2)
            model(m).prc_est(n,j) = pilot.est(m,n).p_prc.ptrans(pilot.ModSpace(m).prc_idx(j));
            if n == 1
                model(m).prc_priormus(j) = pilot.ModSpace(m).prc_config.priormus(pilot.ModSpace(m).prc_idx(j));
                model(m).prc_priorsas(j) = pilot.ModSpace(m).prc_config.priorsas(pilot.ModSpace(m).prc_idx(j));
            end
        end
        
        %obs model
        for k = 1:size(pilot.ModSpace(m).obs_idx,2)
            model(m).obs_est(n,k) = pilot.est(m,n).p_obs.ptrans(pilot.ModSpace(m).obs_idx(k));
            if n == 1
                model(m).obs_priormus(k) = pilot.ModSpace(m).obs_config.priormus(pilot.ModSpace(m).obs_idx(k));
                model(m).obs_priorsas(k) = pilot.ModSpace(m).obs_config.priorsas(pilot.ModSpace(m).obs_idx(k));
            end
        end           
    end
end

%% estimate pilot priors
for m = 1:size(pilot.ModSpace, 2)
    % prc
    model(m).prc_robmean = NaN(size(model(m).prc_priormus));
    model(m).prc_robvar = NaN(size(model(m).prc_priorsas));
    for j = 1:size(pilot.ModSpace(m).prc_idx,2)
        [model(m).prc_robvar(1,j), model(m).prc_robmean(1,j)] = robustcov(model(m).prc_est(:,j));
    end
    
    % obs
    model(m).obs_robmean = NaN(size(model(m).obs_priormus));
    model(m).obs_robvar = NaN(size(model(m).obs_priorsas));
    for k = 1:size(pilot.ModSpace(m).obs_idx,2)
        [model(m).obs_robvar(1,k), model(m).obs_robmean(1,k)] = robustcov(model(m).obs_est(:,k));
    end
end
% save to struct
pilot.priors.mod = model;

%% create model space for main analysis (pilot priors)

% init model space
main = struct();
main.ModSpace = spirl_setup_model_space();

% set pilot priors
for m = 1:size(main.ModSpace, 2)
    % prc
    for j = 1:size(pilot.ModSpace(m).prc_idx,2)
        main.ModSpace(m).prc_config.priormus(main.ModSpace(m).prc_idx(j)) = ...
            round(model(m).prc_robmean(j), 4);
        main.ModSpace(m).prc_config.priorsas(main.ModSpace(m).prc_idx(j)) = ...
            round(model(m).prc_robvar(j), 4);
    end
    main.ModSpace(m).prc_config = tapas_align_priors_fields(...
        main.ModSpace(m).prc_config);
    % obs
    for k = 1:size(pilot.ModSpace(m).obs_idx,2)
        main.ModSpace(m).obs_config.priormus(main.ModSpace(m).obs_idx(k)) = ...
            round(model(m).obs_robmean(k), 4);
        main.ModSpace(m).obs_config.priorsas(main.ModSpace(m).obs_idx(k)) = ...
            round(model(m).obs_robvar(k), 4);
    end
    main.ModSpace(m).obs_config = tapas_align_priors_fields(...
        main.ModSpace(m).obs_config);
end

%% save results as struct
% pilot priors
save_path = fullfile(saveDir, 'results', 'pilots', ['pilot_priors']);
save(save_path, '-struct', 'pilot');

% model space for analysis of main data sets
save_path = fullfile(saveDir, 'results', 'main', ['model_space']);
save(save_path, '-struct', 'main');

disp('pilot priors successfully estimated.')
disp('ready for simulation analyses and inversion of main data set.')

end
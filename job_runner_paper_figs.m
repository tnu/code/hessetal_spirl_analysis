function [] = job_runner_paper_figs(EULER)
% [] = job_runner_paper_figs(EULER)
%
% Creates figures used for the manuscript.
%
% INPUT
%   EULER        binary           Binary indicator variable
%
%   OPTIONAL:
%
% OUTPUT    
%   argout       type
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

%% setup path
saveDir = spirl_setup_paths(EULER);

%% load analysis specifications
options = load(fullfile(saveDir, 'results', ['options']));
rng(123, 'twister');
options.rng.settings = rng;

%% load final results
res = load(fullfile(saveDir, 'results', 'main', ['main_results']));

%% specify colors
options.col.idx = ones(1,length(res.main.trials));
for i = 1:length(res.main.trials)
    if ismember(res.main.traj.block.code(i),[1,6,8,13]) 
        options.col.idx(i) = 2;
    elseif ismember(res.main.traj.block.code(i),[7])
        options.col.idx(i) = 3;
    end
end
options.col.map = [options.col.tnub; options.col.wh; options.col.gry];

%% get screensize & hardcode figure sizes
scrsz = get(0,'screenSize');
% hard code figure sizes
outerpos1 = [0.275*scrsz(3),0.4*scrsz(4),700,250];
outerpos2 = [0.275*scrsz(3),0.4*scrsz(4),400,400];

%% Fig 1B: Probability Structure of SPIRL Task 
figure(...
    'OuterPosition', outerpos1,...
    'Name', 'Probability Trajectory & Inputs');
% probability trajectory
plot(res.main.traj.probs_green, 'Color', 'k', 'LineWidth',1.5)
% xlabel('Trial')
ylim([-0.1 1.1])
hold on
% inputs
plot(res.main.traj.u, '.', 'Color', 'k') % (1=green fractal, 0=yellow)
% color code different phases
ax = axis;
fill([res.main.trials, fliplr(res.main.trials)],...
    [ax(3)*ones(1,length(res.main.trials)), ax(4)*ones(1,length(res.main.trials))],...
    [options.col.idx, fliplr(options.col.idx)], 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
colormap(options.col.map)
hold off

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig1B_task_prob_traj']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

%% Fig 3: average binary predictions & avg muhat1 of M1
% (= avg correct responses, inverted for blocks with p=0.2)

% collect binary predictions and avg over sub (main data set, N=59)
y_bin = NaN(size(res.main.traj.u,1),options.main.nS);
for n = 1:options.main.nS
    y_bin(:,n) = res.main.SPIRL(n).y(:,1);
end
y_bin_avg = mean(y_bin,2,'omitnan');
y_bin_var = var(y_bin,0,2,'omitnan');
yupper = y_bin_avg + sqrt(y_bin_var);
ylower = y_bin_avg - sqrt(y_bin_var);

% get belief about outcome (muhat1) of M1
m=1;
muhat1 = NaN(size(res.main.traj.u,1),options.main.nS);
for n = 1:options.main.nS
    muhat1(:,n) = res.main.est(m,n).traj.muhat(:,1);
end
muhat1_avg = mean(muhat1,2,'omitnan');
muhat1_var = var(muhat1,0,2,'omitnan');
mh1upper = muhat1_avg + sqrt(muhat1_var);
mh1lower = muhat1_avg - sqrt(muhat1_var);

% create figure 
figure(...
    'OuterPosition', outerpos1,...
    'Name', 'Probability Trajectory & Inputs');
% probability trajectory
plot(res.main.traj.probs_green, 'Color', [0 0 0], 'LineWidth',1)
% xlabel('Trial')
ylim([-0.1 1.1])
hold on
% inputs
plot(res.main.traj.u, '.', 'Color', 'k') % (1=green card, 0=yellow)
% color code different phases
ax = axis;
fill([res.main.trials, fliplr(res.main.trials)],...
    [ax(3)*ones(1,length(res.main.trials)), ax(4)*ones(1,length(res.main.trials))],...
    [options.col.idx, fliplr(options.col.idx)], 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
colormap(options.col.map)
% avg binary predictions +- 1*stdev
plot(1:160,y_bin_avg, 'Color','r', 'LineWidth',1)
fill([res.main.trials, fliplr(res.main.trials)], [yupper', fliplr(ylower')], ...
    'r', 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
% plot avg muhat1 +- 1*stdev
plot(1:160,muhat1_avg, 'Color',options.col.tnub, 'LineWidth',1)
fill([res.main.trials, fliplr(res.main.trials)], [mh1upper', fliplr(mh1lower')], ...
    options.col.tnub, 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
hold off

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig3_traj_avg_bin_muhat1_M1']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

%% Fig 3: plot AVG (+ STD) rt trajectories & fits of M1

% calc avg logRT + std
mean_logRT = mean(res.main.logRT_mat, 2, 'omitnan');
var_logRT = var(res.main.logRT_mat, 0, 2, 'omitnan');
logupper = mean_logRT + sqrt(var_logRT);
loglower = mean_logRT - sqrt(var_logRT);

% get avg logRT fit + std of M1
m = 1;
yhat_mat = NaN(size(res.main.traj.u,1),options.main.nS);
for n = 1:options.main.nS
    yhat_mat(:,n) = res.main.est(m,n).optim.yhat(:,2);
end
mod(m).mean_yhat = mean(yhat_mat, 2, 'omitnan');
mod(m).var_yhat = var(yhat_mat, 0, 2, 'omitnan');
mod(m).upper_yhat = mod(m).mean_yhat + sqrt(mod(m).var_yhat);
mod(m).lower_yhat = mod(m).mean_yhat - sqrt(mod(m).var_yhat);

% figure
figure(...
    'OuterPosition', outerpos1,...
    'Name', 'Probability Trajectory & Inputs');
hold on
% plot avg logRT +- 1*std
plot(res.main.trials, mean_logRT, 'LineWidth', 1.5, 'color', 'r')
fill([res.main.trials, fliplr(res.main.trials)], [(logupper)', fliplr((loglower)')], ...
    'r', 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
% plot avg logRT fit +- 1*std
plot(res.main.trials, mod(m).mean_yhat, 'LineWidth', 1.5, 'Color',options.col.tnub)
fill([res.main.trials, fliplr(res.main.trials)], [(mod(m).upper_yhat)', fliplr((mod(m).lower_yhat)')], ...
    options.col.tnub, 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
ylim([5.3 6.8])
xlim([1 160])
% color code different phases
ax = axis;
fill([res.main.trials, fliplr(res.main.trials)],...
    [ax(3)*ones(1,length(res.main.trials)), ax(4)*ones(1,length(res.main.trials))],...
    [options.col.idx, fliplr(options.col.idx)], 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
colormap(options.col.map)
hold off

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig3_traj_avg_logrt_fits_M1']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

%% Fig 3: average Suprise (be1) about the outcome of M1
% (= avg correct responses, inverted for blocks with p=0.2)

% get belief about outcome (muhat1) of M1
m=1;
muhat1 = NaN(size(res.main.traj.u,1),options.main.nS);
for n = 1:options.main.nS
    muhat1(:,n) = res.main.est(m,n).traj.muhat(:,1);
end
muhat1_avg = mean(muhat1,2,'omitnan');
u = res.main.traj.u;
poo = muhat1_avg.^u.*(1-muhat1_avg).^(1-u); % probability of observed outcome
surp = -log2(poo);

% create figure 
figure(...
    'OuterPosition', outerpos1,...
    'Name', 'Probability Trajectory & Inputs');
% plot avg suprise
plot(1:160,surp, 'Color',options.col.tnub, 'LineWidth',1)
hold on
% color code different phases
ax = axis;
fill([res.main.trials, fliplr(res.main.trials)],...
    [ax(3)*ones(1,length(res.main.trials)), ax(4)*ones(1,length(res.main.trials))],...
    [options.col.idx, fliplr(options.col.idx)], 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
colormap(options.col.map)
hold off

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig3_traj_avg_suprise_M1']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

%% Fig 3: average outcome uncertainty (sa1hat, be2) of M1
% (= avg correct responses, inverted for blocks with p=0.2)

% get belief about outcome (muhat1) of M1
m=1;
sa1hat = NaN(size(res.main.traj.u,1),options.main.nS);
for n = 1:options.main.nS
    sa1hat(:,n) = res.main.est(m,n).traj.sahat(:,1);
end
sa1hat_avg = mean(sa1hat,2,'omitnan');

% create figure 
figure(...
    'OuterPosition', outerpos1,...
    'Name', 'Probability Trajectory & Inputs');
% plot avg sa1hat
plot(1:160,sa1hat_avg, 'Color',options.col.tnub, 'LineWidth',1)
ylim([0 0.4])
hold on
% color code different phases
ax = axis;
fill([res.main.trials, fliplr(res.main.trials)],...
    [ax(3)*ones(1,length(res.main.trials)), ax(4)*ones(1,length(res.main.trials))],...
    [options.col.idx, fliplr(options.col.idx)], 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
colormap(options.col.map)
hold off

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig3_traj_avg_sa1hat_M1']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

%% Fig 3: average informational uncertainty (sa2hat, be3) of M1
% (= avg correct responses, inverted for blocks with p=0.2)

% get belief about outcome (muhat1) of M1
m=1;
sa2hat = NaN(size(res.main.traj.u,1),options.main.nS);
for n = 1:options.main.nS
    sa2hat(:,n) = res.main.est(m,n).traj.sahat(:,2);
end
sa2hat_avg = mean(sa2hat,2,'omitnan');

% create figure 
figure(...
    'OuterPosition', outerpos1,...
    'Name', 'Probability Trajectory & Inputs');
% plot avg sa2hat
plot(1:160,sa2hat_avg, 'Color',options.col.tnub, 'LineWidth',1)
hold on
% color code different phases
ax = axis;
fill([res.main.trials, fliplr(res.main.trials)],...
    [ax(3)*ones(1,length(res.main.trials)), ax(4)*ones(1,length(res.main.trials))],...
    [options.col.idx, fliplr(options.col.idx)], 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
colormap(options.col.map)
hold off

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig3_traj_avg_sa2hat_M1']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

%% Fig 3: average volatility of the env (exp(mu3hat), be4) of M1
% (= avg correct responses, inverted for blocks with p=0.2)

% get belief about outcome (muhat1) of M1
m=1;
mu3hat = NaN(size(res.main.traj.u,1),options.main.nS);
for n = 1:options.main.nS
    mu3hat(:,n) = res.main.est(m,n).traj.muhat(:,3);
end
exp_mu3hat = exp(mu3hat);
exp_mu3hat_avg = mean(exp_mu3hat,2,'omitnan');

% create figure 
figure(...
    'OuterPosition', outerpos1,...
    'Name', 'Probability Trajectory & Inputs');
% plot avg exp(mu3hat)
plot(1:160,exp_mu3hat_avg, 'Color',options.col.tnub, 'LineWidth',1)
hold on
% color code different phases
ax = axis;
fill([res.main.trials, fliplr(res.main.trials)],...
    [ax(3)*ones(1,length(res.main.trials)), ax(4)*ones(1,length(res.main.trials))],...
    [options.col.idx, fliplr(options.col.idx)], 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
colormap(options.col.map)
hold off

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig3_traj_avg_exp_mu3hat_M1']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

%% Fig 4A: inverted (p=0.2) avg incorrect predictions vs avg |delta_1| from eHGF-3b (M1)

% calc avg correct preds
y_bin_avg_corr = y_bin_avg;
y_bin_avg_corr(res.main.traj.u==0) = 1-y_bin_avg_corr(res.main.traj.u==0);
% invert avg correct preds
y_bin_avg_corr_inverted = 1-y_bin_avg_corr;

% get absolute value of outcome PE trajectories at of M1
m=1;
da1 = NaN(size(res.main.traj.u,1),options.main.nS);
for n = 1:options.main.nS
    da1(:,n) = abs(res.main.est(m,n).traj.da(:,1));
end
da1_avg = mean(da1,2,'omitnan');

% create figure
figure(...
    'OuterPosition', outerpos1,...
    'Name', 'Probability Trajectory & Inputs');
% probability trajectory
plot(res.main.traj.probs_green, 'Color', [0 0 0], 'LineWidth',1)
ylim([-0.1 1.1])
hold on
% inputs
plot(res.main.traj.u, '.', 'Color', 'k') % (1=green card, 0=yellow)
% color code different phases
ax = axis;
fill([res.main.trials, fliplr(res.main.trials)],...
    [ax(3)*ones(1,length(res.main.trials)), ax(4)*ones(1,length(res.main.trials))],...
    [options.col.idx, fliplr(options.col.idx)], 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
colormap(options.col.map)
% plot avg correct predictions (inverted)
plot(1:160,y_bin_avg_corr_inverted, 'Color','r', 'LineWidth',1)
% plot avg da1
plot(1:160,da1_avg, 'Color',options.col.tnub, 'LineWidth',1)
hold off

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig4A_traj_avg_corr_da1_M1']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

%% Fig 4B: logRT histogram (main data set)
figure()
histogram(res.main.logRT_mat, 15, 'EdgeColor', 'r', 'FaceColor', 'r')
ax = gca; ax.FontSize = 14;

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig4B_logRT_hist_M1']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

%% Fig 4C: logRT model fit residuals (M1)

% get residuals of logRT fits
m=1;
logRT_res = NaN(size(res.main.traj.u,1),options.main.nS);
for n = 1:options.main.nS
    logRT_res(:,n) = res.main.est(m,n).optim.res(:,2);
end

% create figure
figure()
% histogram
histogram(logRT_res, 'EdgeColor', options.col.tnub,...
    'FaceColor', options.col.tnub)
ax = gca; ax.FontSize = 14;

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig4C_logRT_res_hist_M1']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

%% FIG 5A: empirical prior distributions (M1)
m = 1;

% x grid
x_min = -30;
x_max = 30;
x = x_min:0.1:x_max;

% get pdf of prc model params
for j = 1:size(res.main.ModSpace(m).prc_idx,2)
    prc(j).y = normpdf(x, res.pilot.priors.mod(m).prc_robmean(1,j),...
        sqrt(res.pilot.priors.mod(m).prc_robvar(1,j)));
    prc(j).y_prior = normpdf(x, res.pilot.priors.mod(m).prc_priormus(j),...
        sqrt(res.pilot.priors.mod(m).prc_priorsas(j)));
end

% get pdf of obs model params
for k = 1:size(res.main.ModSpace(m).obs_idx,2)
    obs(k).y = normpdf(x, res.pilot.priors.mod(m).obs_robmean(1,k),...
        sqrt(res.pilot.priors.mod(m).obs_robvar(1,k)));
    obs(k).y_prior = normpdf(x, res.pilot.priors.mod(m).obs_priormus(k),...
        sqrt(res.pilot.priors.mod(m).obs_priorsas(k)));
end

% plot om2
j = 1;
figure
plot(x, prc(j).y, 'k', 'LineWidth', 1)
hold on
plot(x, prc(j).y_prior, 'k--')
plot(res.pilot.priors.mod(m).prc_est(:,j), -0.05, 'k.')
hold off
ylim([-0.1 1]) %0.45
xlim([-10 5])
ax = gca; ax.FontSize = 20;
figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig5A_empirical_priors_prc_om2_M1']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

% plot om3
j = 2;
figure
plot(x, prc(j).y, 'k', 'LineWidth', 1)
hold on
plot(x, prc(j).y_prior, 'k--')
plot(res.pilot.priors.mod(m).prc_est(:,j), -0.05, 'k.')
hold off
ylim([-0.1 1]) %0.45
xlim([-6 9])
ax = gca; ax.FontSize = 20;
figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig5A_empirical_priors_prc_om3_M1']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

% plot zeta
k = 1;
figure
plot(x, obs(k).y, 'k', 'LineWidth', 1)
hold on
plot(x, obs(k).y_prior, 'k--')
plot(res.pilot.priors.mod(m).obs_est(:,k), -0.05, 'k.')
hold off
ylim([-0.1 1])
xlim([-3 8])
ax = gca; ax.FontSize = 20;
figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig5A_empirical_priors_obs_ze_M1']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

% plot beta0 (intercept)
k = 2;
figure
plot(x, obs(k).y, 'k', 'LineWidth', 1)
hold on
plot(x, obs(k).y_prior, 'k--')
plot(res.pilot.priors.mod(m).obs_est(:,k), -0.05, 'k.')
hold off
ylim([-0.1 1])
xlim([2 9])
ax = gca; ax.FontSize = 20;
figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig5A_empirical_priors_obs_be0_M1']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

% plot beta1 (Surprise)
k = 3;
figure
plot(x, obs(k).y, 'k', 'LineWidth', 1)
hold on
plot(x, obs(k).y_prior, 'k--')
plot(res.pilot.priors.mod(m).obs_est(:,k), -0.05, 'k.')
hold off
ylim([-0.1 4])
xlim([-5 5])
ax = gca; ax.FontSize = 20;
figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig5A_empirical_priors_obs_be1_M1']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

% plot beta2 (sa1hat)
k = 4;
figure
plot(x, obs(k).y, 'k', 'LineWidth', 1)
hold on
plot(x, obs(k).y_prior, 'k--')
plot(res.pilot.priors.mod(m).obs_est(:,k), -0.05, 'k.')
hold off
ylim([-0.1 1.1])
xlim([-5 5])
ax = gca; ax.FontSize = 20;
figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig5A_empirical_priors_obs_be2_M1']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

% plot beta3 (sa2hat)
k = 5;
figure
plot(x, obs(k).y, 'k', 'LineWidth', 1)
hold on
plot(x, obs(k).y_prior, 'k--')
plot(res.pilot.priors.mod(m).obs_est(:,k), -0.05, 'k.')
hold off
ylim([-0.1 2.5])
xlim([-5 5])
ax = gca; ax.FontSize = 20;
figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig5A_empirical_priors_obs_be3_M1']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

% plot beta4 (exp(mu3hat))
k = 6;
figure
plot(x, obs(k).y, 'k', 'LineWidth', 1)
hold on
plot(x, obs(k).y_prior, 'k--')
plot(res.pilot.priors.mod(m).obs_est(:,k), -0.05, 'k.')
hold off
ylim([-0.1 2.5])
xlim([-5 5])
ax = gca; ax.FontSize = 20;
figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig5A_empirical_priors_obs_be4_M1']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

% plot Sigma
k = 7;
figure
plot(x, obs(k).y, 'k', 'LineWidth', 1)
hold on
plot(x, obs(k).y_prior, 'k--')
plot(res.pilot.priors.mod(m).obs_est(:,k), -0.05, 'k.')
hold off
ylim([-0.1 1])
xlim([-8 3])
ax = gca; ax.FontSize = 20;
figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig5A_empirical_priors_obs_Sa_M1']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

%% FIG 5B: prior pred densities under empirical priors, mu1hat (M1)

% collect mu1hat
m=1;
mu1hat_sim = NaN(size(res.main.traj.u,1),options.main.nS);
ybin_sim = NaN(size(res.main.traj.u,1),options.main.nS);
for n = 1:options.sim.indiv.nS
    mu1hat_sim(:,n) = res.sim.sub(n,m).data.traj.muhat(:,1);
    ybin_sim(:,n) = res.sim.sub(n,m).data.y(:,1);
end
avg_mu1hat_sim = mean(mu1hat_sim, 2, 'omitnan');
avg_ybin_sim = mean(ybin_sim, 2, 'omitnan');

% create figure
figure(...
    'OuterPosition', outerpos1,...
    'Name', 'M1 mu1hat (prior pred dens)');
plot(mu1hat_sim, 'color', [options.col.tnub 0.1])
hold on
plot(avg_mu1hat_sim, 'color', options.col.tnub, 'LineWidth', 1.5)
plot(avg_ybin_sim, 'color', options.col.tnuy, 'LineWidth', 1)
% probability trajectory
plot(res.main.traj.probs_green, 'Color', [0 0 0], 'LineWidth',1)
ylim([-0.1 1.1])
% inputs
plot(res.main.traj.u, '.', 'Color', 'k') % (1=green card, 0=yellow)
% color code different phases
ax = axis;
fill([res.main.trials, fliplr(res.main.trials)],...
    [ax(3)*ones(1,length(res.main.trials)), ax(4)*ones(1,length(res.main.trials))],...
    [options.col.idx, fliplr(options.col.idx)], 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
colormap(options.col.map)
hold off

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig5B_empirical_prior_pred_dens_mu1hat_M1']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

%% FIG 5C: prior pred densities under empirical priors, log(yhat_rt) (M1)

% collect simulated logRTs
m=1;
logRT_sim = NaN(size(res.main.traj.u,1),options.main.nS);
for n = 1:options.sim.indiv.nS
    logRT_sim(:,n) = res.sim.sub(n,m).data.y(:,2);
end
avg_logRT_sim = mean(logRT_sim, 2, 'omitnan');

% create figure
figure(...
    'OuterPosition', outerpos1,...
    'Name', 'M1 log(yhat_rt) (prior pred dens)');
plot(logRT_sim, 'color', [options.col.tnub 0.1])
hold on
plot(1:160, ones(160,1)*log(1700), '--k')
plot(1:160, ones(160,1)*log(100), '--k')
plot(avg_logRT_sim, 'color', options.col.tnub, 'LineWidth', 1.5)
ylim([2 11])
% color code different phases
ax = axis;
fill([res.main.trials, fliplr(res.main.trials)],...
    [ax(3)*ones(1,length(res.main.trials)), ax(4)*ones(1,length(res.main.trials))],...
    [options.col.idx, fliplr(options.col.idx)], 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
colormap(options.col.map)
hold off

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig5C_empirical_prior_pred_dens_log_yhat_rt_M1']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

%% FIG 6A: family level recovery analysis

% get fam frequencies, XP & Ef values
sub_idx = options.sim.fam.opts(1).n_subject;
fam_freq = unique(res.sim.fam.t.freq_family_B(ismember(res.sim.fam.t.n_subject, sub_idx)));
Ef_famA = [];
xp_famA = [];
fam_freq_A = [];
for i = 1:numel(fam_freq)
    Ef_famA = [Ef_famA; res.sim.fam.t.Ef_famA(ismember(res.sim.fam.t.n_subject, sub_idx) & res.sim.fam.t.freq_family_B==fam_freq(i))];
    xp_famA = [xp_famA; res.sim.fam.t.xp_famA(ismember(res.sim.fam.t.n_subject, sub_idx) & res.sim.fam.t.freq_family_B==fam_freq(i))];    
    fam_freq_A = [fam_freq_A; (1-fam_freq(i))*ones(size(res.sim.fam.t.Ef_famA(ismember(res.sim.fam.t.n_subject, sub_idx) & res.sim.fam.t.freq_family_B==fam_freq(i))))];
    avgEf(i) = mean(res.sim.fam.t.Ef_famA(ismember(res.sim.fam.t.n_subject, sub_idx) & res.sim.fam.t.freq_family_B==fam_freq(i)));
    stdEf(i) = std(res.sim.fam.t.Ef_famA(ismember(res.sim.fam.t.n_subject, sub_idx) & res.sim.fam.t.freq_family_B==fam_freq(i)));
    avgXP(i) = mean(res.sim.fam.t.xp_famA(ismember(res.sim.fam.t.n_subject, sub_idx) & res.sim.fam.t.freq_family_B==fam_freq(i)));
    stdXP(i) = std(res.sim.fam.t.xp_famA(ismember(res.sim.fam.t.n_subject, sub_idx) & res.sim.fam.t.freq_family_B==fam_freq(i)));
end

% avg XP & std XP
upper = avgXP + stdXP;
lower = avgXP - stdXP;

% create figure
figure
hold on
% s2 = scatter(fam_freq_A, xp_famA, [], 'k', 'filled');
% alpha(s2, 0.002)
plot(1-fam_freq, avgXP, 'LineWidth', 2, 'Color', 'k');
ax = gca; ax.FontSize = 20;
fill([1-fam_freq', fliplr((1-fam_freq'))], [(upper), fliplr((lower))], ...
         'k', 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
hold off

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig6A_RQ1_fam_rec_XP']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

% avg Ef +- stdev (famA=1-famB, sub=60, 'classic')
upper = avgEf + stdEf;
lower = avgEf - stdEf;

% create figure
figure
hold on
% s1 = scatter(fam_freq_A, Ef_famA, [], 'k', 'filled');
% alpha(s1, 0.002);
plot(1-fam_freq, avgEf, 'LineWidth', 2, 'Color', 'k');
ax = gca; ax.FontSize = 20;
fill([1-fam_freq', fliplr((1-fam_freq'))], [(upper), fliplr((lower))],...
    'k', 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
ylim([0 1])
hold off

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig6A_RQ1_fam_rec_Ef']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

%% FIG 6B: model ident (LME winner classification)

% axis labels
for m = 1:size(res.main.ModSpace,2)
    mod_nr{m} = ['M', num2str(m)];
end

figure
bwr = @(n)interp1([1 2], [options.col.wh; [0 0 0]], linspace(1, 2, n), 'linear');
imagesc(res.rec.class.percLMEwinner)
colormap(bwr(64));
colorbar;
set(gca, 'clim', [0 1])
ax = gca; ax.FontSize = 20;
ax.XTick = [1:size(res.main.ModSpace,2)];
ax.XTickLabel = mod_nr;
ax.YTick = [1:size(res.main.ModSpace,2)];
ax.YTickLabel = mod_nr;

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig6B_RQ2_model_rec_LME']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

%% FIG 6B: model ident (rfx bms, PXP)

% axis labels
for m = 1:size(res.main.ModSpace,2)
    mod_nr{m} = ['M', num2str(m)];
end

figure
bwr = @(n)interp1([1 2], [options.col.wh; [0 0 0]], linspace(1, 2, n), 'linear');
imagesc(res.rec.bmc.rfx.pxp)
colormap(bwr(64));
colorbar;
set(gca, 'clim', [0 1])
ax = gca; ax.FontSize = 20;
ax.XTick = [1:size(res.main.ModSpace,2)];
ax.XTickLabel = mod_nr;
ax.YTick = [1:size(res.main.ModSpace,2)];
ax.YTickLabel = mod_nr;

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig6B_RQ2_model_rec_PXP']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

%% FIG 6B: Color bar only

figure
colormap(bwr(64));
colorbar;
ax = gca; ax.FontSize = 20;
figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig6B_RQ2_colorbar']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

%% FIG 6C: Parameter Recovery (M1)
m = 1;
npars = length(res.main.ModSpace(m).prc_idx)+length(res.main.ModSpace(m).obs_idx);
nx = ceil(sqrt(npars));
ny = round(sqrt(npars));

for j = 1:npars
    figure
    if j > length(res.main.ModSpace(m).prc_idx) %% all obs param
        k = j-length(res.main.ModSpace(m).prc_idx);
        scatter(res.rec.param.obs(m).sim(:,k), res.rec.param.obs(m).est(:,k), 15, 'k', 'filled');
        if k == 1
            param_name = 'logze';
        elseif k == size(res.main.ModSpace(m).obs_idx, 2)
            param_name = 'logSa';
        elseif k == 2
            param_name = 'be0';
        elseif k == 3
            param_name = 'be1';
        elseif k == 4
            param_name = 'be2';
        elseif k == 5
            param_name = 'be3';
        elseif k == 6
            param_name = 'be4';
        end
    else
        scatter(res.rec.param.prc(m).sim(:,j), res.rec.param.prc(m).est(:,j), 15, 'k', 'filled');       
        if j == 1
            param_name = 'om2';
        elseif j == 2
            param_name = 'om3';
        end
    end
    hline = refline(1,0);
    hline.Color = 'k';   
    ax = gca; ax.FontSize = 20;
    figdir = fullfile(saveDir, 'figures', 'paper',...
        ['fig6C_RQ2_param_rec_M1_', param_name]);
    print(figdir, '-dpng');
    print(figdir, '-dsvg');
    close;
end

%% FIG 6D: Parameter Recovery (omega3)
j = 2;
npars = length(res.main.ModSpace(m).prc_idx)+length(res.main.ModSpace(m).obs_idx);
nx = ceil(sqrt(npars));
ny = round(sqrt(npars));

for m = 1:size(res.main.ModSpace,2)
    figure
    scatter(res.rec.param.prc(m).sim(:,j), res.rec.param.prc(m).est(:,j), 15, 'k', 'filled');       
    param_name = 'om3';
    hline = refline(1,0);
    hline.Color = 'k';
    ax = gca; ax.FontSize = 20;
   
    figdir = fullfile(saveDir, 'figures', 'paper',...
        ['fig6D_RQ2_param_rec_M', num2str(m), '_', param_name]);
    print(figdir, '-dpng');
    print(figdir, '-dsvg');
    close;
end

%% FIG 7A: family level rfx BMS (RQ1)

% XP (exceedance probabilities)
figure
bar(res.main.fam.out.families.ep, 'k')
ax1 = gca;
ax1.XTick = [1 2];
ax1.XTickLabel = {}; %{'Informed RT family' 'Uninformed family'};
% ylabel('exceedance probability')
ylim([0 1])
ax = gca; ax.FontSize = 20;

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig7A_RQ1_fam_rfx_bms_XP']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

% Ef (expected frequencies)
figure
bar(res.main.fam.out.families.Ef, 'k')
ax1 = gca;
ax1.XTick = [1 2];
ax1.XTickLabel = {}; %{'Informed RT family' 'Uninformed family'};
% ylabel('expected frequency')
ylim([0 1])
ax = gca; ax.FontSize = 20;

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig7A_RQ1_fam_rfx_bms_Ef']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

%% FIG 7B: indiv model level RFX BMS (RQ2)

% PXP
figure
bar(res.main.indiv.out.pxp, 'k')
ax1 = gca;
ax1.XTick = [1 2 3 4 5 6 7];
ax1.XTickLabel = {'M1' 'M2' 'M3' 'M4' 'M5' 'M6' 'M7'};
ylim([0 1])
ax = gca; ax.FontSize = 20;

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig7B_RQ2_rfx_bms_PXP']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

% Ef
figure
bar(res.main.indiv.out.Ef, 'k')
ax1 = gca;
ax1.XTick = [1 2 3 4 5 6 7];
ax1.XTickLabel = {'M1' 'M2' 'M3' 'M4' 'M5' 'M6' 'M7'};
ylim([0 1])
ax = gca; ax.FontSize = 20;

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig7B_RQ2_rfx_bms_Ef']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

%% Fig 7C: MAP estimates vs. init prior means (M1)
m=1;
npars = 4;
for j = 1:npars
    figure
    k = j+2;
    raincloud_plot(res.main.param.obs(m).est(:,k), 'box_on', 1, ...
        'color', [0 0 0], 'alpha', 1,...
        'box_dodge', 1, 'box_dodge_amount', .15, 'dot_dodge_amount', .15,...
        'box_col_match', 0, 'lwr_bnd', 0.3);
    hold on
    xline(res.pilot.ModSpace(m).obs_config.priormus(k)) % initial priors
    if k == 1
        param_name = 'logze';
    elseif k == size(res.main.ModSpace(m).obs_idx, 2)
        param_name = 'logSa';
    elseif k == 2
        param_name = 'be0';
    elseif k == 3
        param_name = 'be1';
    elseif k == 4
        param_name = 'be2';
    elseif k == 5
        param_name = 'be3';
    elseif k == 6
        param_name = 'be4';
    end
    hold off
    ax = gca; ax.FontSize = 20;
    figdir = fullfile(saveDir, 'figures', 'paper',...
        ['fig7C_RQ2_MAP_vs_init_priormu_M1_', param_name]);
    print(figdir, '-dpng');
    print(figdir, '-dsvg');
    close;
end

%%
addpath('C:\Users\alhess\Documents\Code\povilaskarvelis-DataViz-95e0fe0\daboxplot\')
figure
daboxplot(res.main.param.obs(m).est(:,3:6), ...
    'whiskers',0,'scatter',1,'scattersize',25,'scatteralpha',0.6,...
    'withinlines',1,'outliers',0)
%%
figure
daboxplot(res.main.param.obs(m).est(:,3:6) ./ max(abs(res.main.param.obs(m).est(:,3:6))), ...
    'whiskers',0,'scatter',1,'scattersize',25,'scatteralpha',0.6,...
    'withinlines',1,'outliers',0)
%%
figure
daboxplot(res.main.param.obs(m).est ./ max(abs(res.main.param.obs(m).est)), ...
    'whiskers',0,'scatter',1,'scattersize',25,'scatteralpha',0.6,...
    'withinlines',1,'outliers',0)

%% connecting indiv est
addpath('C:\Users\alhess\Documents\Code\povilaskarvelis-DataViz-95e0fe0\daviolinplot\')
figure
yline(0)
hold on; 
daviolinplot(res.main.param.obs(m).est(:,3:6), ...
    'whiskers',0,'scatter',1,'scattersize',25,'scatteralpha',0.6,...
    'withinlines',1,'outliers',0)
%%
figure
daviolinplot(res.main.param.obs(m).est ./ max(abs(res.main.param.obs(m).est)), ...
    'whiskers',0,'scatter',1,'scattersize',25,'scatteralpha',0.6,...
    'withinlines',1,'outliers',0)


%% Fig 8A: Posterior prective checks M1 - y_bin (adjusted correctness)
m=1;

% collect logLL as measure for goodness of fit
for n = 1:options.main.nS
    for m = 1:size(res.main.ModSpace, 2)
        % log ll matrix
        res.main.Ll(n,m) = -res.main.est(m,n).optim.negLl;
        Llsplit = sum(res.main.est(m,n).optim.trialLogLlsplit, 'omitnan');
        res.main.Ll_bin(n,m) = Llsplit(1,1);
        res.main.Ll_rt(n,m) = Llsplit(1,2);      
    end
end

% return largest elements
[maxval, maxidx] = maxk(res.main.Ll(:,1),options.main.nS);

% select best/avg/worst fitting subjects
ppc_sub = [maxidx(3); maxidx(30); maxidx(options.main.nS)];

% get adjusted correctness for each subject
n_max = 3;
for s = 1:n_max
    n = ppc_sub(s);
    adj_corr{s,1} = res.main.ppc.mod(m).sub(n).adj_correct';
end

% create figure
figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
% plot data sampled from posterior of sub n
h = spirl_rm_raincloud(adj_corr, options.col.tnub); % view is flipped at the end of this function!!!
hold on
for n = 1:n_max
    % plot empirical data of sub n
    p = plot([res.main.SPIRL(ppc_sub(n)).adj_correct res.main.SPIRL(ppc_sub(n)).adj_correct],...
        [min(h.s{n}.YData)-1 max(h.s{n}.YData)+1],...
        'r', 'LineWidth', 6);
    uistack(p, 'bottom') % get empirical binary data to bottom
end
ax = gca; ax.FontSize = 20;
ax.YTickLabel = {ppc_sub(end:-1:1)};
mrgn = 15;
ylim([ax.YTick(1)-mrgn ax.YTick(end)+mrgn])
xlim([0.5 0.95]) % y-axis
hold off

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig8A_ppc_M1_bin']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

%% Fig 8B: Posterior prective checks M1 - log(y_rt)
m=1;

n_max = 3;
% create figure
for s = 1:n_max
    n = ppc_sub(s);
    figure('OuterPosition', outerpos1,...
    'Name', 'Probability Trajectory & Inputs');        
    % plot empirical logRT traj of post_sub n
    a=plot(res.main.est(m,res.main.ppc.ppc_sub(n)).y(:,2), 'r', 'LineWidth', 2);
    hold on
    % plot logRT PREDICTIONS generated using MAP values
    plot(res.main.ppc.mod(m).sub(n).map_sim.yhat(:,2),...
        'LineWidth', 2, 'color', options.col.tnub)
    % plot simulated logRTs from draws from posterior
    for i = 1:options.main.ppc.nDraws_per_sub
        try
            plot(res.main.ppc.mod(m).sub(n,i).sim.y(:,2), 'color', [options.col.tnub 0.1]) % alpha=0.1
        end
    end
    % RT limits by experiment
    plot(1:160, ones(160,1)*log(1700), '--k')
    plot(1:160, ones(160,1)*log(100), '--k')
    hold off
    ylim([4 8])
    uistack(a, 'top') % get empirical RTs to top
    ax = gca; ax.FontSize = 12;
    
    figdir = fullfile(saveDir, 'figures', 'paper',...
        ['fig8B_ppc_M1_logrt_sub_', num2str(n)]);
    print(figdir, '-dpng');
    print(figdir, '-dsvg');
    close;
end

%% Fig 8B: Posterior prective checks M1 - log(y_rt) incl legend
m=1;

n_max = 3;
% create figure
figure('Name', 'Probability Trajectory & Inputs');
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
for s = 1:n_max
    subplot(2,2,s)
    n = ppc_sub(s);
    % plot empirical logRT traj of post_sub n
    a=plot(res.main.est(m,res.main.ppc.ppc_sub(n)).y(:,2), 'r', 'LineWidth', 2);
    hold on
    % plot logRT PREDICTIONS generated using MAP values
    plot(res.main.ppc.mod(m).sub(n).map_sim.yhat(:,2),...
        'LineWidth', 2, 'color', options.col.tnub)
    % plot simulated logRTs from draws from posterior
    for i = 1:options.main.ppc.nDraws_per_sub
        try
            plot(res.main.ppc.mod(m).sub(n,i).sim.y(:,2), 'color', [options.col.tnub 0.1]) % alpha=0.1
        end
    end
    % RT limits by experiment
    plot(1:160, ones(160,1)*log(1700), '--k')
    plot(1:160, ones(160,1)*log(100), '--k')
    hold off
    ylim([4 8])
    ax = gca; ax.FontSize = 12;    
end

legend('empirical log$(y_{rt})$', 'sim log$(\hat{y}_{rt})$ (MAP)', 'sim log$(\hat{y}_{rt})$', 'Interpreter','latex',...
    'Position', [0.72 0.14 0.03 0.07])

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['fig8B_ppc_M1_logrt_legend']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

%% Fig 8B: ppc logRT histograms (M1)

% get simulated logRT from posterior predictive distr
m=1;
for j = 1:size(ppc_sub,1)
    n = ppc_sub(j);
    logRT_sim = NaN(size(res.main.traj.u,1),options.main.ppc.nDraws_per_sub);
    for i = 1:options.main.ppc.nDraws_per_sub
        try
            logRT_sim(:,i) = (res.main.ppc.mod(m).sub(n,i).sim.y(:,2)); %(res.main.est(m,n).y(:,2))
        end
    end

    % create figure
    figure()
    % histogram
    histogram(logRT_sim, 'EdgeColor', options.col.tnub,...
        'FaceColor', options.col.tnub)
    ax = gca; ax.FontSize = 28;
    xlim([2 10])
    ylim([0 1800])

    figdir = fullfile(saveDir, 'figures', 'paper',...
        ['fig8B_sim_logRT_hist_M1_ppc_sub',num2str(n)]);
    print(figdir, '-dpng');
    print(figdir, '-dsvg');
    close;
end


%% ________________________________________________________________________
%%%%%%%%%%%%%%%%%%%%%%%%% Supplementary Materials %%%%%%%%%%%%%%%%%%%%%%%%%

%% Fig S1: behavioural data of indiv subjects & fits (M1)
m=1;
plot_subs = [maxidx(1:4); maxidx(29:30); maxidx(options.main.nS-3:options.main.nS)];
for s = 1:length(plot_subs)
    n = plot_subs(s);
    spirl_tapas_ehgf_binary_combObs_plotTraj(res.main.est(m,n))
    figdir = fullfile(saveDir, 'figures', 'paper',...
        ['figS1_data_fits_m', num2str(m), '_indiv_sub_', num2str(n)]);
    print(figdir, '-dpng');
    print(figdir, '-dsvg');
    close;
end

%% Fig S2a: Empirical priors (densities & prior pred dens of M1-M7)

% plot estimated prior densities
for m = 1:size(res.main.ModSpace,2)
    npars = length(res.main.ModSpace(m).prc_idx)+length(res.main.ModSpace(m).obs_idx);
    nx = ceil(sqrt(npars));
    ny = round(sqrt(npars));
    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    for j = 1:npars
        subplot(nx, ny, j)
        if j > length(res.main.ModSpace(m).prc_idx) % obs
            k = j - length(res.main.ModSpace(m).prc_idx);
            idx = res.main.ModSpace(m).obs_idx(k);
            if k == 1 %log(zeta)
                x_min = -4;
                x_max = 8;
            elseif k == size(res.pilot.priors.mod(m).obs_est, 2)
                x_min = res.pilot.ModSpace(m).obs_config.priormus(idx)...
                    -3*res.pilot.ModSpace(m).obs_config.priorsas(idx);
                x_max = res.pilot.ModSpace(m).obs_config.priormus(idx)...
                    +3*res.pilot.ModSpace(m).obs_config.priorsas(idx);
            else %betas
                x_min = -30;
                x_max = 30;
            end
            x = x_min:0.1:x_max;
            
            y = normpdf(x, res.main.ModSpace(m).obs_config.priormus(idx), sqrt(res.main.ModSpace(m).obs_config.priorsas(idx)));
            y_prior = normpdf(x, res.pilot.ModSpace(m).obs_config.priormus(idx), sqrt(res.pilot.ModSpace(m).obs_config.priorsas(idx)));
            
            plot(x, y, 'k')
            hold on
            plot(x, y_prior, 'k--')
            plot(res.pilot.priors.mod(m).obs_est(:,k), -0.05, 'ko')
            ylim([-0.1 1])
            str = sprintf('mu = %1.2f, Sa = %1.2f', res.main.ModSpace(m).obs_config.priormus(idx), res.main.ModSpace(m).obs_config.priorsas(idx));
            T = text(min(get(gca, 'xlim')), max(get(gca, 'ylim')), str);
            set(T, 'fontsize', 12, 'verticalalignment', 'top', 'horizontalalignment', 'left');
            if k == 1
                title('log(\zeta)')
            elseif k == size(res.pilot.priors.mod(m).obs_est, 2)
                title('log(\Sigma)')
            elseif k == 2
                title('\beta_0')
            elseif k == 3
                title('\beta_1')
            elseif k == 4
                title('\beta_2')
            elseif k == 5
                title('\beta_3')
            elseif k == 6
                title('\beta_4')
            end
        else %prc
            idx = res.main.ModSpace(m).prc_idx(j);
            x_min = res.pilot.ModSpace(m).prc_config.priormus(idx)...
                -3*res.pilot.ModSpace(m).prc_config.priorsas(idx);
            x_max = res.pilot.ModSpace(m).prc_config.priormus(idx)...
                +3*res.pilot.ModSpace(m).prc_config.priorsas(idx);
            x = x_min:0.1:x_max;
            
            y = normpdf(x, res.main.ModSpace(m).prc_config.priormus(idx), sqrt(res.main.ModSpace(m).prc_config.priorsas(idx)));
            y_prior = normpdf(x, res.pilot.ModSpace(m).prc_config.priormus(idx), sqrt(res.pilot.ModSpace(m).prc_config.priorsas(idx)));
            
            plot(x, y, 'k')
            hold on
            plot(x, y_prior, 'k--')
            plot(res.pilot.priors.mod(m).prc_est(:,j), -0.05, 'ko')
            ylim([-0.1 1])
            str = sprintf('mu = %1.2f, Sa = %1.2f', res.main.ModSpace(m).prc_config.priormus(idx), res.main.ModSpace(m).prc_config.priorsas(idx));
            T = text(min(get(gca, 'xlim')), max(get(gca, 'ylim')), str);
            set(T, 'fontsize', 12, 'verticalalignment', 'top', 'horizontalalignment', 'left');
            if j == 1
                title('\omega_2')
            elseif j == 2
                title('\omega_3')
            end
        end
        
    end
    %legend('empirical prior', 'initial prior', 'MAP estimates', 'Position', [0.94 0.48 0.03 0.07])
    figdir = fullfile(saveDir, 'figures', 'paper',...
        ['figS2_empirical_prior_densities_m', num2str(m)]);
    print(figdir, '-dpng');
    print(figdir, '-dsvg');
    close;
end

%% S2b: empirical prior pred dens (ehgf)
for m = 1:size(res.main.ModSpace,2)
    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    
    % mean
    for j = 1:3
        traj_sim = NaN(size(res.main.traj.u,1),options.main.nS);
        for n = 1:options.sim.indiv.nS
            traj_sim(:,n) = res.sim.sub(n,m).data.traj.muhat(:,j);
        end
        avg_traj_sim = mean(traj_sim, 2, 'omitnan');
        if j == 1
            ybin_sim = NaN(size(res.main.traj.u,1),options.main.nS);
            for n = 1:options.sim.indiv.nS
                ybin_sim(:,n) = res.sim.sub(n,m).data.y(:,1);
            end
            avg_ybin_sim = mean(ybin_sim, 2, 'omitnan');
        end
        subplot(3,2,7-2*j)
        plot(res.sim.sub(1,m).data.traj.muhat(:,j), 'color', [options.col.tnub 0.1])
        hold on
        for n = 2:100
            plot(res.sim.sub(n,m).data.traj.muhat(:,j), 'color', [options.col.tnub 0.1])
        end
        plot(avg_traj_sim, 'color', options.col.tnub, 'LineWidth', 1.5)
        if j == 1
            plot(avg_ybin_sim, 'color', options.col.tnuy)
        end
        ytxt = ['$\hat{\mu}_{', num2str(j), '}$'];
        ylabel(ytxt, 'Interpreter', 'Latex')
        if j == 1
            xlabel('trials')
        end
        ax = gca; ax.FontSize = 16;
    end
    
    % var
    for k = 1:3
        traj_sim = NaN(size(res.main.traj.u,1),options.main.nS);
        for n = 1:options.sim.indiv.nS
            traj_sim(:,n) = res.sim.sub(n,m).data.traj.sahat(:,k);
        end
        avg_traj_sim = mean(traj_sim, 2, 'omitnan');
        subplot(3,2,8-2*k)
        plot(res.sim.sub(1,m).data.traj.sahat(:,k), 'color', [options.col.tnub 0.1])
        hold on
        for n = 2:100
            plot(res.sim.sub(n,m).data.traj.sahat(:,k), 'color', [options.col.tnub 0.1])
        end
        plot(avg_traj_sim, 'color', options.col.tnub, 'LineWidth', 1.5)
        ytxt = ['$\hat{\sigma}_{', num2str(k), '}$'];
        ylabel(ytxt, 'Interpreter', 'Latex')
        if k == 1
            xlabel('trials')
        end
        ax = gca; ax.FontSize = 16;
    end
    figdir = fullfile(saveDir, 'figures', 'paper',...
        ['figS2_empirical_prior_pred_distr_ehgf_ybin_M', num2str(m)]);
    print(figdir, '-dpng');
    print(figdir, '-dsvg');
    close;
end

% empirical prior pred distr (log RT)
figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
% collect simulated logRTs
for m = 1:size(res.main.ModSpace,2)
    logRT_sim = NaN(size(res.main.traj.u,1),options.main.nS);
    for n = 1:options.sim.indiv.nS
        logRT_sim(:,n) = res.sim.sub(n,m).data.y(:,2);
    end
    avg_logRT_sim = mean(logRT_sim, 2, 'omitnan');

    % create figure
    subplot(4,2,m)
    plot(avg_logRT_sim, 'color', options.col.tnub, 'LineWidth', 1.5)
    hold on
    plot(1:160, ones(160,1)*log(1700), '--k')
    plot(logRT_sim, 'color', [options.col.tnub 0.1])    
    plot(1:160, ones(160,1)*log(100), '--k')
    ylim([4 8]);%ylim([log(100) log(1700)]);
    % color code different phases
    ax = axis;
    fill([res.main.trials, fliplr(res.main.trials)],...
        [ax(3)*ones(1,length(res.main.trials)), ax(4)*ones(1,length(res.main.trials))],...
        [options.col.idx, fliplr(options.col.idx)], 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
    colormap(options.col.map)
    hold off
    ylabel('log($y_{rt,sim}$)', 'Interpreter', 'Latex')
    if m == 6 || m == 7
        xlabel('trials')
    end
    ax = gca; ax.FontSize = 12;
    txt = ['model ' num2str(m)];
    title(txt)
end
legend('avg $log(\hat{y}_{rt})$', 'response window', '$log(\hat{y}_{rt})$', 'Interpreter','latex',...
    'Position', [0.72 0.14 0.03 0.07])
%legend('avg yhat_{rt}', 'response window', 'yhat_{rt}', 'Position', [0.72 0.16 0.03 0.07])

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['figS2_empirical_prior_pred_distr_log_yhat_rt']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

%% avg +- stdev of empirical prior pred distr (log RT)
figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
% collect simulated logRTs
for m = 1:size(res.main.ModSpace,2)
    logRT_sim = NaN(size(res.main.traj.u,1),options.main.nS);
    for n = 1:options.sim.indiv.nS
        logRT_sim(:,n) = res.sim.sub(n,m).data.y(:,2);
    end
    avg_logRT_sim = mean(logRT_sim, 2, 'omitnan');
    var_yhat = var(logRT_sim, 0, 2, 'omitnan');
    upper_yhat = avg_logRT_sim + sqrt(var_yhat);
    lower_yhat = avg_logRT_sim - sqrt(var_yhat);

    % create figure
    subplot(4,2,m)
    plot(avg_logRT_sim, 'color', options.col.tnub, 'LineWidth', 1.5)
    hold on
    fill([res.main.trials, fliplr(res.main.trials)], [(upper_yhat)', fliplr((lower_yhat)')], ...
             'b', 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
    hold off
    ylabel('log($y_{rt,sim}$)', 'Interpreter', 'Latex')
    if m == 6 || m == 7
        xlabel('trials')
    end
    ax = gca; ax.FontSize = 12;
    txt = ['model ' num2str(m)];
    title(txt)
    ylim([log(100) log(1700)]);
end
legend('$log(\hat{y}_{rt})$', '$Var(log(\hat{y}_{rt}))$', 'Interpreter','latex',...
    'Position', [0.72 0.14 0.03 0.07])

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['figS2_empirical_prior_pred_distr_log_yhat_rt_avg_stdev']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;


%% Fig S3: Parameter recovery results (M1-M7)

for m = 2:size(res.main.ModSpace,2)
    npars = length(res.main.ModSpace(m).prc_idx)+length(res.main.ModSpace(m).obs_idx);
    nx = ceil(sqrt(npars));
    ny = round(sqrt(npars));
    nx = ceil(npars/4);
    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0.15 1 0.9]);
    for j = 1:npars
        if j > length(res.main.ModSpace(m).prc_idx) %% all obs param
            k = j-length(res.main.ModSpace(m).prc_idx);
            scatter(res.rec.param.obs(m).sim(:,k), res.rec.param.obs(m).est(:,k), 15, 'k', 'filled');
            pcc = res.rec.param.obs(m).pcc(k);
            hline = refline(1,0);
            hline.Color = 'k';
            if k == 1
                title('log(\zeta)')
            elseif k == size(res.main.ModSpace(m).obs_idx, 2)
                title('log(\Sigma)')
            elseif k == 2
                title('\beta_0')
            elseif k == 3
                title('\beta_1')
            elseif k == 4
                title('\beta_2')
            elseif k == 5
                title('\beta_3')
            elseif k == 6
                title('\beta_4')
            end
        else
            scatter(res.rec.param.prc(m).sim(:,j), res.rec.param.prc(m).est(:,j), 15, 'k', 'filled');
            pcc = res.rec.param.prc(m).pcc(j);
            hline = refline(1,0);
            hline.Color = 'k';
            if j == 1
                title('\omega_2')
            elseif j == 2
                title('\omega_3')
            end
        end        
        xlabel('Simulated values');
        ylabel('Recovered values');
        str = sprintf('r = %1.2f', pcc);
        textXpos = min(get(gca, 'xlim')) + (max(get(gca, 'xlim')) - min(get(gca, 'xlim')))*0.05;
        textYpos = max(get(gca, 'ylim')) - (max(get(gca, 'ylim')) - min(get(gca, 'ylim')))*0.05;
        T = text(textXpos, textYpos, str);
        set(T, 'fontsize', 12, 'verticalalignment', 'top', 'horizontalalignment', 'left');
    end
    figdir = fullfile(saveDir, 'figures', 'paper',...
        ['figS3_param_rec_M', num2str(m)]);
    print(figdir, '-dpng');
    print(figdir, '-dsvg');
    close;
end

%% Fig S4: average logRTs and log RT fits (M1-M7)
mean_logRT = mean(res.main.logRT_mat, 2, 'omitnan');
var_logRT = var(res.main.logRT_mat, 0, 2, 'omitnan');
logupper = mean_logRT + sqrt(var_logRT);
loglower = mean_logRT - sqrt(var_logRT);
figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
for m = 1:size(res.main.ModSpace,2)
    for n = 1:options.main.nS
        yhat_mat(:,n) = res.main.est(m,n).optim.yhat(:,2);
    end
    mod(m).mean_yhat = mean(yhat_mat, 2, 'omitnan');
    mod(m).var_yhat = var(yhat_mat, 0, 2, 'omitnan');
    mod(m).upper_yhat = mod(m).mean_yhat + sqrt(mod(m).var_yhat);
    mod(m).lower_yhat = mod(m).mean_yhat - sqrt(mod(m).var_yhat);
    
    subplot(4,2,m)
    plot(res.main.trials, mean_logRT, 'LineWidth', 2, 'color', options.col.tnub)
    hold on
    fill([res.main.trials, fliplr(res.main.trials)], [(logupper)', fliplr((loglower)')], ...
             'b', 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
    plot(res.main.trials, mod(m).mean_yhat, 'LineWidth', 2)
    fill([res.main.trials, fliplr(res.main.trials)], [(mod(m).upper_yhat)', fliplr((mod(m).lower_yhat)')], ...
             'r', 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
    xlim([1 160])
    ylabel('logRT [ms]')
    if m == 6 || m == 7
        xlabel('trials')
    end
    ax = gca; ax.FontSize = 12;
    txt = ['model ' num2str(m)];
    title(txt)
end
legend('$log(y_{rt})$', '$Var(log(y_{rt}))$', '$log(\hat{y}_{rt})$',...
    '$Var(log(\hat{y}_{rt}))$', 'Interpreter','latex',...
    'Position', [0.72 0.14 0.03 0.07])
figdir = fullfile(saveDir, 'figures', 'paper',...
    ['figS4_avg_logrt_fits']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

%% Fig SX: binary predictions model fit residuals (M1)

% get residuals of logRT fits
m=1;
ybin_res = NaN(size(res.main.traj.u,1),options.main.nS);
for n = 1:options.main.nS
    ybin_res(:,n) = res.main.est(m,n).optim.res(:,1);
end

% create figure
figure()
% histogram
histogram(ybin_res, 'EdgeColor', options.col.tnub, 'FaceColor', options.col.tnub)

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['figSX_bin_res_hist_M1']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

%% Fig S5 posterior predictive checks
m=1;

% select best/avg/worst fitting subjects
ppc_sub = [maxidx(1:4); maxidx(29:30); maxidx(options.main.nS-3:options.main.nS)];

% get adjusted correctness for each subject
n_max = 10;
for s = 1:n_max
    n = ppc_sub(s);
    adj_corr{s,1} = res.main.ppc.mod(m).sub(n).adj_correct';
end

% create figure
figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
% plot data sampled from posterior of sub n
h = spirl_rm_raincloud(adj_corr, options.col.tnub); % view is flipped at the end of this function!!!
hold on
for n = 1:n_max
    % plot empirical data of sub n
    p = plot([res.main.SPIRL(ppc_sub(n)).adj_correct res.main.SPIRL(ppc_sub(n)).adj_correct],...
        [min(h.s{n}.YData)-1 max(h.s{n}.YData)+1],...
        'r', 'LineWidth', 6);
    uistack(p, 'bottom') % get empirical binary data to bottom
end
ax = gca; ax.FontSize = 14;
ax.YTickLabel = {ppc_sub(end:-1:1)};
mrgn = 15;
ylim([ax.YTick(1)-mrgn ax.YTick(end)+mrgn])
xlim([0.5 0.95]) % y-axis
hold off

figdir = fullfile(saveDir, 'figures', 'paper',...
    ['figS5_ppc_M1_bin']);
print(figdir, '-dpng');
print(figdir, '-dsvg');
close;

% log RT
m=1;

% create figure
for s = 1:n_max
    n = ppc_sub(s);
    figure('OuterPosition', outerpos1,...
    'Name', 'Probability Trajectory & Inputs');        
    % plot empirical logRT traj of post_sub n
    a=plot(res.main.est(m,res.main.ppc.ppc_sub(n)).y(:,2), 'r', 'LineWidth', 2);
    hold on
    % plot logRT PREDICTIONS generated using MAP values
    plot(res.main.ppc.mod(m).sub(n).map_sim.yhat(:,2),...
        'LineWidth', 2, 'color', options.col.tnub)
    % plot simulated logRTs from draws from posterior
    for i = 1:options.main.ppc.nDraws_per_sub
        try
            plot(res.main.ppc.mod(m).sub(n,i).sim.y(:,2), 'color', [options.col.tnub 0.1]) % alpha=0.1
        end
    end
    % RT limits by experiment
    plot(1:160, ones(160,1)*log(1700), '--k')
    plot(1:160, ones(160,1)*log(100), '--k')
    hold off
    ylim([4 8])
    uistack(a, 'top') % get empirical RTs to top
    ax = gca; ax.FontSize = 14;
    
    figdir = fullfile(saveDir, 'figures', 'paper',...
        ['figS5_ppc_M1_logrt_sub_', num2str(n)]);
    print(figdir, '-dpng');
    print(figdir, '-dsvg');
    close;
end

%% disp
disp('all paper figures created & saved.')

end
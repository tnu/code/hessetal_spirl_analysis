function [] = job_runner_12_main_results(EULER)
% [] = job_runner_12_main_results(EULER)
%
% Calculates results from model-based analysis of the main data set and
% performs posterior predictive checks.
%
% INPUT
%   EULER        binary           Binary indicator variable
%
%   OPTIONAL:
%
% OUTPUT    
%   argout       type
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

%% setup path
saveDir = spirl_setup_paths(EULER);

%% load analysis specifications
options = load(fullfile(saveDir, 'results', ['options']));
rng(123, 'twister');
options.rng.settings = rng;

%% load pilot priors
pilot = load(fullfile(saveDir, 'results', 'pilots', ['pilot_priors']));

%% load model space for analysis of the main data set
main = load(fullfile(saveDir, 'results', 'main', ['model_space']));

%% load simulated data
sim = load(fullfile(saveDir, 'results', 'sim', 'indiv', ['simulated_data']));

%% load recovery analysis results
rec = load(fullfile(saveDir, 'results', 'sim', 'indiv', ['recovery_analysis']));

%% load results from simulation analyses
fam = load(fullfile(saveDir, 'results', 'sim', 'family',...
    ['res_family_power_analysis']));

%% create res struct
res = struct();
res.pilot = pilot;
res.main = main;
res.sim = sim;
res.rec = rec;
res.sim.fam = fam;

%% load results from model inversion on the main data set
% loop over participants & models
for n = 1:options.main.nS
    for m = 1:size(res.main.ModSpace, 2)
        fprintf('current iteration: n=%1.0f, m=%1.0f \n', n,m);
        res.main.est(m,n) = load(fullfile(saveDir, 'results',...
            'main', ['sub', num2str(n)], ['est_mod', num2str(m)]));
        % LME matrix
        res.main.LME(n,m) = res.main.est(m,n).optim.LME;
        % param values
        res.main.param.prc(m).est(n,:) = res.main.est(m,n).p_prc.ptrans(res.main.ModSpace(m).prc_idx);
        res.main.param.obs(m).est(n,:) = res.main.est(m,n).p_obs.ptrans(res.main.ModSpace(m).obs_idx);         
    end
end

%% perform family level rfx BMS
res.main.F = res.main.LME';
[res.main.fam.posterior, res.main.fam.out] = VBA_groupBMC(res.main.F, options.fam.bmc_opt); % run family-level 

%% rfx BMS (individual model level)
[res.main.indiv.posterior, res.main.indiv.out] = VBA_groupBMC(res.main.F, res.rec.bmc.opt);

%% paired t-test for difference between MAP estimates & init prior means: beta1-4
m=1; %winning model
npars = 4;
for j = 1:npars
    k = j+2;
    % one-sample t-test (MAP est vs init prior mean), Bonferroni corrected
    [res.main.param.ttest(j).h,res.main.param.ttest(j).p,...
        res.main.param.ttest(j).ci,res.main.param.ttest(j).stats]...
        = ttest(res.main.param.obs(m).est(:,k), 0, 'Alpha', 0.05/npars);
end

%% Posterior Predictive Checking

% create vector with idx of synthetic subjects
res.main.ppc.ppc_sub = 1:options.main.nS;

% draw samples from posterior for each model and synthetic subject
for m = 1:size(res.main.ModSpace,2)
    c.c_prc = res.main.ModSpace(m).prc_config;
    c.c_obs = res.main.ModSpace(m).obs_config;
    for n = 1:length(res.main.ppc.ppc_sub)
        % sample from posterior
        for i = 1:options.main.ppc.nDraws_per_sub
            res.main.ppc.mod(m).sub(n,i).post_sub_idx = res.main.ppc.ppc_sub(n);
            % prc model
            res.main.ppc.mod(m).sub(n,i).ptrans_prc = res.main.est(m,res.main.ppc.ppc_sub(n)).p_prc.ptrans;
            for j = 1:size(res.main.ModSpace(m).prc_idx,2)
                res.main.ppc.mod(m).sub(n,i).ptrans_prc(res.main.ModSpace(m).prc_idx(j)) = ...
                    normrnd(res.main.est(m,res.main.ppc.ppc_sub(n)).p_prc.ptrans(res.main.ModSpace(m).prc_idx(j)), ...
                    abs(sqrt(res.main.est(m,res.main.ppc.ppc_sub(n)).optim.Sigma(j,j))));
            end
            % obs model
            res.main.ppc.mod(m).sub(n,i).ptrans_obs = res.main.est(m,res.main.ppc.ppc_sub(n)).p_obs.ptrans;
            for k = 1:size(res.main.ModSpace(m).obs_idx,2)
                res.main.ppc.mod(m).sub(n,i).ptrans_obs(res.main.ModSpace(m).obs_idx(k)) = ...
                    normrnd(res.main.est(m,res.main.ppc.ppc_sub(n)).p_obs.ptrans(res.main.ModSpace(m).obs_idx(k)), ...
                    abs(sqrt(res.main.est(m,res.main.ppc.ppc_sub(n)).optim.Sigma(k+size(res.main.ModSpace(m).prc_idx,2),k+size(res.main.ModSpace(m).prc_idx,2)))));
            end
            % transform param values back to native space (for sim input)
            res.main.ppc.mod(m).sub(n,i).nativeInp_prc = res.main.ModSpace(m).prc_config.transp_prc_fun(c, res.main.ppc.mod(m).sub(n,i).ptrans_prc);
            res.main.ppc.mod(m).sub(n,i).nativeInp_obs = res.main.ModSpace(m).obs_config.transp_obs_fun(c, res.main.ppc.mod(m).sub(n,i).ptrans_obs);
        end
    end
end


% generate data using samples from posterior
for m = 1:size(res.main.ModSpace,2)
    % reset rng state idx
    options.rng.idx = 1;
    for n = 1:length(res.main.ppc.ppc_sub)
        res.main.ppc.mod(m).sub(n).logRT_mat = NaN(size(res.main.logRT_mat,1), options.main.ppc.nDraws_per_sub);
        res.main.ppc.mod(m).sub(n).yhat_mat = NaN(size(res.main.logRT_mat,1), options.main.ppc.nDraws_per_sub);
        for i = 1:options.main.ppc.nDraws_per_sub
            try
                res.main.ppc.mod(m).sub(n,i).sim = tapas_simModel(res.main.est(m,n).u,...
                    res.main.ModSpace(m).prc,...
                    res.main.ppc.mod(m).sub(n,i).nativeInp_prc,... posterior param values in native space
                    res.main.ModSpace(m).obs,...
                    res.main.ppc.mod(m).sub(n,i).nativeInp_obs,... nat space
                    options.rng.settings.State(options.rng.idx, 1));
            res.main.ppc.mod(m).sub(n).logRT_mat(:,i) = res.main.ppc.mod(m).sub(n,i).sim.y(:,2);
            res.main.ppc.mod(m).sub(n).yhat_mat(:,i) = res.main.ppc.mod(m).sub(n,i).sim.yhat(:,2);
            catch
                disp('model: ')
                disp(m)
                disp('post_sub:')
                disp(res.main.ppc.ppc_sub(n))
                res.main.ppc.mod(m).sub(n).logRT_mat(:,i) = NaN(size(res.main.logRT_mat,1),1);
                res.main.ppc.mod(m).sub(n).yhat_mat(:,i) = NaN(size(res.main.logRT_mat,1),1);
            end            
            % Update the rng state idx
            options.rng.idx = options.rng.idx+1;
            if options.rng.idx == (length(options.rng.settings.State)+1)
                options.rng.idx = 1;
            end
        end
    end
end

%% generate synthetic data using map estimates
for m = 1:size(res.main.ModSpace,2)
    for n = 1:length(res.main.ppc.ppc_sub)
        res.main.ppc.mod(m).sub(n).map_sim = tapas_simModel(res.main.est(m,n).u,... 
                res.main.ModSpace(m).prc,...
                res.main.est(m,res.main.ppc.ppc_sub(n)).p_prc.p,... posterior param values in native space
                res.main.ModSpace(m).obs,...
                res.main.est(m,res.main.ppc.ppc_sub(n)).p_obs.p,... nat space
                options.rng.settings.State(options.rng.idx, 1));
    end
end

%% PPC indiv binary: calc adjusted correct responses
% adjusted correctness real subject
res.main.nTr_adj = sum(max(res.main.traj.probs_green, 1-res.main.traj.probs_green));
for n = 1:length(res.main.ppc.ppc_sub)
    res.main.SPIRL(res.main.ppc.ppc_sub(n)).tr_correct = (res.main.SPIRL(res.main.ppc.ppc_sub(n)).y(:,1) == res.main.SPIRL(res.main.ppc.ppc_sub(n)).u);
    res.main.SPIRL(res.main.ppc.ppc_sub(n)).adj_correct = sum(res.main.SPIRL(res.main.ppc.ppc_sub(n)).tr_correct)/res.main.nTr_adj;
end
% adjusted correctness sim subject
for m = 1:size(res.main.ModSpace,2)
    for n = 1:length(res.main.ppc.ppc_sub)
        res.main.ppc.mod(m).sub(n).adj_correct = NaN(1,options.main.ppc.nDraws_per_sub);
        for i = 1:options.main.ppc.nDraws_per_sub
            try
            res.main.ppc.mod(m).sub(n,i).tr_correct = (res.main.ppc.mod(m).sub(n,i).sim.y(:,1) == res.main.SPIRL(res.main.ppc.ppc_sub(n)).u);
            res.main.ppc.mod(m).sub(n).adj_correct(i) = sum(res.main.ppc.mod(m).sub(n,i).tr_correct)/res.main.nTr_adj;
            end
        end
    end
end

%% PPC indiv binary (additional check): calc adjusted correct responses (prior pred. distr.)
% adjusted correctness sim subject (SAMPLING FROM PRIOR!)
for m = 1:size(res.main.ModSpace,2)
    for n = 1
        res.sim.sub(n,m).adj_correct = NaN(1, options.sim.indiv.nS);
        for i = 1:options.sim.indiv.nS
            res.sim.sub(i,m).tr_correct = (res.sim.sub(i,m).data.y(:,1) == res.pilot.SPIRL(n).u);
            res.sim.sub(n,m).adj_correct(i) = sum(res.sim.sub(i,m).tr_correct)/res.main.nTr_adj;
        end
    end
end

%% PPC indiv: calc # switches & stay probabilities (binary predictions)
% real subject
for n = 1:length(res.main.ppc.ppc_sub)
    % #switches
    res.main.SPIRL(res.main.ppc.ppc_sub(n)).n_switch = 0;
    % stay prob
    res.main.SPIRL(res.main.ppc.ppc_sub(n)).n_corr = 0;
    res.main.SPIRL(res.main.ppc.ppc_sub(n)).n_stay_corr = 0;
    res.main.SPIRL(res.main.ppc.ppc_sub(n)).n_false = 0;
    res.main.SPIRL(res.main.ppc.ppc_sub(n)).n_stay_false = 0;
    for t = 2:size(res.main.SPIRL(res.main.ppc.ppc_sub(n)).y,1)
        if res.main.SPIRL(res.main.ppc.ppc_sub(n)).y(t-1,1) ~= res.main.SPIRL(res.main.ppc.ppc_sub(n)).y(t,1)
            % switch
            res.main.SPIRL(res.main.ppc.ppc_sub(n)).n_switch = res.main.SPIRL(res.main.ppc.ppc_sub(n)).n_switch+1;
            if res.main.SPIRL(res.main.ppc.ppc_sub(n)).y(t-1,1) == res.main.SPIRL(res.main.ppc.ppc_sub(n)).u(t-1,1)
                % correct
                res.main.SPIRL(res.main.ppc.ppc_sub(n)).n_corr = res.main.SPIRL(res.main.ppc.ppc_sub(n)).n_corr+1;
            else
                % false
                res.main.SPIRL(res.main.ppc.ppc_sub(n)).n_false = res.main.SPIRL(res.main.ppc.ppc_sub(n)).n_false+1;
            end
        else % stay
            if res.main.SPIRL(res.main.ppc.ppc_sub(n)).y(t-1,1) == res.main.SPIRL(res.main.ppc.ppc_sub(n)).u(t-1,1)
                % correct + stay
                res.main.SPIRL(res.main.ppc.ppc_sub(n)).n_corr = res.main.SPIRL(res.main.ppc.ppc_sub(n)).n_corr+1;
                res.main.SPIRL(res.main.ppc.ppc_sub(n)).n_stay_corr = res.main.SPIRL(res.main.ppc.ppc_sub(n)).n_stay_corr+1;
            else
                % false + stay
                res.main.SPIRL(res.main.ppc.ppc_sub(n)).n_false = res.main.SPIRL(res.main.ppc.ppc_sub(n)).n_false+1;
                res.main.SPIRL(res.main.ppc.ppc_sub(n)).n_stay_false = res.main.SPIRL(res.main.ppc.ppc_sub(n)).n_stay_false+1;
            end
        end
    end
    res.main.SPIRL(res.main.ppc.ppc_sub(n)).p_stay = [res.main.SPIRL(res.main.ppc.ppc_sub(n)).n_stay_false/res.main.SPIRL(res.main.ppc.ppc_sub(n)).n_false ...
        res.main.SPIRL(res.main.ppc.ppc_sub(n)).n_stay_corr/res.main.SPIRL(res.main.ppc.ppc_sub(n)).n_corr];
end
% sim subject
for m = 1:size(res.main.ModSpace,2)
    for n = 1:length(res.main.ppc.ppc_sub)
        % #switches
        res.main.ppc.mod(m).sub(n).n_switch_all = NaN(1,options.main.ppc.nDraws_per_sub);
        % stay prob
        res.main.ppc.mod(m).sub(n).n_corr_all = NaN(1,options.main.ppc.nDraws_per_sub);
        res.main.ppc.mod(m).sub(n).n_stay_corr_all = NaN(1,options.main.ppc.nDraws_per_sub);
        res.main.ppc.mod(m).sub(n).n_false_all = NaN(1,options.main.ppc.nDraws_per_sub);
        res.main.ppc.mod(m).sub(n).n_stay_false_all = NaN(1,options.main.ppc.nDraws_per_sub);
        for i = 1:options.main.ppc.nDraws_per_sub
            if ~isempty(res.main.ppc.mod(m).sub(n,i).sim)
                % #switches
                res.main.ppc.mod(m).sub(n,i).n_switch = 0;
                % stay prob
                res.main.ppc.mod(m).sub(n,i).n_corr = 0;
                res.main.ppc.mod(m).sub(n,i).n_stay_corr = 0;
                res.main.ppc.mod(m).sub(n,i).n_false = 0;
                res.main.ppc.mod(m).sub(n,i).n_stay_false = 0;
                for t = 2:size(res.main.ppc.mod(m).sub(n,i).sim.y,1)
                    if res.main.ppc.mod(m).sub(n,i).sim.y(t-1,1) ~= res.main.ppc.mod(m).sub(n,i).sim.y(t,1)
                        % switchres.main.ppc.mo
                        res.main.ppc.mod(m).sub(n,i).n_switch = res.main.ppc.mod(m).sub(n,i).n_switch+1;
                        if res.main.ppc.mod(m).sub(n,i).sim.y(t-1,1) == res.main.ppc.mod(m).sub(n,i).sim.u(t-1,1)
                            res.main.ppc.mod(m).sub(n,i).n_corr = res.main.ppc.mod(m).sub(n,i).n_corr+1; % correct
                        else
                            res.main.ppc.mod(m).sub(n,i).n_false = res.main.ppc.mod(m).sub(n,i).n_false+1; % false
                        end
                    else %stay
                        if res.main.ppc.mod(m).sub(n,i).sim.y(t-1,1) == res.main.ppc.mod(m).sub(n,i).sim.u(t-1,1) % correct+stay
                            res.main.ppc.mod(m).sub(n,i).n_corr = res.main.ppc.mod(m).sub(n,i).n_corr+1;
                            res.main.ppc.mod(m).sub(n,i).n_stay_corr = res.main.ppc.mod(m).sub(n,i).n_stay_corr+1; 
                        else % false+stay
                            res.main.ppc.mod(m).sub(n,i).n_false = res.main.ppc.mod(m).sub(n,i).n_false+1;
                            res.main.ppc.mod(m).sub(n,i).n_stay_false = res.main.ppc.mod(m).sub(n,i).n_stay_false+1; 
                        end
                    end
                end
            else
                res.main.ppc.mod(m).sub(n,i).n_switch = NaN;
                res.main.ppc.mod(m).sub(n,i).n_corr = NaN;
                res.main.ppc.mod(m).sub(n,i).n_stay_corr = NaN;
                res.main.ppc.mod(m).sub(n,i).n_false = NaN;
                res.main.ppc.mod(m).sub(n,i).n_stay_false = NaN;
            end
            res.main.ppc.mod(m).sub(n).n_switch_all(i) = res.main.ppc.mod(m).sub(n,i).n_switch;
            res.main.ppc.mod(m).sub(n).n_corr_all(i) = res.main.ppc.mod(m).sub(n,i).n_corr;
            res.main.ppc.mod(m).sub(n).n_stay_corr_all(i) = res.main.ppc.mod(m).sub(n,i).n_stay_corr;
            res.main.ppc.mod(m).sub(n).n_false_all(i) = res.main.ppc.mod(m).sub(n,i).n_false;
            res.main.ppc.mod(m).sub(n).n_stay_false_all(i) = res.main.ppc.mod(m).sub(n,i).n_stay_false;
        end
        res.main.ppc.mod(m).sub(n).p_stay_all = [res.main.ppc.mod(m).sub(n).n_stay_false_all'./res.main.ppc.mod(m).sub(n).n_false_all' ...
            res.main.ppc.mod(m).sub(n).n_stay_corr_all'./res.main.ppc.mod(m).sub(n).n_corr_all'];
        res.main.ppc.mod(m).sub(n).prev_corr = [zeros(options.main.ppc.nDraws_per_sub,1) ones(options.main.ppc.nDraws_per_sub,1)];
    end
end

%% PPC indiv additional check: calc #switches under prior (prior pred. distr.)
% adjusted correctness sim subject (SAMPLING FROM PRIOR!)
for m = 1:size(res.main.ModSpace,2)
    for n = 1
        res.sim.sub(n,m).n_switch_all = NaN(1, options.sim.indiv.nS);
        for i = 1:options.sim.indiv.nS
            % #switches
            res.sim.sub(i,m).n_switch = 0;
            for t = 2:size(res.sim.sub(i,m).data.y,1)
                if res.sim.sub(i,m).data.y(t-1,1) ~= res.sim.sub(i,m).data.y(t,1)
                    % switchres.main.ppc.mo
                    res.sim.sub(i,m).n_switch = res.sim.sub(i,m).n_switch+1;
                end
            end
            res.sim.sub(n,m).n_switch_all(i) = res.sim.sub(i,m).n_switch;
        end
    end
end

%% calculate # switches main & pilot data set
% PPC indiv: calc # switches & stay probabilities (binary predictions)
res.main.ppc.ppc_sub_pilot = 1:options.pilots.nS;
% real subject
for n = 1:length(res.main.ppc.ppc_sub_pilot)
    % #switches
    res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).n_switch = 0;
    % stay prob
    res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).n_corr = 0;
    res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).n_stay_corr = 0;
    res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).n_false = 0;
    res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).n_stay_false = 0;
    for t = 2:size(res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).y,1)
        if res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).y(t-1,1) ~= res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).y(t,1)
            % switch
            res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).n_switch = res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).n_switch+1;
            if res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).y(t-1,1) == res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).u(t-1,1)
                % correct
                res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).n_corr = res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).n_corr+1;
            else
                % false
                res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).n_false = res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).n_false+1;
            end
        else % stay
            if res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).y(t-1,1) == res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).u(t-1,1)
                % correct + stay
                res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).n_corr = res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).n_corr+1;
                res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).n_stay_corr = res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).n_stay_corr+1;
            else
                % false + stay
                res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).n_false = res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).n_false+1;
                res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).n_stay_false = res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).n_stay_false+1;
            end
        end
    end
    res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).p_stay = ...
        [res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).n_stay_false/res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).n_false ...
        res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).n_stay_corr/res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).n_corr];
end

%% save results as struct
save_path = fullfile(saveDir, 'results', 'main', ['main_results']);
save(save_path, '-struct', 'res', '-v7.3');

disp('main data set successfully analyzed.')

end
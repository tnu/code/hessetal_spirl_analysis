function [] = job_runner_9_family_power_res(EULER)
% [] = job_runner_9_family_power_res(EULER)
%
% Run family level BMS for 100 simulations of all different simulation
% settings in iteration j.
%
% INPUT
%   EULER        binary           Binary indicator variable
%   j            integer          Integer for iteration of 100 fam sim runs
%
%   OPTIONAL:
%
% OUTPUT    
%   argout       type
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

%% setup path
saveDir = spirl_setup_paths(EULER);

%% load analysis specifications
options = load(fullfile(saveDir, 'results', ['options']));
rng(123, 'twister');
options.rng.settings = rng;

%% load family power setup
fam = load(fullfile(saveDir, 'results', 'sim', 'family', ['family_power_setup']));

%% load fam sim results
result = [];
for j = 1:options.sim.fam.opts(1).n_jobs_sim          
    dat = load(fullfile(saveDir, 'results', 'sim', 'family',...
        ['res_family_power_sim', num2str(j)]));
    fam.sim(j).result = dat.result;
    result = [result; dat.result];
end  

%% create table
t = struct2table(result); % size: n_sim*numel(options) x 14

%% save to struct
fam.all_results = result;
fam.t = t;

%% save family power results as struct
save_path = fullfile(saveDir, 'results', 'sim', 'family',...
    ['res_family_power_analysis']);
save(save_path, '-struct', 'fam', '-v7.3');

disp('family power sim results analyzed.')

end
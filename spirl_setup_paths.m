function [saveDir] = spirl_setup_paths(EULER)
% [saveDir] = spirl_setup_paths(EULER)
%
% This function adds all the necessary subfolders to the path and
% outputs the directory where the results & figures folders are stored.
%
% INPUT
%   EULER        binary       Binary indicator variable  
%
%   OPTIONAL:
%
% OUTPUT    
%   saveDir      string       Path where res & figs folders are stored
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

%% directory where results & figures folders are stored
saveDir = pwd;

%% setup path
if EULER == 1
    % add subfolders
    addpath('hgf_extensions')
    addpath('comb_obs_models')
    addpath('utils')
    
    % add submodules (toolboxes)
    addpath(genpath('RainCloudPlots'));
    addpath(genpath('tapas'));
    cd('VBA-toolbox');
    VBA_setup();
    cd ..
end

end
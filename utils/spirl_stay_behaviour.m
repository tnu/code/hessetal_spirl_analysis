function [prev_corr, p_stay_avg, p_stay_var] = spirl_stay_behaviour(SPIRL, trials, options, type, saveDir)
% [] = spirl_stay_behaviour(SPIRL, trials, traj, options, type, saveDir)
%
% Visualizes stay behaviour (model-agnostic analysis of binary responses).
%
% INPUT
%   SPIRL        struct       Data (binary inputs and responses, rt)
%   trials       vector       1x160 vector with trial indices
%   traj         struct       Input generating probability trajectory
%   options      struct       Specified settings of the analysis pipeline
%   type         string       Data set: 'pilots' or 'main'
%   saveDir      string       Dir where figures are saved
%
%   OPTIONAL:
%
% OUTPUT    
%   prev_corr    vector       Boolean 1x2 vector for correct/incorrect
%   p_stay_avg   vector       1x2 vector with avg stay prob (corr/incorr)
%   p_stay_var   vector       1x2 vector with var of stay prob (corr/incorr)
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

%% initialize varianbles
n_corr = zeros(size(SPIRL));
n_stay_corr = zeros(size(SPIRL));
n_switch_corr = zeros(size(SPIRL));
n_1false = zeros(size(SPIRL));
n_stay_1false = zeros(size(SPIRL));
n_switch_false = zeros(size(SPIRL));

%% over through subjects & trials (count stay/switch behaviour)
for n = 1:size(SPIRL, 2)
    for k = 1:size(trials,2)-1
        if SPIRL(n).u(k) == SPIRL(n).y(k,1) % correct prediction
            n_corr(n) = n_corr(n) + 1;
            if SPIRL(n).y(k,1) == SPIRL(n).y(k+1,1) %stay
                n_stay_corr(n) = n_stay_corr(n) + 1;
            else %switch
                n_switch_corr(n) = n_switch_corr(n) + 1;
            end
        elseif SPIRL(n).u(k) ~= SPIRL(n).y(k,1) % incorrect prediction
            n_1false(n) = n_1false(n) + 1;
            if SPIRL(n).y(k,1) == SPIRL(n).y(k+1,1) %stay
                n_stay_1false(n) = n_stay_1false(n) + 1;
            else %switch
                n_switch_false(n) = n_switch_false(n) + 1;
            end
        end
    end
end

%% calculate stay-probability (mean & var across subjects)
p_stay_avg = [mean(n_stay_1false./n_1false) mean(n_stay_corr./n_corr)];
p_stay_var = [var(n_stay_1false./n_1false) var(n_stay_corr./n_corr)];
prev_corr = [0 1];

%% visualize switch-stay behaviour
figure
h = errorbar(prev_corr, p_stay_avg, sqrt(p_stay_var), 'o-');
h.Color = options.col.tnub;
ax1 = gca;
ax1.XTick = [0 1];
ax1.XTickLabel = {'incorrect' 'correct'};
xlabel('previous prediction')
ylabel('p(stay)')
xlim([-0.1 1.1])
ylim([0 1])

figdir = fullfile(saveDir, 'figures', type, 'model_agnostic',...
    ['stay_behaviour_corr_incorr']);
print(figdir, '-dtiff');
close;

end
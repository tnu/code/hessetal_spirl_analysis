function [options] = spirl_specs()
% [options] = spirl_specs()
%
% This function saves all the settings for running the analysis pipeline to
% a struct.
%
% INPUT
%   argin        type           
%
%   OPTIONAL:
%
% OUTPUT    
%   options      struct       Struct with specification of the analysis
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

% pilot data set
options.pilots.nS = 20;
options.pilots.filename = ['SPIRL_pilot_',...
    num2str(options.pilots.nS), 'sub_preprocessed_IncludedInAnalysis'];

% simulation analysis
options.sim.indiv.nS = 100;

% family-level simulations
options.sim.fam.opts = VBA_factorial_struct ( ...
    'size_families', {[4 3]},...{[4 3]}, ... model space (families)
    'model_prob_winner', [1],   ... logpp generation (models from winning family)
    'model_prob_corr', 1,   ... logpp generation (models from winning fam EXCEPT winning model)
    'model_prob_noise', 1,   ... logpp generation (randomly sampling model logpp)
    'freq_family_B', (0 : 60) / 60, ... logpp generation: proportion of subjects (with winning model) from family B
    'n_subject', 60, ... logpp generation: n_subjects
    'n_simulation', 50, ...100 n_sim per logpp-generation-condition & sim_job
    'n_jobs_sim', 100 ...50 
    );

% family-level bayesian model comparison options
options.fam.bmc_opt.families = { ...
    1 : options.sim.fam.opts(1).size_families(1), ...
    options.sim.fam.opts(1).size_families(1) + (1 : options.sim.fam.opts(1).size_families(2)) ...
    }; % family struct for input to VBA group Bayesian Model Comparison
options.fam.bmc_opt.verbose = false;
options.fam.bmc_opt.DisplayWin = false;

% main data set
options.main.nS = 59;
options.main.filename = ['SPIRL_main_',...
    num2str(options.main.nS), 'sub_preprocessed_IncludedInAnalysis'];

% posterior predictive checking: number of draws
options.main.ppc.nDraws_per_sub = 100;

% optimization algorithm
options.opt_config = eval('tapas_quasinewton_optim_config');
options.opt_config.nRandInit = 399;

% seed for rng
rng(123, 'twister')
options.rng.settings = rng;
options.rng.idx = 1; % Set counter for random number states

% define colors for plotting
options.col.wh = [1 1 1];
options.col.gry = [0.5 0.5 0.5];
options.col.tnub = [0 110 182]/255; 
options.col.tnuy = [255 166 22]/255;
options.col.grn = [0 0.6 0];

end
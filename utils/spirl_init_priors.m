function [] = spirl_init_priors(pilot, options, saveDir)
% [] = spirl_init_priors(pilot, options, saveDir)
%
% Generates plots of prior predictive densities under the initial priors.
%
% INPUT
%   pilot        struct       Model space for analysis of held-out data set
%   options      struct       Specified settings of the analysis pipeline
%   saveDir      string       Dir where figures are saved
%
%   OPTIONAL:
%
% OUTPUT    
%   argout       type         
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

%% initial priors 3-level ehgf for binary inputs
c.c_prc = pilot.ModSpace(1).prc_config;
prc_vect_nat = c.c_prc.transp_prc_fun(c, c.c_prc.priormus);
prc_idx = pilot.ModSpace(1).prc_idx;
c.c_obs = eval('tapas_unitsq_sgm_config');
obs_vect_nat = c.c_obs.transp_obs_fun(c, c.c_obs.priormus);

%% grid: free param values ehgf
for i = 1:length(prc_idx)
    free_param(i,:) = ...
        (prc_vect_nat(prc_idx(i))-3*sqrt(c.c_prc.priorsas(prc_idx(i)))):1:(prc_vect_nat(prc_idx(i))+3*sqrt(c.c_prc.priorsas(prc_idx(i))));
end

% generate synthetic data
for idx_om3 = 1:size(free_param, 2)
    prc_vect_nat(prc_idx(2)) = free_param(2,idx_om3);
    for idx_om2 = 1:size(free_param, 2)
        prc_vect_nat(prc_idx(1)) = free_param(1,idx_om2);
        try
            sim(idx_om2,idx_om3) = tapas_simModel(pilot.traj.u,...
                pilot.ModSpace(1).prc,...
                prc_vect_nat,...
                'tapas_unitsq_sgm',...
                obs_vect_nat,...
                options.rng.settings.State(1)); % necessary to add prc & obs config ???
        end
    end
end

%% plot prior predictive distr (muhat1) & grid of free param values
figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);

% input
subplot(2,1,1)
plot(pilot.traj.u, '.k')
ylim([-0.1 1.1]);
ylabel('$\hat{\mu}_1$', 'Interpreter', 'Latex');
xlabel('Trial')
hold on

subplot(2,1,2)
xlabel('${\omega}_2$', 'Interpreter', 'Latex');
ylabel('${\omega}_3$', 'Interpreter', 'Latex');
hold on

col_idx = 0;

for k = 1:size(free_param, 2)
    l=0;
    for i = 1:size(free_param, 2)        
        subplot(2,1,1)
        col = [1-col_idx/size(free_param, 2) 0.2+l/size(free_param, 2)*0.6 col_idx/size(free_param, 2)*(1-l/size(free_param, 2))];
        try
            plot(sim(i,k).traj.muhat(:,1), 'Color', col)
            % color legend (om2 & om3 values)
            subplot(2,1,2)
            plot(free_param(1,k),free_param(2,i), '.', 'MarkerSize', 20, 'color', col)
        end
        
        l=l+1;
    end
    col_idx=col_idx+1;
end

%muhat1 (prior mean)
subplot(2,1,1)
plot(sim(round(i/2),round(k/2)).traj.muhat(:,1), 'Color', 'black', 'LineWidth', 2)
plot(pilot.traj.probs_green, 'Color', 'black')
% color legend (om2 & om3 prior mean)
subplot(2,1,2)
plot(free_param(1,round(k/2)),free_param(2,round(i/2)), 'x', 'MarkerSize', 20, 'LineWidth', 2, 'color', 'black')

figdir = fullfile(saveDir, 'figures', 'pilots', 'priors',...
    ['Initial_priors_ehgf_grid_om2_om3']);
print(figdir, '-dtiff');
close;


%% plot trajs from extreme param values
% om3 = -4, om2 \in [-9, -3, 3]
figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
for j = 1:3
    subplot(3,2,7-2*j)
    plot(sim(1,1).traj.muhat(:,j))
    hold on
    plot(sim(ceil(size(free_param,2)/2),1).traj.muhat(:,j))
    try
        plot(sim(size(free_param,2),1).traj.muhat(:,j))
    end
    if j == 1
        plot(pilot.traj.u, '.k')
        plot(pilot.traj.probs_green, 'Color', 'black')
        ylim([-0.1 1.1]);
    end
    xlabel('trials')
    ytxt = ['$\hat{\mu}_{', num2str(j), '}$'];
    ylabel(ytxt, 'Interpreter', 'Latex')
    txt = ['muhat_' num2str(j)];
    title(txt)
    subplot(3,2,8-2*j)
    plot(sim(1,1).traj.sahat(:,j))
    hold on
    plot(sim(ceil(size(free_param,2)/2),1).traj.sahat(:,j))
    try
        plot(sim(size(free_param,2),1).traj.sahat(:,j))
    end
    xlabel('trials')
    ytxt = ['$\hat{\sigma}_{', num2str(j), '}$'];
    ylabel(ytxt, 'Interpreter', 'Latex')
    txt = ['sahat_' num2str(j)];
    title(txt)
end
legend({'\omega_{2}=-9 \omega_{3}=-4', '\omega_{2}=-3 \omega_{3}=-4', '\omega_{2}=3 \omega_{3}=-4'},...
        'Position', [0.94 0.48 0.03 0.07])
figdir = fullfile(saveDir, 'figures', 'pilots', 'priors',...
    ['Initial_priors_ehgf_om2var_om3_', num2str(round(free_param(2,1)))]);
print(figdir, '-dtiff');
close;

%% plot trajs from extreme param values
% om3 = 2, om2 \in [-9, -3, 3]
figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
for j = 1:3
    subplot(3,2,7-2*j)
    plot(sim(1,ceil(size(free_param,2)/2)).traj.muhat(:,j))
    hold on
    plot(sim(ceil(size(free_param,2)/2),ceil(size(free_param,2)/2)).traj.muhat(:,j))
    try
        plot(sim(size(free_param,2),ceil(size(free_param,2)/2)).traj.muhat(:,j))
    end
    if j == 1
        plot(pilot.traj.u, '.k')
        plot(pilot.traj.probs_green, 'Color', 'black')
        ylim([-0.1 1.1]);
    end
    xlabel('trials')
    ytxt = ['$\hat{\mu}_{', num2str(j), '}$'];
    ylabel(ytxt, 'Interpreter', 'Latex')
    txt = ['muhat_' num2str(j)];
    title(txt)
    subplot(3,2,8-2*j)
    plot(sim(1,ceil(size(free_param,2)/2)).traj.sahat(:,j))
    hold on
    plot(sim(ceil(size(free_param,2)/2),ceil(size(free_param,2)/2)).traj.sahat(:,j))
    try
        plot(sim(size(free_param,2),ceil(size(free_param,2)/2)).traj.sahat(:,j))
    end
    xlabel('trials')
    ytxt = ['$\hat{\sigma}_{', num2str(j), '}$'];
    ylabel(ytxt, 'Interpreter', 'Latex')
    txt = ['sahat_' num2str(j)];
    title(txt)
end
legend({'\omega_{2}=-9 \omega_{3}=2', '\omega_{2}=-3 \omega_{3}=2', '\omega_{2}=3 \omega_{3}=2'},...
        'Position', [0.94 0.48 0.03 0.07])
figdir = fullfile(saveDir, 'figures', 'pilots', 'priors',...
    ['Initial_priors_ehgf_om2var_om3_', num2str(round(free_param(2,ceil(size(free_param,2)/2))))]);
print(figdir, '-dtiff');
close;

%% plot trajs from extreme param values
% om3 = 8, om2 \in [-9, -3, 3]
figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
for j = 1:3
    subplot(3,2,7-2*j)
    plot(sim(1,size(free_param,2)).traj.muhat(:,j))
    hold on
    plot(sim(ceil(size(free_param,2)/2),size(free_param,2)).traj.muhat(:,j))
    try
        plot(sim(size(free_param,2),size(free_param,2)).traj.muhat(:,j))
    end
    if j == 1
        plot(pilot.traj.u, '.k')
        plot(pilot.traj.probs_green, 'Color', 'black')
        ylim([-0.1 1.1]);
    end
    xlabel('trials')
    ytxt = ['$\hat{\mu}_{', num2str(j), '}$'];
    ylabel(ytxt, 'Interpreter', 'Latex')
    txt = ['muhat_' num2str(j)];
    title(txt)
    subplot(3,2,8-2*j)
    plot(sim(1,size(free_param,2)).traj.sahat(:,j))
    hold on
    plot(sim(ceil(size(free_param,2)/2),size(free_param,2)).traj.sahat(:,j))
    try
        plot(sim(size(free_param,2),size(free_param,2)).traj.sahat(:,j))
    end
    xlabel('trials')
    ytxt = ['$\hat{\sigma}_{', num2str(j), '}$'];
    ylabel(ytxt, 'Interpreter', 'Latex')
    txt = ['sahat_' num2str(j)];
    title(txt)
end
legend({'\omega_{2}=-9 \omega_{3}=8', '\omega_{2}=-3 \omega_{3}=8', '\omega_{2}=3 \omega_{3}=8'},...
        'Position', [0.94 0.48 0.03 0.07])
figdir = fullfile(saveDir, 'figures', 'pilots', 'priors',...
    ['Initial_priors_ehgf_om2var_om3_', num2str(round(free_param(2,size(free_param,2))))]);
print(figdir, '-dtiff');
close;

%% sample prior 3-level ehgf for binary inputs
clear sim;
om2 = NaN(100,1);
om3 = NaN(100,1);

% define 100 parameter values (1 priormu + 99 samples)
n_samples = 100;
om2(1) = c.c_prc.priormus(13);
om3(1) = c.c_prc.priormus(14);
for i = 2:n_samples
    om2(i) = normrnd(om2(1), sqrt(c.c_prc.priorsas(13)));
    om3(i) = normrnd(om3(1), sqrt(c.c_prc.priorsas(14)));
end
 
% iterate through 100 samples
for j = 1:n_samples

    prc_vect_nat(14) = om3(j);
    prc_vect_nat(13) = om2(j);

    try
        % generate synthetic data
        sim(j) = tapas_simModel(pilot.traj.u,...
            pilot.ModSpace(1).prc,...
            prc_vect_nat,...
            'tapas_unitsq_sgm',...'ulmia_obs_sa2mh3',...
            obs_vect_nat,...
            options.rng.settings.State(1)); %seed
    end

end

% plot prior predictive distr (muhat1)
figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0.3 1 0.6]);

% input
plot(pilot.traj.u, '.', 'color', options.col.grn)
ylim([-0.1 1.1]);
ylabel('$\hat{\mu}_1$', 'Interpreter', 'Latex');
xlabel('Trial')
hold on

col_idx = 0;

l=0;
for i = 1:n_samples     
    try
        plot(sim(i).traj.muhat(:,1), 'Color', options.col.tnub)
    end
    l=l+1;
end

plot(sim(1).traj.muhat(:,1), 'Color', 'black', 'LineWidth', 2)
plot(pilot.traj.probs_green, 'color', options.col.grn)
figdir = fullfile(saveDir, 'figures', 'pilots', 'priors',...
    ['Initial_priors_ehgf_sample_om2_om3']);
print(figdir, '-dtiff');
close;

%% plot 3b-eHGF prior pred. dens.
figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
for j = 1:3
    subplot(3,2,7-2*j)
    plot(sim(1).traj.muhat(:,j), 'color', options.col.tnub)
    hold on
    for n = 2:100
        try
            plot(sim(n).traj.muhat(:,j), 'color', options.col.tnub)
        end
    end
    xlabel('trials')
    ytxt = ['$\hat{\mu}_{', num2str(j), '}$'];
    ylabel(ytxt, 'Interpreter', 'Latex')
    txt = ['muhat_' num2str(j)];
    title(txt)
    if j == 3
        ylim([-6 2])
    end
end
for k = 1:3
    subplot(3,2,8-2*k)
    plot(sim(1).traj.sahat(:,k), 'color', options.col.tnub)
    hold on
    for n = 2:100
        try
            plot(sim(n).traj.sahat(:,k), 'color', options.col.tnub)
        end
    end
    xlabel('trials')
    ytxt = ['$\hat{\sigma}_{', num2str(k), '}$'];
    ylabel(ytxt, 'Interpreter', 'Latex')
    txt = ['sahat_' num2str(k)];
    title(txt)
    if k == 3
        ylim([0 800])
    end
end
figdir = fullfile(saveDir, 'figures', 'pilots', 'priors',...
    ['Initial_priors_ehgf_prior_pred_dens']);
print(figdir, '-dtiff');
close;

end
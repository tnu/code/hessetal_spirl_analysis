function [] = spirl_create_results_dir(options)
% [] = spirl_create_results_dir(options)
%
% Helper function to create directories for saving results & figures.
%
% INPUT
%   options      struct        Settings for analysis pipeline
%
%   OPTIONAL:
%
% OUTPUT    
%   argout       type
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

% create dir to store results
for n = 1:options.pilots.nS
    mkdir(fullfile(pwd, 'results', 'pilots', ['sub', num2str(n)]));
end
for n = 1:options.sim.indiv.nS
    mkdir(fullfile(pwd, 'results', 'sim', 'indiv', ['sub', num2str(n)]));
end

mkdir(fullfile(pwd, 'results', 'sim', 'family'));

for n = 1:options.main.nS
    mkdir(fullfile(pwd, 'results', 'main', ['sub', num2str(n)]));
end

% create dir to store figures
mkdir(fullfile(pwd, 'figures', 'pilots', 'model_agnostic'))
mkdir(fullfile(pwd, 'figures', 'pilots', 'priors', 'param_indiv'))
mkdir(fullfile(pwd, 'figures', 'pilots', 'fits', 'comb'))
mkdir(fullfile(pwd, 'figures', 'pilots', 'fits', 'logRT'))
mkdir(fullfile(pwd, 'figures', 'pilots', 'fits', 'opt_algo'))

mkdir(fullfile(pwd, 'figures', 'sim', 'indiv', 'traj'))
mkdir(fullfile(pwd, 'figures', 'sim', 'indiv', 'rec', 'comb_traj'))
mkdir(fullfile(pwd, 'figures', 'sim', 'family', 'ROC_xp'))
mkdir(fullfile(pwd, 'figures', 'sim', 'family', 'ROC_Ef'))

mkdir(fullfile(pwd, 'figures', 'main', 'model_agnostic'))
mkdir(fullfile(pwd, 'figures', 'main', 'fits', 'comb'))
mkdir(fullfile(pwd, 'figures', 'main', 'fits', 'logRT'))
mkdir(fullfile(pwd, 'figures', 'main', 'fits', 'opt_algo'))
mkdir(fullfile(pwd, 'figures', 'main', 'ppc', 'bin'))
mkdir(fullfile(pwd, 'figures', 'main', 'ppc', 'cont'))

mkdir(fullfile(pwd, 'figures', 'paper'))
mkdir(fullfile(pwd, 'figures', 'ccn_2023'))

end
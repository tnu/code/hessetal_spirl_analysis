function [] = spirl_plot_pilot_prior_pdf(model, idx, type)
% [] = spirl_plot_pilot_prior_pdf(model, idx, type)
%
% Visualizes prior distribution of a specific param idx from a given model
% under the specified empirical priors.
%
% INPUT
%   model       struct       Model specs and fits
%   idx         integer      Idx of model parameter to be visualized
%   type        string       Model type: 'prc' or 'obs'
%
%   OPTIONAL:
%
% OUTPUT    
%   argout       type         
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

if strcmp(type, 'prc')

    x_min = model.prc_priormus(idx)-3*model.prc_priorsas(idx);
    x_max = model.prc_priormus(idx)+3*model.prc_priorsas(idx);
    x = x_min:0.1:x_max;
    
    y = normpdf(x, model.prc_robmean(1,idx), sqrt(model.prc_robvar(1,idx)));
    y_prior = normpdf(x, model.prc_priormus(idx), sqrt(model.prc_priorsas(idx)));
    
    figure
    plot(x, y, 'k')
    hold on
    plot(x, y_prior, 'k--')
    plot(model.prc_est(:,idx), -0.05, 'ko')
    
    ylim([-0.1 1])
    str = sprintf('mu = %1.2f, Sa = %1.2f', model.prc_robmean(1,idx), model.prc_robvar(1,idx));
    T = text(min(get(gca, 'xlim')), max(get(gca, 'ylim')), str);
    set(T, 'fontsize', 12, 'verticalalignment', 'top', 'horizontalalignment', 'left');
    legend('pilot prior', 'initial prior', 'MAP estimates')
    
    if idx == 1
        title('\omega_2')
    elseif idx == 2
        title('\omega_3')
    end
    
elseif strcmp(type, 'obs')
    
    if idx == 1
        x_min = -3;
        x_max = 8;
    else
        x_min = model.obs_priormus(idx)-3*model.obs_priorsas(idx);
        x_max = model.obs_priormus(idx)+3*model.obs_priorsas(idx);
    end
    x = x_min:0.1:x_max;
    
    y = normpdf(x, model.obs_robmean(1,idx), sqrt(model.obs_robvar(1,idx)));
    y_prior = normpdf(x, model.obs_priormus(idx), sqrt(model.obs_priorsas(idx)));
    
    figure
    plot(x, y, 'k')
    hold on
    plot(x, y_prior, 'k--')
    plot(model.obs_est(:,idx), -0.05, 'ko')
    
    ylim([-0.1 1])
    str = sprintf('mu = %1.2f, Sa = %1.2f', model.obs_robmean(1,idx), model.obs_robvar(1,idx));
    T = text(min(get(gca, 'xlim')), max(get(gca, 'ylim')), str);
    set(T, 'fontsize', 12, 'verticalalignment', 'top', 'horizontalalignment', 'left');
    legend('pilot prior', 'initial prior', 'MAP estimates')
    
    if idx == 1
        title('log(\zeta)')
    elseif idx == size(model.obs_est, 2)
        title('log(\Sigma)')
    elseif idx == 2
        title('\beta_0')
    elseif idx == 3
        title('\beta_1')
    elseif idx == 4
        title('\beta_2')
    elseif idx == 5
        title('\beta_3')
    elseif idx == 6
        title('\beta_4')
    end
    
end

end
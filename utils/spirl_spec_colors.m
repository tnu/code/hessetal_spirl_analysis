function [col] = spirl_spec_colors()
% [col] = spirl_spec_colors()
%
% This function returns a struct col with fields containing 1x3 vectors
% (RGB triplets normalized to unity). Current colors: white, grey,
% tnu_blue, green.
%
% INPUT
%   argin        type           
%
%   OPTIONAL:
%
% OUTPUT    
%   col          struct       Struct with color fields (RGB triplets)
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

% define colors
col.wh = [1 1 1];
col.gry = [0.5 0.5 0.5];
col.tnub = [0 110 182]/255;
col.grn = [0 0.6 0];

end
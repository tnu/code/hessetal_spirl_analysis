function [ModelSpace] = spirl_setup_model_space()
% [ModelSpace] = spirl_setup_model_space()
%
% Creates and outputs a struct of the model space used in the model-based
% part of the analysis containing config files for perceptual and response
% models.
%
% INPUT
%   argin            type           
%
%   OPTIONAL:
%
% OUTPUT    
%   ModelSpace       struct      Struct containing prc & obs model configs
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

%% init struct
ModelSpace = struct();

%% Perceptual Models

% 3-level binary eHGF
for i = 1:7
    ModelSpace(i).prc = 'spirl_tapas_ehgf_binary';
    ModelSpace(i).prc_config = eval('spirl_tapas_ehgf_binary_config');
end


%% Response Models

%--------------------------------------------------------------------------
% Informed RT family

% M1: Lawson-inspired model
ModelSpace(1).name = 'M1';
ModelSpace(1).obs = 'm1_comb_obs';
ModelSpace(1).obs_config = eval('m1_comb_obs_config');

% M2: Informational uncertainty and log-volatility model
ModelSpace(2).name = 'M2';
ModelSpace(2).obs = 'm2_comb_obs';
ModelSpace(2).obs_config = eval('m2_comb_obs_config');

% M3: Informational, environmental and outcome uncertainty model
ModelSpace(3).name = 'M3';
ModelSpace(3).obs = 'm3_comb_obs';
ModelSpace(3).obs_config = eval('m3_comb_obs_config');

% M4: Informational & environmental uncertainty model
ModelSpace(4).name = 'M4';
ModelSpace(4).obs = 'm4_comb_obs';
ModelSpace(4).obs_config = eval('m4_comb_obs_config');

%--------------------------------------------------------------------------
% Uninformed RT family

% M5: Null model
ModelSpace(5).name = 'M5';
ModelSpace(5).obs = 'm5_comb_obs';
ModelSpace(5).obs_config = eval('m5_comb_obs_config');

% M6: Linear decay model
ModelSpace(6).name = 'M6';
ModelSpace(6).obs = 'm6_comb_obs';
ModelSpace(6).obs_config = eval('m6_comb_obs_config');

% M7: Null model with 2 intercepts
ModelSpace(7).name = 'M7';
ModelSpace(7).obs = 'm7_comb_obs';
ModelSpace(7).obs_config = eval('m7_comb_obs_config');


%% find free parameters
for i = 1:size(ModelSpace,2)
    % prc
    prc_idx = ModelSpace(i).prc_config.priorsas;
    prc_idx(isnan(prc_idx)) = 0;
    ModelSpace(i).prc_idx = find(prc_idx);
    % obs
    obs_idx = ModelSpace(i).obs_config.priorsas;
    obs_idx(isnan(obs_idx)) = 0;
    ModelSpace(i).obs_idx = find(obs_idx);
end

end
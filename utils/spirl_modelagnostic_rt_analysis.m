function [] = spirl_modelagnostic_rt_analysis(SPIRL, logRT_mat, trials, traj, options, type, saveDir)
% [] = spirl_modelagnostic_rt_analysis(SPIRL, logRT_mat, trials, traj, options, type, saveDir)
%
% Generates figures of model-agnostic analysis of rt data.
%
% INPUT
%   SPIRL        struct       Data (binary inputs and responses, rt)
%   logRT_mat    matrix       160xN matrix of log-transformed rt data
%   trials       vector       1x160 vector with trial indices
%   traj         struct       Input generating probability trajectory
%   options      struct       Specified settings of the analysis pipeline
%   type         string       Data set: 'pilots' or 'main'
%   saveDir      string       Dir where figures are saved
%
%   OPTIONAL:
%
% OUTPUT    
%   argout       type         
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

%% specify Colors for plotting
% sepcify color map
options.col.idx = ones(1,length(trials));
for i = 1:length(trials)
    if ismember(traj.block.code(i),[1,6,8,13]) 
        options.col.idx(i) = 2;
    elseif ismember(traj.block.code(i),[7])
        options.col.idx(i) = 3;
    end
end
options.col.map = [options.col.tnub; options.col.wh; options.col.gry];

%% Plot Probability Trajectory & Binary Inputs
scrsz = get(0,'screenSize');
outerpos1 = [0.275*scrsz(3),0.4*scrsz(4),0.45*scrsz(3),0.4*scrsz(4)];
figure(...
    'OuterPosition', outerpos1,...
    'Name', 'Probability Trajectory & Inputs');
% probability trajectory
plot(traj.probs_green, 'Color', options.col.grn)
xlabel('Trial')
ylim([-0.1 1.1])
ylabel('P(reward | green card)')
hold on
% inputs
plot(traj.u, '.', 'Color', options.col.grn) % (1=green card, 0=yellow)
% color code different phases
ax = axis;
fill([trials, fliplr(trials)],...
    [ax(3)*ones(1,length(trials)), ax(4)*ones(1,length(trials))],...
    [options.col.idx, fliplr(options.col.idx)], 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
colormap(options.col.map)

figdir = fullfile(saveDir, 'figures', type, 'model_agnostic',...
    ['Probability_Traj_Inputs']);
print(figdir, '-dtiff');
close;

%% plot Histogram (logRT)
outerpos2 = [0.05*scrsz(3),0.05*scrsz(4),0.5*scrsz(3),0.95*scrsz(4)];
figure('OuterPosition', outerpos2)
subplot(2,1,1)
ax1 = histogram(logRT_mat);
ax1.FaceColor = options.col.tnub; % bar colors
title('logRT')
ylabel('Frequency')
xlabel('[ms]')
subplot(2,1,2)
ax2 = histogram(exp(logRT_mat));
ax2.FaceColor = options.col.gry; % bar colors
title('Response Times')
ylabel('Frequency')
xlabel('[ms]')

figdir = fullfile(saveDir, 'figures', type, 'model_agnostic',...
    ['Histogram_logRT']);
print(figdir, '-dtiff');
close;

%% Quantile-Quantile (QQ) plot (logRT)
% stack all RTs into one single vector
logRT_reshaped = reshape(logRT_mat, size(logRT_mat, 1)*size(logRT_mat, 2), 1);
outerpos3 = [0.55*scrsz(3),0.2*scrsz(4),0.4*scrsz(3),0.7*scrsz(4)];
figure('OuterPosition', outerpos3)
ax3 = qqplot(logRT_reshaped);
ax3(1).MarkerEdgeColor = options.col.tnub;
disp('Correlation between sample data and normal quantiles:')
disp(corr(ax3(1).YData(~isnan(ax3(1).YData))', ax3(1).XData(~isnan(ax3(1).XData))'))

figdir = fullfile(saveDir, 'figures', type, 'model_agnostic',...
    ['QQ_plot_logRT']);
print(figdir, '-dtiff');
close;

%% individual RT trajectories
figure('OuterPosition', outerpos2)
% logRTs
plot(logRT_mat, 'Color', options.col.tnub)
title('logRT')
ylabel('[ms]')
xlabel('Trial')
hold on
% color code different phases
ax = axis;
fill([trials, fliplr(trials)],...
    [ax(3)*ones(1,length(trials)), ax(4)*ones(1,length(trials))],...
    [options.col.idx, fliplr(options.col.idx)], 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
colormap(options.col.map)

figdir = fullfile(saveDir, 'figures', type, 'model_agnostic',...
    ['logRT_traj_all_subjects']);
print(figdir, '-dtiff');
close;

%% avg RT trajectories
% calc RT avg (+- std_dev)
avg_logRT = mean(logRT_mat,2,'omitnan');
var_logRT = var(logRT_mat, 0, 2, 'omitnan');
upper = avg_logRT + sqrt(var_logRT);
lower = avg_logRT - sqrt(var_logRT);
% plot
outerpos4 = [0.275*scrsz(3),0.4*scrsz(4),0.5*scrsz(3),0.38*scrsz(4)];
figure(...
    'OuterPosition', outerpos4,...
    'Name', 'AVG logRT Trajectory');
plot(avg_logRT, 'LineWidth', 2, 'Color', options.col.tnub)
hold on
% shade st_dev
fill([trials, fliplr(trials)], [(upper)', fliplr((lower)')], ...
         options.col.tnub, 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
% color code different phases
ax = axis;
fill([trials, fliplr(trials)],...
    [ax(3)*ones(1,length(trials)), ax(4)*ones(1,length(trials))],...
    [options.col.idx, fliplr(options.col.idx)], 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
colormap(options.col.map)
ylabel('log Reaction Time [ms]')
xlabel('Trial')
title('Mean \pm Standard deviation of logRTs (avg over all pilot subjects)')

figdir = fullfile(saveDir, 'figures', type, 'model_agnostic',...
    ['avg_logRT_traj']);
print(figdir, '-dtiff');
close;

%% blocks
block_logRT = {};
for i = 1:max(traj.block.code)
    % all participants
    block_logRT{i} = reshape(logRT_mat(traj.block.code==i, :), [size(SPIRL, 2)*traj.block.length(i), 1]);
end

%% phases
phase_logRT{1,1} = [block_logRT{1}; block_logRT{6}; block_logRT{8}; block_logRT{13}]; %stable
phase_logRT{2,1} = [block_logRT{2}; block_logRT{3}; block_logRT{4}; block_logRT{5};...
    block_logRT{9}; block_logRT{10}; block_logRT{11}; block_logRT{12}]; %volatile
phase_logRT{3,1} = block_logRT{7}; %random

%% Raincloud plots
rm_raincloud(phase_logRT, options.col.tnub);
ticklabels = {'stable' 'volat' 'random'};
set(gca,'yticklabel', ticklabels)
xlabel('logRT [ms]')

figdir = fullfile(saveDir, 'figures', type, 'model_agnostic',...
    ['logRT_phases_raincloud_plot']);
print(figdir, '-dtiff');
close;

end
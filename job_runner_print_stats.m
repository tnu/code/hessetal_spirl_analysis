function [] = job_runner_print_stats(EULER)
% [] = job_runner_print_stats(EULER)
%
% Creates figures for all steps of the entire analysis pipeline.
%
% INPUT
%   EULER        binary           Binary indicator variable
%
%   OPTIONAL:
%
% OUTPUT    
%   argout       type
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

%% setup path
saveDir = spirl_setup_paths(EULER);

%% load analysis specifications
options = load(fullfile(saveDir, 'results', ['options']));
rng(123, 'twister');
options.rng.settings = rng;

%% load final results
res = load(fullfile(saveDir, 'results', 'main', ['main_results']));

%% model-agnostic RT analysis (pilot data)
% ANOVA table & stats
disp('ANOVA of avg logRT over subjects including factor phase (pilot data):')
disp(res.pilot.anova.tbl)
disp(res.pilot.anova.stats)

%% model-agnostic RT analysis (main data)
% ANOVA table & stats
disp('ANOVA of avg logRT over subjects including factor phase (pilot data):')
disp(res.main.anova.tbl)
disp(res.main.anova.stats)

%% sufficient stats of empirical priors (M1)
m=1;
disp('Sufficient statistics of empirical prior means of M1:')
disp(res.main.ModSpace(m).prc_config)
disp(res.main.ModSpace(m).obs_config)

%% parameter recovery of all models (PCC incl p-values)
for m = 1:7
    fprintf('Parameter recovery results of M%i (pre-defined threshold: p < 0.05) \n', m)
    disp('PCCs (perceptual model parameters):')
    disp(res.rec.param.prc(m).pcc)
    disp('P-values (perceptual model parameters):')
    disp(res.rec.param.prc(m).pval)
    disp('PCCs (response model parameters):')
    disp(res.rec.param.obs(m).pcc)
    disp('P-values (response model parameters):')
    disp(res.rec.param.obs(m).pval)
end

%% model identifiability
% LME winner classification (balanced accuracy)
disp('Model identifiability - LME winner classification')
fprintf('Balanced Accuracy = %1.2f (pre-defined threshold: balanced_acc > %1.2f) \n',...
    res.rec.class.balacc, res.rec.class.chancethr)
disp('LME winner freq confusion matrix:')
disp(res.rec.class.percLMEwinner)

% rfx bms (PXPs)
disp('Model identifiability - RFX BMS')
disp('PXP confusion matrix (pre-defined threshold: PXP > 0.90):')
disp(res.rec.bmc.rfx.pxp)

%% family level recovery analysis
% true fam freq vs. Ef / XP (only plots and no threshold)

%% results from family level rfx bms on main data set (RQ1)
disp('Family level RFX BMS results (pre-defined threshold: XP > 0.95)')
disp('Exceedance probabilities (XP) (informed RT family vs. uninformed RT family):')
disp(res.main.fam.out.families.ep)
disp('Posterior family probabilities (expected frequencies, Ef) (informed RT family vs. uninformed RT family):')
disp(res.main.fam.out.families.Ef)

%% results from model level rfx bms on main data set (RQ2)
disp('Individual model level RFX BMS results (pre-defined threshold: PXP > 0.90)')
disp('Protected exceedance probabilities (PXP) of M1-M7:')
disp(res.main.indiv.out.pxp)
disp('Posterior model probabilities (expected frequencies, Ef) of M1-M7:')
disp(res.main.indiv.out.Ef)

%% one-sample t-test (difference between MAP and prior mean of logRT GLM params of main data set)
% M1, be1-be4
disp('one-sample t-tests (MAP estimates vs prior mean, Bonferoni-corrected):')
disp('(pre-defined threshold: p < 0.0125)')
disp('1=H0 rejected, 0=H0 NOT rejected')
for j = 1:4
    fprintf('beta_{%i} \n', j)
    fprintf('h=%i \n', res.main.param.ttest(j).h)
    fprintf('p=%1.5f \n', res.main.param.ttest(j).p)
    disp('CI:')
    disp(res.main.param.ttest(j).ci)
    disp(res.main.param.ttest(j).stats)
end

%% Posterior predictive checks M1
m=1;
% binary
disp('Adjusted correctness of binary responses (M1):')
for n = [2 3 14 17 18 28 31 42 46 51] % 10 random subjects of main data set
    fprintf('empirical sub %i: %1.2f  \n', res.main.ppc.ppc_sub(n), res.main.SPIRL(res.main.ppc.ppc_sub(n)).adj_correct)
    disp('avg of 100 synthetic subjects generated from posterior:')
    disp(mean(res.main.ppc.mod(m).sub(n).adj_correct, 'omitnan'))
end

%% print message
disp('all stats printed.')

end
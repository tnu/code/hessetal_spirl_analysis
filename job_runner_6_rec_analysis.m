function [] = job_runner_6_rec_analysis(EULER)
% [] = job_runner_6_rec_analysis(EULER)
%
% Calculates metrics for model identifiability and parameter recovery
% analyses.
%
% INPUT
%   EULER        binary           Binary indicator variable
%
%   OPTIONAL:
%
% OUTPUT    
%   argout       type
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

%% setup path
saveDir = spirl_setup_paths(EULER);

%% load analysis specifications
options = load(fullfile(saveDir, 'results', ['options']));
rng(123, 'twister');
options.rng.settings = rng;

%% load model space for analysis of the main data set
main = load(fullfile(saveDir, 'results', 'main', ['model_space']));

%% load simulated data
sim = load(fullfile(saveDir, 'results', 'sim', 'indiv', ['simulated_data']));

%% create res struct
res = struct();
res.main = main;
res.sim = sim;

%% load and store results from model inversion
rec = struct();
for n = 1:options.sim.indiv.nS
    for m = 1:size(res.main.ModSpace, 2)
        fprintf('current iteration: n=%1.0f, m=%1.0f \n', n,m);
        for i = 1:size(res.main.ModSpace, 2)
            % load results from model inversion
            rec.est(m,n,i).data = load(fullfile(saveDir, 'results',...
            'sim', 'indiv', ['sub', num2str(n)],...
            ['sim_mod', num2str(m), '_est_mod', num2str(i)]));
            % store LME in matrix
            rec.model(m).LME(n,i) = rec.est(m,n,i).data.optim.LME;
        end
        % store parameter values (sim & est)
        rec.param.prc(m).sim(n,:) = res.sim.sub(n,m).input.prc.transInp(res.main.ModSpace(m).prc_idx);
        rec.param.obs(m).sim(n,:) = res.sim.sub(n,m).input.obs.transInp(res.main.ModSpace(m).obs_idx);
        rec.param.prc(m).est(n,:) = rec.est(m,n,m).data.p_prc.ptrans(res.main.ModSpace(m).prc_idx);
        rec.param.obs(m).est(n,:) = rec.est(m,n,m).data.p_obs.ptrans(res.main.ModSpace(m).obs_idx);        
    end
end

%% model identifiability (LME Winner classification)

% pre-allocate
class.LMEwinner = NaN(size(res.main.ModSpace, 2), size(res.main.ModSpace, 2));
class.percLMEwinner = NaN(size(class.LMEwinner));

% calc winner freq for each data generating model
for m = 1:size(res.main.ModSpace, 2)
    [class.max(m).val, class.max(m).idx] = max(rec.model(m).LME, [], 2);
    for i = 1:size(res.main.ModSpace, 2)
        class.LMEwinner(m,i) = sum(class.max(m).idx==i);
    end
    class.percLMEwinner(m,:) = class.LMEwinner(m,:)./options.sim.indiv.nS;
    % accuracy
    class.acc(m) = class.percLMEwinner(m,m);
end

% balanced accuraccy
class.balacc = mean(class.acc);
% chance threshold (inv binomial distr)
class.chancethr = binoinv(0.9, options.sim.indiv.nS, 1/size(res.main.ModSpace, 2)) / options.sim.indiv.nS;
% save to struct
rec.class = class;

%% parameter recovery (Pearson's correlation coefficient)
for m = 1:size(res.main.ModSpace, 2)
    % prc model
    [prc_coef, prc_p] = corr(rec.param.prc(m).sim, rec.param.prc(m).est);
    rec.param.prc(m).pcc = diag(prc_coef);
    rec.param.prc(m).pval = diag(prc_p);
    % obs model
    [obs_coef, obs_p] = corr(rec.param.obs(m).sim, rec.param.obs(m).est);
    rec.param.obs(m).pcc = diag(obs_coef);
    rec.param.obs(m).pval = diag(obs_p);
end

%% model identifiability (RFX BMS)

% pre-allocate
bmc.rfx.Ef = NaN(size(res.main.ModSpace, 2), size(res.main.ModSpace, 2));
bmc.rfx.ep = NaN(size(bmc.rfx.Ef));
bmc.rfx.pxp = NaN(size(bmc.rfx.Ef));

% toolbox settings for BMS results display
bmc.opt.verbose = false;
bmc.opt.DisplayWin = false;

% run BMS for each data generating model
for m = 1:size(res.main.ModSpace, 2)
    L = rec.model(m).LME';
    [bmc.post, bmc.out] = VBA_groupBMC(L, bmc.opt); %evtl add options...
    bmc.rfx.Ef(m,:) = bmc.out.Ef';
    bmc.rfx.ep(m,:) = bmc.out.ep;
    bmc.rfx.pxp(m,:) = bmc.out.pxp;
end

% save to struct
rec.bmc = bmc;

%% save results as struct
save_path = fullfile(saveDir, 'results', 'sim', 'indiv', ['recovery_analysis']);
save(save_path, '-struct', 'rec');

disp('recovery analysis complete.')
disp('ready for family level simulations.')

end
function [] = job_runner_ccn_figs(EULER)
% [] = job_runner_ccn_figs(EULER)
%
% Creates figures used for the CCN conference paper (2023).
%
% INPUT
%   EULER        binary           Binary indicator variable
%
%   OPTIONAL:
%
% OUTPUT    
%   argout       type
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

%% setup path
saveDir = spirl_setup_paths(EULER);

%% load analysis specifications
options = load(fullfile(saveDir, 'results', ['options']));
rng(123, 'twister');
options.rng.settings = rng;

%% load final results
res = load(fullfile(saveDir, 'results', 'main', ['main_results']));

%% specify colors
options.col.idx = ones(1,length(res.main.trials));
for i = 1:length(res.main.trials)
    if ismember(res.main.traj.block.code(i),[1,6,8,13]) 
        options.col.idx(i) = 2;
    elseif ismember(res.main.traj.block.code(i),[7])
        options.col.idx(i) = 3;
    end
end
options.col.map = [options.col.tnub; options.col.wh; options.col.gry];

%% collect binary predictions (main data set, N=59)
y_bin = NaN(size(res.main.traj.u,1),options.main.nS);
for n = 1:options.main.nS
    y_bin(:,n) = res.main.SPIRL(n).y(:,1);
end

%% average binary predictions 
% (= avg correct responses, inverted for blocks with p=0.2)
scrsz = get(0,'screenSize');
outerpos1 = [0.275*scrsz(3),0.4*scrsz(4),0.45*scrsz(3),0.4*scrsz(4)];
figure(...
    'OuterPosition', outerpos1,...
    'Name', 'Probability Trajectory & Inputs');
% probability trajectory
plot(res.main.traj.probs_green, 'Color', [0 0 0], 'LineWidth',1)
% xlabel('Trial')
ylim([-0.1 1.1])
hold on
% inputs
plot(res.main.traj.u, '.', 'Color', options.col.grn) % (1=green card, 0=yellow)
y_bin_avg = mean(y_bin,2,'omitnan');
plot(1:160,y_bin_avg, 'Color',options.col.tnub)
ylim([0,6]);
legend('$p(r|c=1)$', '$u$', '$\overline{y}_{bin}$', 'Interpreter','latex', 'Position', [0.5 0.5 0.05 0.07])

% close;

%% average correct predictions 
% calc avg correct preds & invert avg binary pred
y_bin_avg_corr = y_bin_avg;
y_bin_avg_corr(res.main.traj.u==0) = 1-y_bin_avg_corr(res.main.traj.u==0);

scrsz = get(0,'screenSize');
outerpos2 = [0.275*scrsz(3),0.4*scrsz(4),0.45*scrsz(3),0.4*scrsz(4)];
figure(...
    'OuterPosition', outerpos2,...
    'Name', 'Probability Trajectory & Inputs');
% probability trajectory
plot(res.main.traj.probs_green, 'Color', [0 0 0], 'LineWidth',1)
xlabel('Trial')
ylim([-0.1 1.1])
hold on
% inputs
plot(res.main.traj.u, '.', 'Color', options.col.grn) % (1=green card, 0=yellow)
% color code different phases
ax = axis;
fill([res.main.trials, fliplr(res.main.trials)],...
    [ax(3)*ones(1,length(res.main.trials)), ax(4)*ones(1,length(res.main.trials))],...
    [options.col.idx, fliplr(options.col.idx)], 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
colormap(options.col.map)

plot(1:160,y_bin_avg_corr)

% close;

%% inverted avg correct predictions vs avg delta_1 from eHGF-3b (M1)
% get PE trajectories
da1 = NaN(size(res.main.traj.u,1),options.main.nS);
for n = 1:options.main.nS
    da1(:,n) = abs(res.main.est(1,n).traj.da(:,1));
end
da1_avg = mean(da1,2,'omitnan');

% invert avg binary pred
y_bin_avg_inverted = 1-y_bin_avg_corr;

% plot
figure(...
    'OuterPosition', outerpos1,...
    'Name', 'Probability Trajectory & Inputs');
% probability trajectory
plot(res.main.traj.probs_green, 'Color', [0 0 0], 'LineWidth',1)
xlabel('Trial')
ylim([-0.1 1.1])
hold on
% inputs
plot(res.main.traj.u, '.', 'Color', options.col.grn) % (1=green card, 0=yellow)
% color code different phases
ax = axis;
fill([res.main.trials, fliplr(res.main.trials)],...
    [ax(3)*ones(1,length(res.main.trials)), ax(4)*ones(1,length(res.main.trials))],...
    [options.col.idx, fliplr(options.col.idx)], 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
colormap(options.col.map)

plot(1:160,y_bin_avg_inverted)
plot(1:160,da1_avg)

% close;

%% MAIN: plot AVG rt trajectories & fits
mean_logRT = mean(res.main.logRT_mat, 2, 'omitnan');
var_logRT = var(res.main.logRT_mat, 0, 2, 'omitnan');
logupper = mean_logRT + sqrt(var_logRT);
loglower = mean_logRT - sqrt(var_logRT);
figure(...
    'OuterPosition', outerpos1,...
    'Name', 'Probability Trajectory & Inputs');
for m = 1%:size(res.main.ModSpace,2)
    for n = 1:options.main.nS
        yhat_mat(:,n) = res.main.est(m,n).optim.yhat(:,2);
    end
    mod(m).mean_yhat = mean(yhat_mat, 2, 'omitnan');
    mod(m).var_yhat = var(yhat_mat, 0, 2, 'omitnan');
    mod(m).upper_yhat = mod(m).mean_yhat + sqrt(mod(m).var_yhat);
    mod(m).lower_yhat = mod(m).mean_yhat - sqrt(mod(m).var_yhat);
    
    plot(res.main.trials, mean_logRT, 'LineWidth', 2, 'color', options.col.tnub)
    hold on
    fill([res.main.trials, fliplr(res.main.trials)], [(logupper)', fliplr((loglower)')], ...
             'b', 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
    plot(res.main.trials, mod(m).mean_yhat, 'LineWidth', 2)
    fill([res.main.trials, fliplr(res.main.trials)], [(mod(m).upper_yhat)', fliplr((mod(m).lower_yhat)')], ...
             'r', 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
    ylim([5.3 10])
    xlim([1 160])
end
legend('$log(y_{rt})$', '$\sqrt{Var(log(y_{rt}))}$', '$log(\hat{y}_{rt})$', '$\sqrt{Var(log(\hat{y}_{rt}))}$', 'Interpreter','latex', 'Position', [0.5 0.7 0.05 0.07])

% close;

%% average binary predictions & logRTs in the same plot
% (= avg correct responses, inverted for blocks with p=0.2)
scrsz = get(0,'screenSize');
outerpos1 = [0.275*scrsz(3),0.4*scrsz(4),0.45*scrsz(3),0.4*scrsz(4)];
figure(...
    'OuterPosition', outerpos1,...
    'Name', 'Probability Trajectory & Inputs');

subplot(2,1,1)
% probability trajectory
plot(res.main.traj.probs_green, 'Color', [0 0 0], 'LineWidth',1)
% xlabel('Trial')
ylim([-0.1 1.1])
ylabel('$u$, $\overline{y}_{bin}$, $p(r|c=1)$', 'Interpreter', 'latex')
hold on
% inputs
plot(res.main.traj.u, '.', 'Color', options.col.grn) % (1=green card, 0=yellow)
y_bin_avg = mean(y_bin,2,'omitnan');
plot(1:160,y_bin_avg, 'Color',options.col.tnub)
% color code different phases
ax = axis;
fill([res.main.trials, fliplr(res.main.trials)],...
    [ax(3)*ones(1,length(res.main.trials)), ax(4)*ones(1,length(res.main.trials))],...
    [options.col.idx, fliplr(options.col.idx)], 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
colormap(options.col.map)

subplot(2,1,2)
m = 1;
for n = 1:options.main.nS
    yhat_mat(:,n) = res.main.est(m,n).optim.yhat(:,2);
end
mod(m).mean_yhat = mean(yhat_mat, 2, 'omitnan');
mod(m).var_yhat = var(yhat_mat, 0, 2, 'omitnan');
mod(m).upper_yhat = mod(m).mean_yhat + sqrt(mod(m).var_yhat);
mod(m).lower_yhat = mod(m).mean_yhat - sqrt(mod(m).var_yhat);

plot(res.main.trials, mean_logRT, 'LineWidth', 2, 'color', options.col.tnub)
hold on
fill([res.main.trials, fliplr(res.main.trials)], [(logupper)', fliplr((loglower)')], ...
         'b', 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
plot(res.main.trials, mod(m).mean_yhat, 'LineWidth', 2)
fill([res.main.trials, fliplr(res.main.trials)], [(mod(m).upper_yhat)', fliplr((mod(m).lower_yhat)')], ...
         'r', 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
ylim([5.3 6.8])
xlim([1 160])
ylabel('$\log(y_{rt})$ [ms]', 'Interpreter', 'latex')
xlabel('trial number')

figdir = fullfile(saveDir, 'figures', 'ccn_2023',...
    ['fig1_traj_avg_bin_rt_M1']);
print(figdir, '-dpng');
print(figdir, '-dsvg'); %deps, dpdf, ... (vector graphics)
close;

%% REC: plot model ident (rfx bms, PXP)
% title
comp_tit = {'PXP (RFX BMS)'};
% axis labels
mod_nr = {};
for m = 1:size(res.main.ModSpace,2)
    mod_nr{m} = ['M', num2str(m)];
end

figure
bwr = @(n)interp1([1 2], [options.col.wh; options.col.tnub], linspace(1, 2, n), 'linear');
imagesc(res.rec.bmc.rfx.pxp)
colormap(bwr(64));
colorbar;
set(gca, 'clim', [0 1])
xlabel('Recovered');
ylabel('Simulated');
ax = gca;
ax.XTick = [1:size(res.main.ModSpace,2)];
ax.XTickLabel = mod_nr;
ax.YTick = [1:size(res.main.ModSpace,2)];
ax.YTickLabel = mod_nr;

pos=get(gca,'position');
[rows,cols]=size(res.rec.bmc.rfx.pxp);
nums = flip(res.rec.bmc.rfx.pxp',2);
width=pos(3)/(cols);
height=pos(4)/(rows);
for i=1:cols
      for j=1:rows               
        annotation('textbox',[pos(1)+width*(i-1),pos(2)+height*(j-1),width,height], ...
        'string',num2str(nums(i,j), '%.2f'),'LineStyle','none','HorizontalAlignment','center',...
        'VerticalAlignment','middle');
      end
end

% close;

%% REC: plot param rec
m = 1;
npars = length(res.main.ModSpace(m).prc_idx)+length(res.main.ModSpace(m).obs_idx);
nx = ceil(sqrt(npars));
ny = round(sqrt(npars));
figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
for j = 1:npars
    subplot(nx, ny, j)
    if j > length(res.main.ModSpace(m).prc_idx) %% all obs param
        k = j-length(res.main.ModSpace(m).prc_idx);
        scatter(res.rec.param.obs(m).sim(:,k), res.rec.param.obs(m).est(:,k), 15, 'k', 'filled');
        pcc = res.rec.param.obs(m).pcc(k);
        hline = refline(1,0);
        hline.Color = 'k';
        if k == 1
            title('log(\zeta)')
        elseif k == size(res.main.ModSpace(m).obs_idx, 2)
            title('log(\Sigma)')
        elseif k == 2
            title('\beta_0')
        elseif k == 3
            title('\beta_1')
        elseif k == 4
            title('\beta_2')
        elseif k == 5
            title('\beta_3')
        elseif k == 6
            title('\beta_4')
        end
    else
        scatter(res.rec.param.prc(m).sim(:,j), res.rec.param.prc(m).est(:,j), 15, 'k', 'filled');
        pcc = res.rec.param.prc(m).pcc(j);
        hline = refline(1,0);
        hline.Color = 'k';
        if j == 1
            title('\omega_2')
        elseif j == 2
            title('\omega_3')
        end
    end

    xlabel('Simulated values');
    ylabel('Recovered values');
    str = sprintf('r = %1.2f', pcc);
    textXpos = min(get(gca, 'xlim')) + (max(get(gca, 'xlim')) - min(get(gca, 'xlim')))*0.05;
    textYpos = max(get(gca, 'ylim')) - (max(get(gca, 'ylim')) - min(get(gca, 'ylim')))*0.05;
    T = text(textXpos, textYpos, str);
    set(T, 'fontsize', 12, 'verticalalignment', 'top', 'horizontalalignment', 'left');
end

% close;

%% FAM SIM: Ef - avg & indiv n sim (family A)
% famA=1-famB, sub=60, 'classic'
sub_idx = options.sim.fam.opts(1).n_subject; %60
fam_freq = unique(res.sim.fam.t.freq_family_B(ismember(res.sim.fam.t.n_subject, sub_idx)));

Ef_famA = [];
fam_freq_A = [];
for i = 1:numel(fam_freq)
    Ef_famA = [Ef_famA; res.sim.fam.t.Ef_famA(ismember(res.sim.fam.t.n_subject, sub_idx) & res.sim.fam.t.freq_family_B==fam_freq(i))];
    fam_freq_A = [fam_freq_A; (1-fam_freq(i))*ones(size(res.sim.fam.t.Ef_famA(ismember(res.sim.fam.t.n_subject, sub_idx) & res.sim.fam.t.freq_family_B==fam_freq(i))))];
    avgEf(i) = mean(res.sim.fam.t.Ef_famA(ismember(res.sim.fam.t.n_subject, sub_idx) & res.sim.fam.t.freq_family_B==fam_freq(i)));
    stdEf(i) = std(res.sim.fam.t.Ef_famA(ismember(res.sim.fam.t.n_subject, sub_idx) & res.sim.fam.t.freq_family_B==fam_freq(i)));
end
upper = avgEf + stdEf;
lower = avgEf - stdEf;

% avg Ef & std Ef
figure
hold on
plot(1-fam_freq, avgEf, 'LineWidth', 2, 'Color', options.col.tnub);
ax = axis;
fill([1-fam_freq', fliplr((1-fam_freq'))], [(upper), fliplr((lower))], ...
         options.col.tnub, 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
hold off
ylim([0 1])
xlabel('true frequency family A')
ylabel('expected frequency family A')
legend('avg Ef', 'std Ef', 'Location', 'SouthEast')

% close;

%% REC: plot param rec REFORMATTED
m = 1;
npars = length(res.main.ModSpace(m).prc_idx)+length(res.main.ModSpace(m).obs_idx);
nx = ceil(sqrt(npars));
ny = round(sqrt(npars));
figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 0.5 1]);
for j = 1:npars
    subplot(5, 2, j)
    if j > length(res.main.ModSpace(m).prc_idx) %% all obs param
        k = j-length(res.main.ModSpace(m).prc_idx);
        scatter(res.rec.param.obs(m).sim(:,k), res.rec.param.obs(m).est(:,k), 15, 'k', 'filled');
        pcc = res.rec.param.obs(m).pcc(k);
        hline = refline(1,0);
        hline.Color = 'k';
        if k == 1
            vstr = 'log(\zeta)';
        elseif k == size(res.main.ModSpace(m).obs_idx, 2)
            vstr = 'log(\Sigma)';
        elseif k == 2
            vstr = '\beta_0';
        elseif k == 3
            vstr = '\beta_1';
        elseif k == 4
            vstr = '\beta_2';
        elseif k == 5
            vstr = '\beta_3';
        elseif k == 6
            vstr = '\beta_4';
        end
    else
        scatter(res.rec.param.prc(m).sim(:,j), res.rec.param.prc(m).est(:,j), 15, 'k', 'filled');
        pcc = res.rec.param.prc(m).pcc(j);
        hline = refline(1,0);
        hline.Color = 'k';
        if j == 1
            vstr = '\omega_2';
        elseif j == 2
            vstr = '\omega_3';
        end
    end

    str1 = sprintf(', r = %1.2f', pcc);
    str = [vstr, str1];
    textXpos = min(get(gca, 'xlim')) + (max(get(gca, 'xlim')) - min(get(gca, 'xlim')))*0.05;
    textYpos = max(get(gca, 'ylim')) - (max(get(gca, 'ylim')) - min(get(gca, 'ylim')))*0.05;
    T = text(textXpos, textYpos, str);
    set(T, 'fontsize', 12, 'verticalalignment', 'top', 'horizontalalignment', 'left');    
end

%%% MANUALLY ADJUST text in subplot(5,2,8) !!!!

% save param rec fig
figdir = fullfile(saveDir, 'figures', 'ccn_2023',...
    ['fig2_param_rec_M1']);
print(figdir, '-dpng');
print(figdir, '-dsvg'); %deps, dpdf, ... (vector graphics)
close;

end
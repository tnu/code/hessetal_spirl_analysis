function [] = job_runner_8_sim_family_BMS(EULER, j)
% [] = job_runner_8_sim_family_BMS(EULER, j)
%
% Run family level BMS for 100 simulations of all different simulation
% settings in iteration j.
%
% INPUT
%   EULER        binary           Binary indicator variable
%   j            integer          Integer for iteration of 100 fam sim runs
%
%   OPTIONAL:
%
% OUTPUT    
%   argout       type
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

%% setup path
saveDir = spirl_setup_paths(EULER);

%% load analysis specifications
options = load(fullfile(saveDir, 'results', ['options']));
rng(123, 'twister');
options.rng.settings = rng;

%% load family power setup
fam = load(fullfile(saveDir, 'results', 'sim', 'family', ['family_power_setup']));

%% create res struct
res = struct();
res.sim.fam = fam;

%% run family-level BMS for all different options
dat.result = [];
for nSim = 1:options.sim.fam.opts(1).n_simulation
    for i = 1:numel(options.sim.fam.opts)
        % get fam sim options setting i
        opt = options.sim.fam.opts(i);
        % run BMS
        [~, out] = VBA_groupBMC(res.sim.fam.elem(i,j).F, options.fam.bmc_opt); % run family-level BMS
        % store
        result = repmat(opt,1,1);
        % classic family level comparison (Penny et al...)
        result(1).type = 'classic';
        result(1).Ef = out.families.Ef(2);
        result(1).xp = out.families.ep(2);
        result(1).bor = out.bor;
        result(1).Ef_famA = out.families.Ef(1);
        result(1).xp_famA = out.families.ep(1);
        % save results from all option elements in one struct
        dat.result = [dat.result; result]; 
        % print iteration info
        disp('done with iteration: ')
        disp(i)
    end
end

%% save family power results as struct
save_path = fullfile(saveDir, 'results', 'sim', 'family',...
    ['res_family_power_sim', num2str(j)]);
save(save_path, '-struct', 'dat');

disp('done with fam sim run: ')
disp(j)

end
function [logp, yhat, res] = m4_logrt_linear_binary(r, infStates, ptrans)
% [logp, yhat, res] = m4_logrt_linear_binary(r, infStates, ptrans)
%
% Calculates the log-probability of log-reaction times y (in units of
% log-ms) according to the linear log-RT model of M4 developed for the
% SPIRL study (Informational & environmental uncertainty model).
% (Designed to be compatible with the HGF Toolbox as part of TAPAS).
%
% INPUT
%   r             struct      Struct obtained from tapas_fitModel.m fct
%   infStates     tensor      Tensor containing inferred states from the
%                             perceptual model    
%   ptrans        vector      1xP vector with free param values (est space)
%
%   OPTIONAL:
%
% OUTPUT    
%   logp          vector       1xN vector containing trialwise log
%                              probabilities of logRTs
%   yhat          vector       1xN vector containing noise-free predictions
%   res           vector       1xN vector containing residuals
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

% Transform parameters to their native space
be0  = ptrans(2);
be1  = ptrans(3);
be2  = ptrans(4);
sa   = exp(ptrans(length(ptrans)));

% Initialize returned log-probabilities, predictions,
% and residuals as NaNs so that NaN is returned for all
% irregualar trials
n = size(infStates,1);
logp = NaN(n,1);
yhat = NaN(n,1);
res  = NaN(n,1);

% Weed irregular trials out from responses
y = r.y(:,1);
y(r.irr) = [];

% Extract trajectories of interest from infStates
sa2hat = infStates(:,2,2);
sa2    = infStates(:,2,4);

% Shifted Inferential variance (aka informational or estimation uncertainty, ambiguity)
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
inf = sa2;

% Shifted phasic & tonic volatility (aka environmental or unexpected uncertainty)
% exp(ka2*mu3hat(k)+om2) = sa2hat(k) - sa2(k-1) = exp(ka2*mu3(k-1)+om2)
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
inf_shifted = [0.1; sa2(1:(length(sa2)-1))]; % take sa2 of previous trial (sa2(0) = 0.1)
env = sa2hat - inf_shifted; % transform down to 1st level

% Calculate predicted log-reaction time
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
inf(r.irr) = [];
env(r.irr) = [];
logrt = be0 + be1.*inf + be2.*env;

% Calculate log-probabilities for non-irregular trials
% Note: 8*atan(1) == 2*pi (this is used to guard against
% errors resulting from having used pi as a variable).
reg = ~ismember(1:n,r.irr);
logp(reg) = -1/2.*log(8*atan(1).*sa) -(y-logrt).^2./(2.*sa);
yhat(reg) = logrt;
res(reg) = y-logrt;

return;
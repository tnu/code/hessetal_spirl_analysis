function [y, logrt] = m7_logrt_linear_binary_sim(r, infStates, p, y_bin_sim)
% [y, logrt] = m7_logrt_linear_binary_sim(r, infStates, p)
%
% Simulates logRTs with Gaussian noise.
% (Designed to be compatible with the HGF Toolbox as part of TAPAS).
%
% INPUT
%   r             struct      Struct obtained from tapas_simModel.m fct
%   infStates     tensor      Tensor containing inferred states from the
%                             perceptual model    
%   p             vector      1xP vector with free param values (nat space)
%
%   OPTIONAL:
%
% OUTPUT    
%   y             vector       Nx1 vector with simulated logRTs
%   logrt         vector       Nx1 vector containing noise-free predictions
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

% Get parameters
be0_corr  = p(2);
be1_incorr  = p(3);
sa = p(length(p));

% Number of trials
n = size(infStates,1);
trials_corr = zeros(n,1);

% Inputs
u = r.u(:,1);

% Simulated binary predictions
y_bin = y_bin_sim;

% correctness of responses
% ~~~~~~~~~~~~~~~~~~~~~~~~
for k = 1:(n-1)
    if y_bin(k) == u(k)
        trials_corr(k+1) = 1;
    end
end
trials_incorr = ~trials_corr;

% Calculate predicted log-reaction time
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
logrt(1:n,1) = be0_corr.*trials_corr + be1_incorr.*trials_incorr;

% Initialize random number generator
if isnan(r.c_sim.seed)
    rng('shuffle');
else
    rng(r.c_sim.seed);
end

% Simulate
y = logrt+sqrt(sa)*randn(n, 1);

end
function [] = job_runner_plot_results(EULER)
% [] = job_runner_plot_results(EULER)
%
% Creates figures for all steps of the entire analysis pipeline.
%
% INPUT
%   EULER        binary           Binary indicator variable
%
%   OPTIONAL:
%
% OUTPUT    
%   argout       type
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

%% setup path
saveDir = spirl_setup_paths(EULER);

%% load analysis specifications
options = load(fullfile(saveDir, 'results', ['options']));
rng(123, 'twister');
options.rng.settings = rng;

%% load final results
res = load(fullfile(saveDir, 'results', 'main', ['main_results']));

%% ________________________________________________________________________
% start producing figures...

%% MODEL SPACE: plot initial priors (prior pred dens)
spirl_init_priors(res.pilot, options, saveDir)

%% PILOT: figures from model-agnostic RT analysis
spirl_modelagnostic_rt_analysis(res.pilot.SPIRL, res.pilot.logRT_mat,...
    res.pilot.trials, res.pilot.traj, options, 'pilots', saveDir);

%% PILOT: plot stay behaviour (model-agnostic, binary)
[res.pilot.prev_corr, res.pilot.p_stay_avg, res.pilot.p_stay_var] = ...
    spirl_stay_behaviour(res.pilot.SPIRL, res.pilot.trials, options, 'pilots', saveDir);


%% PILOT: plot estimated priors
for m = 1:size(res.main.ModSpace,2)
    npars = length(res.main.ModSpace(m).prc_idx)+length(res.main.ModSpace(m).obs_idx);
    nx = ceil(sqrt(npars));
    ny = round(sqrt(npars));
    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    for j = 1:npars
        subplot(nx, ny, j)
        if j > length(res.main.ModSpace(m).prc_idx) % obs
            k = j - length(res.main.ModSpace(m).prc_idx);
            idx = res.main.ModSpace(m).obs_idx(k);
            if k == 1 %log(zeta)
                x_min = -3;
                x_max = 8;
            elseif k == size(res.pilot.priors.mod(m).obs_est, 2)
                x_min = res.pilot.ModSpace(m).obs_config.priormus(idx)...
                    -3*res.pilot.ModSpace(m).obs_config.priorsas(idx);
                x_max = res.pilot.ModSpace(m).obs_config.priormus(idx)...
                    +3*res.pilot.ModSpace(m).obs_config.priorsas(idx);
            else %betas
                x_min = -30;
                x_max = 30;
            end
            x = x_min:0.1:x_max;
            
            y = normpdf(x, res.main.ModSpace(m).obs_config.priormus(idx), sqrt(res.main.ModSpace(m).obs_config.priorsas(idx)));
            y_prior = normpdf(x, res.pilot.ModSpace(m).obs_config.priormus(idx), sqrt(res.pilot.ModSpace(m).obs_config.priorsas(idx)));
            
            plot(x, y, 'k')
            hold on
            plot(x, y_prior, 'k--')
            plot(res.pilot.priors.mod(m).obs_est(:,k), -0.05, 'ko')
            ylim([-0.1 1])
            str = sprintf('mu = %1.2f, Sa = %1.2f', res.main.ModSpace(m).obs_config.priormus(idx), res.main.ModSpace(m).obs_config.priorsas(idx));
            T = text(min(get(gca, 'xlim')), max(get(gca, 'ylim')), str);
            set(T, 'fontsize', 12, 'verticalalignment', 'top', 'horizontalalignment', 'left');
            if k == 1
                title('log(\zeta)')
            elseif k == size(res.pilot.priors.mod(m).obs_est, 2)
                title('log(\Sigma)')
            elseif k == 2
                title('\beta_0')
            elseif k == 3
                title('\beta_1')
            elseif k == 4
                title('\beta_2')
            elseif k == 5
                title('\beta_3')
            elseif k == 6
                title('\beta_4')
            end
        else %prc
            idx = res.main.ModSpace(m).prc_idx(j);
            x_min = res.pilot.ModSpace(m).prc_config.priormus(idx)...
                -3*res.pilot.ModSpace(m).prc_config.priorsas(idx);
            x_max = res.pilot.ModSpace(m).prc_config.priormus(idx)...
                +3*res.pilot.ModSpace(m).prc_config.priorsas(idx);
            x = x_min:0.1:x_max;
            
            y = normpdf(x, res.main.ModSpace(m).prc_config.priormus(idx), sqrt(res.main.ModSpace(m).prc_config.priorsas(idx)));
            y_prior = normpdf(x, res.pilot.ModSpace(m).prc_config.priormus(idx), sqrt(res.pilot.ModSpace(m).prc_config.priorsas(idx)));
            
            plot(x, y, 'k')
            hold on
            plot(x, y_prior, 'k--')
            plot(res.pilot.priors.mod(m).prc_est(:,j), -0.05, 'ko')
            ylim([-0.1 1])
            str = sprintf('mu = %1.2f, Sa = %1.2f', res.main.ModSpace(m).prc_config.priormus(idx), res.main.ModSpace(m).prc_config.priorsas(idx));
            T = text(min(get(gca, 'xlim')), max(get(gca, 'ylim')), str);
            set(T, 'fontsize', 12, 'verticalalignment', 'top', 'horizontalalignment', 'left');
            if j == 1
                title('\omega_2')
            elseif j == 2
                title('\omega_3')
            end
        end
        
    end
    legend('pilot prior', 'initial prior', 'MAP estimates', 'Position', [0.94 0.48 0.03 0.07])
    figdir = fullfile(saveDir, 'figures', 'pilots', 'priors',...
        ['Priors_model', num2str(m)]);
    print(figdir, '-dtiff');
    close;
end

%% PILOT: plot estimated priors (seperate fig for each param)
for m = 1:size(res.main.ModSpace, 2)
    for j = 1:size(res.main.ModSpace(m).prc_idx,2)
        spirl_plot_pilot_prior_pdf(res.pilot.priors.mod(m), j, 'prc')
        figdir = fullfile(saveDir, 'figures', 'pilots', 'priors', 'param_indiv',...
            ['Priors_model', num2str(m), '_prcparam', num2str(j)]);
        print(figdir, '-dtiff');
        close;        
    end
    for k = 1:size(res.main.ModSpace(m).obs_idx,2)
        spirl_plot_pilot_prior_pdf(res.pilot.priors.mod(m), k, 'obs')
        figdir = fullfile(saveDir, 'figures', 'pilots', 'priors', 'param_indiv',...
            ['Priors_model', num2str(m), '_obsparam', num2str(k)]);
        print(figdir, '-dtiff');
        close;
    end
end

%% PILOT: plot plot binary (hgf) & continuous rt trajectories together
for m = 1:size(res.main.ModSpace,2)
    for sub = 1:options.pilots.nS
        spirl_tapas_ehgf_binary_combObs_plotTraj(res.pilot.est(m,sub))
        figdir = fullfile(saveDir, 'figures', 'pilots', 'fits', 'comb',...
            ['binary_model', num2str(m), '_sub', num2str(sub)]);
        print(figdir, '-dtiff');
        close;
    end    
end

%% PILOT: plot AVG rt trajectories & fits
mean_logRT = mean(res.pilot.logRT_mat, 2, 'omitnan');
var_logRT = var(res.pilot.logRT_mat, 0, 2, 'omitnan');
logupper = mean_logRT + sqrt(var_logRT);
loglower = mean_logRT - sqrt(var_logRT);
figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
for m = 1:size(res.main.ModSpace,2)
    for n = 1:options.pilots.nS
        yhat_mat(:,n) = res.pilot.est(m,n).optim.yhat(:,2);
    end
    mod(m).mean_yhat = mean(yhat_mat, 2, 'omitnan');
    mod(m).var_yhat = var(yhat_mat, 0, 2, 'omitnan');
    mod(m).upper_yhat = mod(m).mean_yhat + sqrt(mod(m).var_yhat);
    mod(m).lower_yhat = mod(m).mean_yhat - sqrt(mod(m).var_yhat);
    
    subplot(4,2,m)
    plot(res.pilot.trials, mean_logRT, 'LineWidth', 2, 'color', options.col.tnub)
    hold on
    fill([res.pilot.trials, fliplr(res.pilot.trials)], [(logupper)', fliplr((loglower)')], ...
             'b', 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
    plot(res.pilot.trials, mod(m).mean_yhat, 'LineWidth', 2)
    fill([res.pilot.trials, fliplr(res.pilot.trials)], [(mod(m).upper_yhat)', fliplr((mod(m).lower_yhat)')], ...
             'r', 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
    xlim([1 160])
    ylabel('logRT [ms]')
    xlabel('trials')
    txt = ['model ' num2str(m)];
    title(txt)
end
legend('$log(y_{rt})$', '$Var(log(y_{rt}))$', '$log(\hat{y}_{rt})$', '$Var(log(\hat{y}_{rt}))$', 'Interpreter','latex', 'Position', [0.94 0.48 0.03 0.07])
figdir = fullfile(saveDir, 'figures', 'pilots', 'fits', 'logRT',...
    ['avg_logRT']);
print(figdir, '-dtiff');
close;

%% PILOT: plot AVG rt trajectories, fits & regressors
for m = 1:size(res.main.ModSpace,2)
    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0.3 0 0.45 1]);
    subplot(2,1,1)
    plot(res.pilot.trials, mean_logRT, 'LineWidth', 2, 'color', options.col.tnub)
    hold on
    fill([res.pilot.trials, fliplr(res.pilot.trials)], [(logupper)', fliplr((loglower)')], ...
             'b', 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
    plot(res.pilot.trials, mod(m).mean_yhat, 'LineWidth', 2)
    fill([res.pilot.trials, fliplr(res.pilot.trials)], [(mod(m).upper_yhat)', fliplr((mod(m).lower_yhat)')], ...
             'r', 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
    legend('$log(y_{rt})$', '$Var(log(y_{rt}))$', '$log(\hat{y}_{rt})$', '$Var(log(\hat{y}_{rt}))$', 'Interpreter','latex')
    xlim([1 160])
    ylabel('logRT [ms]')
    title('avg log rt fits')
    
    % AVG regressors
    subplot(2,1,2)
    hold on
    if m == 1
        for n = 1:options.pilots.nS
            m1hreg = res.pilot.est(m,n).traj.muhat(:,1);
            poo = m1hreg.^res.pilot.est(m,n).u.*(1-m1hreg).^(1-res.pilot.est(m,n).u); % probability of observed outcome
            surp = -log2(poo);
            surp_shifted_mat(:,n) = [1; surp(1:(length(surp)-1))];
            sh1_mat(:,n) = res.pilot.est(m,n).traj.sahat(:,1);
            sh2_mat(:,n) = res.pilot.est(m,n).traj.sahat(:,2);
            mh3_mat(:,n) = res.pilot.est(m,n).traj.muhat(:,3);            
        end
        plot(res.pilot.trials, mean(surp_shifted_mat, 2, 'omitnan'), 'Color', [0 0.1 0.6]);
        plot(res.pilot.trials, mean(sh1_mat, 2, 'omitnan'), 'Color', [0 0.5 0])
        plot(res.pilot.trials, mean(sh2_mat, 2, 'omitnan'), 'Color', [0.5 0.5 0])
        plot(res.pilot.trials, mean(exp(mh3_mat), 2, 'omitnan'), 'Color', [0.5 0 0])
        legend({'$S_{shifted}$', '$\hat{\sigma}_{1}$', '$\hat{\sigma}_{2}$', '$exp(\hat{\mu}_{3})$'}, 'Interpreter', 'Latex')
        axis('tight')
    elseif m == 2
        for n = 1:options.pilots.nS
            sa2_mat(:,n) = res.pilot.est(m,n).traj.sa(:,2);
            mh3_mat(:,n) = res.pilot.est(m,n).traj.muhat(:,3);
        end
        plot(res.pilot.trials, mean(sa2_mat, 2, 'omitnan'), 'Color', [0 0.5 0])
        plot(res.pilot.trials, mean(mh3_mat, 2, 'omitnan'), 'Color', [0 1 0])
        legend({'$\sigma_{2}$', '$\hat{\mu}_{3}$'}, 'Interpreter', 'Latex')
        axis('tight')
    elseif m == 3
        for n = 1:options.pilots.nS
            sh1_mat(:,n) = res.pilot.est(m,n).traj.sahat(:,1);
            inf_shift_mat(:,n) = [0.1; res.pilot.est(m,n).traj.sa(1:(length(res.pilot.trials)-1),2)];
            env_mat(:,n) = res.pilot.est(m,n).traj.sahat(:,2) - inf_shift_mat(:,n);
        end
        plot(res.pilot.trials, mean(sh1_mat, 2, 'omitnan'), 'Color', [0 0.5 0])
        plot(res.pilot.trials, mean(inf_shift_mat, 2, 'omitnan'), 'Color', [0.5 0.5 0])
        plot(res.pilot.trials, mean(env_mat, 2, 'omitnan'), 'Color', [0.5 0 0])
        legend({'$\hat{\sigma}_{1}$', '${\sigma}_{2}$', '$exp(\kappa_{2}\hat{\mu}_{3}+\omega_{2})$'}, 'Interpreter', 'Latex')
    elseif m == 4
        for n = 1:options.pilots.nS
            inf_shift_mat(:,n) = [0.1; res.pilot.est(m,n).traj.sa(1:(length(res.pilot.trials)-1),2)];
            env_mat(:,n) = res.pilot.est(m,n).traj.sahat(:,2) - inf_shift_mat(:,n);
        end
        plot(res.pilot.trials, mean(inf_shift_mat, 2, 'omitnan'), 'Color', [0.5 0.5 0])
        plot(res.pilot.trials, mean(env_mat, 2, 'omitnan'), 'Color', [0.5 0 0])
        legend({'${\sigma}_{2}$', '$exp(\kappa_{2}\hat{\mu}_{3}+\omega_{2})$'}, 'Interpreter', 'Latex')
    elseif m == 5
        for n = 1:options.pilots.nS
            be0(:,n) = res.pilot.est(m,n).p_obs.p(2);
        end
        plot(res.pilot.trials, ones(size(res.pilot.trials))*mean(be0, 2, 'omitnan'), 'Color', [0 0.5 0])
        legend({'$\beta_{0}$'}, 'Interpreter', 'Latex')
    elseif m == 6
        for n = 1:options.pilots.nS
            be0(:,n) = res.pilot.est(m,n).p_obs.p(2);
            be1(:,n) = res.pilot.est(m,n).p_obs.p(3);
        end
        lin_decay = mean(be1, 2, 'omitnan').*res.pilot.trials./length(res.pilot.trials);
        plot(res.pilot.trials, mean(be0, 2, 'omitnan')+lin_decay, 'Color', [0 0.5 0])
        ylim([5.2 6.8])
        legend({'$k$'}, 'Interpreter', 'Latex')
    elseif m == 7
        for n = 1:options.pilots.nS
            be0_corr(:,n) = res.pilot.est(m,n).p_obs.p(2);
            be0_incorr(:,n) = res.pilot.est(m,n).p_obs.p(3);
        end
        plot(res.pilot.trials, ones(size(res.pilot.trials))*mean(be0_corr, 2, 'omitnan'), 'Color', [0 0.5 0])
        plot(res.pilot.trials, ones(size(res.pilot.trials))*mean(be0_incorr, 2, 'omitnan'), 'Color', [0.5 0.5 0])
        ylim([5.2 6.8])
        legend({'$\beta_{0,correct}$', '${\beta}_{0,incorrect}$'}, 'Interpreter', 'Latex')        
    end
    ylabel('logRT [ms]')
    xlabel('trials')
    txt = ['regressors model ' num2str(m)];
    title(txt)
    
    figdir = fullfile(saveDir, 'figures', 'pilots', 'fits', 'logRT',...
        ['avg_logRT_regressors_mod', num2str(m)]);
    print(figdir, '-dtiff');
    close;
end

%% PILOT: plot rt trajectories & fits (single-sub)
for m = 1:size(res.main.ModSpace,2)
    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    for sub = 1:options.pilots.nS
        subplot(4,5,sub)
        plot(res.pilot.est(m,sub).y(:,2), 'color', options.col.tnub)
        hold on
        plot(res.pilot.est(m,sub).optim.yhat(:,2), 'r')
        xlim([0 160])
        ylim([2 10])
        ylabel('logRT [ms]')
        xlabel('trials')
        txt = ['pilot sub ' num2str(sub)];
        title(txt)
    end
    legend('$log(y_{rt})$', '$log(\hat{y}_{rt})$', 'Interpreter','latex', 'Position', [0.94 0.48 0.03 0.07])
    figdir = fullfile(saveDir, 'figures', 'pilots', 'fits', 'logRT',...
        ['logRT_model', num2str(m)]);
    print(figdir, '-dtiff');
    close;
end

%% PILOT: plot rt fits + regressors (single-sub)
for m = 1:size(res.main.ModSpace,2)
    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    for n = 1:options.pilots.nS
        subplot(4,5,n)
        plot(res.pilot.est(m,n).y(:,2), 'color', options.col.tnub)
        hold on
        plot(res.pilot.est(m,n).optim.yhat(:,2), 'r')
        if m == 1
            m1hreg = res.pilot.est(m,n).traj.muhat(:,1);
            poo = m1hreg.^res.pilot.est(m,n).u.*(1-m1hreg).^(1-res.pilot.est(m,n).u); % probability of observed outcome
            surp = -log2(poo);
            surp_shifted = [1; surp(1:(length(surp)-1))];
            plot(res.pilot.trials, surp_shifted, 'Color', [0 0.1 0.6]);
            plot(res.pilot.trials, res.pilot.est(m,n).traj.sahat(:,1), 'Color', [0 0.5 0])
            plot(res.pilot.trials, res.pilot.est(m,n).traj.sahat(:,2), 'Color', [0.5 0.5 0])
            plot(res.pilot.trials, res.pilot.est(m,n).traj.muhat(:,3), 'Color', [0.5 0 0])
        elseif m == 2
            plot(res.pilot.trials, res.pilot.est(m,n).traj.sa(:,2), 'Color', [0 0.5 0])
            plot(res.pilot.trials, res.pilot.est(m,n).traj.muhat(:,3), 'Color', [0 1 0])
        elseif m == 3
            inf_shift = [0.1; res.pilot.est(m,n).traj.sa(1:(length(res.pilot.trials)-1),2)];
            env = res.pilot.est(m,n).traj.sahat(:,2) - inf_shift;
            plot(res.pilot.trials, res.pilot.est(m,n).traj.sahat(:,1), 'Color', [0 0.5 0])
            plot(res.pilot.trials, inf_shift, 'Color', [0.5 0.5 0])
            plot(res.pilot.trials, env, 'Color', [0.5 0 0])
        elseif m == 4
            inf_shift = [0.1; res.pilot.est(m,n).traj.sa(1:(length(res.pilot.trials)-1),2)];
            env = res.pilot.est(m,n).traj.sahat(:,2) - inf_shift;
            plot(res.pilot.trials, inf_shift, 'Color', [0.5 0.5 0])
            plot(res.pilot.trials, env, 'Color', [0.5 0 0])
        elseif m == 5
            be0 = res.pilot.est(m,n).p_obs.p(2);
            plot(res.pilot.trials, ones(size(res.pilot.trials))*be0, 'Color', [0 0.5 0])
        elseif m == 6
            be0 = res.pilot.est(m,n).p_obs.p(2);
            lin_decay = res.pilot.est(m,n).p_obs.p(3).*res.pilot.trials./length(res.pilot.trials);
            plot(res.pilot.trials, be0+lin_decay, 'Color', [0 0.5 0])
        elseif m == 7
            be0_corr = res.pilot.est(m,n).p_obs.p(2);
            be0_incorr = res.pilot.est(m,n).p_obs.p(3);
            plot(res.pilot.trials, ones(size(res.pilot.trials))*be0_corr, 'Color', [0 0.5 0])
            plot(res.pilot.trials, ones(size(res.pilot.trials))*be0_incorr, 'Color', [0.5 0.5 0])
        end
        axis('tight')
        ylabel('logRT [ms]')
        xlabel('trials')
        txt = ['pilot sub ' num2str(n)];
        title(txt)
    end
    if m == 1
        legend({'$log(y_{rt})$', '$log(\hat{y}_{rt})$', '$S_{shifted}$', '$\hat{\sigma}_{1}$', '$\hat{\sigma}_{2}$', '$exp(\hat{\mu}_{3})$'}, 'Interpreter','latex', 'Position', [0.94 0.48 0.03 0.07])
    elseif m == 2
        legend({'$log(y_{rt})$', '$log(\hat{y}_{rt})$', '$\sigma_{2}$', '$\hat{\mu}_{3}$'}, 'Interpreter','latex', 'Position', [0.94 0.48 0.03 0.07])
    elseif m == 3
        legend({'$log(y_{rt})$', '$log(\hat{y}_{rt})$', '$\hat{\sigma}_{1}$', '${\sigma}_{2}$', '$exp(\kappa_{2}\hat{\mu}_{3}+\omega_{2})$'}, 'Interpreter','latex', 'Position', [0.94 0.48 0.03 0.07])
    elseif m == 4
        legend({'$log(y_{rt})$', '$log(\hat{y}_{rt})$', '${\sigma}_{2}$', '$exp(\kappa_{2}\hat{\mu}_{3}+\omega_{2})$'}, 'Interpreter','latex', 'Position', [0.94 0.48 0.03 0.07])
    elseif m == 5
        legend({'$log(y_{rt})$', '$log(\hat{y}_{rt})$', '$\beta_{0}$'}, 'Interpreter','latex', 'Position', [0.94 0.48 0.03 0.07])
    elseif m == 6
        legend({'$log(y_{rt})$', '$log(\hat{y}_{rt})$', '$k$'}, 'Interpreter','latex', 'Position', [0.94 0.48 0.03 0.07])
    elseif m == 7
        legend({'$log(y_{rt})$', '$log(\hat{y}_{rt})$', '$\beta_{0,correct}$', '${\beta}_{0,incorrect}$'}, 'Interpreter','latex', 'Position', [0.94 0.48 0.03 0.07])
    end
    figdir = fullfile(saveDir, 'figures', 'pilots', 'fits', 'logRT',...
        ['logRT_regressors_model', num2str(m)]);
    print(figdir, '-dtiff');
    close;
end

%% PILOT: plot convergence of opt algo
n_max = 10;
figs = ceil(options.pilots.nS/n_max);
for m = 1:size(res.main.ModSpace,2)
    % obj fct
    for f = 1:figs
        figure
        set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
        for n_idx = 1:n_max
            n_sub = n_idx + n_max*(f-1);
            if n_sub <= options.pilots.nS
                subplot(ceil(n_max/2), 2, n_idx)
                plot(0, res.pilot.est(m,n_sub).optim.iter.val(1), 'o',...
                    'color', options.col.tnub) %init value
                hold on
                plot([0:options.opt_config.maxIter], res.pilot.est(m,n_sub).optim.iter.val,...
                    'color', options.col.tnub)
                for i = 1:size(res.pilot.est(m,n_sub).optim.iter.rst, 2)
                    xline(res.pilot.est(m,n_sub).optim.iter.rst(i), '--')
                end
                xlim([0 options.opt_config.maxIter])
                xlabel('opt algo iterations')
                ylabel('nlj')
                txt = ['pilot sub ' num2str(n_sub)];
                title(txt)
            end
            n_low = n_sub-(n_max-1);
            if n_sub > options.pilots.nS
                n_sub = options.pilots.nS;
            end
        end
        figdir = fullfile(saveDir, 'figures', 'pilots',...
            'fits', 'opt_algo',...
            ['nlj_optim_mod', num2str(m), '_sub', num2str(n_low), '-', num2str(n_sub)]);
        print(figdir, '-dtiff');
        close;
    end
    
    % params
    for f = 1:figs
        figure
        set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
        for n_idx = 1:n_max
            n_sub = n_idx + n_max*(f-1);
            if n_sub <= options.pilots.nS
                subplot(ceil(n_max/2), 2, n_idx)
                plot(zeros(size(res.pilot.est(m,n_sub).optim.iter.x(1,:))),...
                    res.pilot.est(m,n_sub).optim.iter.x(1,:), 'o')%,...
                hold on
                plot(repmat([0:options.opt_config.maxIter]', 1, size(res.pilot.est(m,n_sub).optim.iter.x,2)),...
                    res.pilot.est(m,n_sub).optim.iter.x)%,...
                for i = 1:size(res.pilot.est(m,n_sub).optim.iter.rst, 2)
                    xline(res.pilot.est(m,n_sub).optim.iter.rst(i), '--')
                end
                xlim([0 options.opt_config.maxIter])
                xlabel('opt algo iterations')
                ylabel('parameter values')
                txt = ['pilot sub ' num2str(n_sub)];
                title(txt)
            end
            n_low = n_sub-(n_max-1);
            if n_sub > options.pilots.nS
                n_sub = options.pilots.nS;
            end
        end
        figdir = fullfile(saveDir, 'figures', 'pilots',...
            'fits', 'opt_algo',...
            ['param_optim_mod', num2str(m), '_sub', num2str(n_low), '-', num2str(n_sub)]);
        print(figdir, '-dtiff');
        close;
    end
end

%% PILOT: plot LogLl decomposed
mod = struct;
for m = 1:size(res.main.ModSpace,2)
    mod(m).LogLlsplit = NaN(options.pilots.nS,2);
    for sub = 1:options.pilots.nS
        mod(m).LogLlsplit(sub,:) = sum(res.pilot.est(m,sub).optim.trialLogLlsplit, 'omitnan');
    end
    figure('Name', 'logLl contributions');
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    bar(mod(m).LogLlsplit)
    ylabel('log likelihood')
    xlabel('pilot subjects')
    legend({'bin. logLl', 'cont. logLl'}, 'Location', 'Southeast')
    ylim([-200 200])
    figdir = fullfile(saveDir, 'figures', 'pilots', 'fits',...
        ['LogLl_split_mod', num2str(m)]);
    print(figdir, '-dtiff');
    close;
end

%% PILOT: plot LME decomposed
for m = 1:size(res.main.ModSpace,2)
    mod(m).LME_decomp = NaN(options.pilots.nS,5);
    mod(m).LME_decomp(:,1:2) = mod(m).LogLlsplit;
    for sub = 1:options.pilots.nS
        mod(m).LME_decomp(sub,3) = res.pilot.est(m,sub).optim.decompLME.logjoint + res.pilot.est(m,sub).optim.negLl;
        mod(m).LME_decomp(sub,4) = res.pilot.est(m,sub).optim.decompLME.postpredcorr;
        mod(m).LME_decomp(sub,5) = res.pilot.est(m,sub).optim.decompLME.freepars;
    end
    figure('Name', 'logLl contributions');
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    bar(mod(m).LME_decomp)
    xlabel('pilot subjects')
    legend({'logLl bin', 'logLl cont', 'log Prior', 'post. corr.', 'free pars'},...
        'Position', [0.94 0.48 0.03 0.07])
    ylim([-200 200])
    figdir = fullfile(saveDir, 'figures', 'pilots', 'fits',...
        ['decompLME_mod', num2str(m)]);
    print(figdir, '-dtiff');
    close;
end

%% PILOT: plot AVG posterior param correlations
for m = 1:size(res.main.ModSpace,2)
    post_param_corr = zeros(size(res.pilot.est(m,1).optim.Corr));
    avg_c = res.pilot.est(m,1);
    for sub = 1:options.pilots.nS
        post_param_corr = post_param_corr + res.pilot.est(m,sub).optim.Corr;
    end
    avg_c.optim.Corr = post_param_corr./options.pilots.nS;
    tapas_fit_plotCorr(avg_c)
    % axis labels
    ax = gca;
    ax.XTick = 1:(length(res.main.ModSpace(m).prc_idx)+length(res.main.ModSpace(m).obs_idx));
    ax.YTick = 1:(length(res.main.ModSpace(m).prc_idx)+length(res.main.ModSpace(m).obs_idx));
    if size(ax.XTick,2) == 5
        ax.XTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\Sigma'};
        ax.YTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\Sigma'};
    elseif size(ax.XTick,2) == 6
        ax.XTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\beta_1', '\Sigma'};
        ax.YTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\beta_1', '\Sigma'};
    elseif size(ax.XTick,2) == 7
        ax.XTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\beta_1', '\beta_2', '\Sigma'};
        ax.YTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\beta_1', '\beta_2', '\Sigma'};
    elseif size(ax.XTick,2) == 8
        ax.XTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\beta_1', '\beta_2', '\beta_3', '\Sigma'};
        ax.YTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\beta_1', '\beta_2', '\beta_3', '\Sigma'};
    elseif size(ax.XTick,2) == 9
        ax.XTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\beta_1', '\beta_2', '\beta_3', '\beta_4', '\Sigma'};
        ax.YTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\beta_1', '\beta_2', '\beta_3', '\beta_4', '\Sigma'};
    end
    figdir = fullfile(saveDir, 'figures', 'pilots', 'fits',...
        ['post_param_corr_AVG_mod', num2str(m)]);
    print(figdir, '-dtiff');
    close;
end

%% SIM indiv: plot logRT predictions (noise-free)
figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
for m = 1:size(res.main.ModSpace,2)
    subplot(4,2,m)
    plot(res.sim.sub(1,m).data.yhat(:,2), 'color', options.col.tnub) %%SNR
    hold on
    for n = 2:100
        plot(res.sim.sub(n,m).data.yhat(:,2), 'color', options.col.tnub) %%SNR
    end
    plot(1:160, ones(160,1)*log(1700), '--k')
    plot(1:160, ones(160,1)*log(100), '--k')
    ylim([0 12])
    ylabel('log($\hat{y}_{rt,sim}$)', 'Interpreter', 'Latex')
    xlabel('trials')
    txt = ['model ' num2str(m)];
    title(txt)
end
figdir = fullfile(saveDir, 'figures', 'sim', 'indiv', 'traj',...
    ['logRT_predictions']);
print(figdir, '-dtiff');
close;

%% SIM indiv: plot logRT trajectories
figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
for m = 1:size(res.main.ModSpace,2)
    subplot(4,2,m)
    plot(res.sim.sub(1,m).data.y(:,2), 'color', options.col.tnub)
    hold on
    for n = 2:100
        plot(res.sim.sub(n,m).data.y(:,2), 'color', options.col.tnub)
    end
    plot(1:160, ones(160,1)*log(1700), '--k')
    plot(1:160, ones(160,1)*log(100), '--k')
    ylim([0 12])
    ylabel('log($y_{rt,sim}$)', 'Interpreter', 'Latex')
    xlabel('trials')
    txt = ['model ' num2str(m)];
    title(txt)
end
figdir = fullfile(saveDir, 'figures', 'sim', 'indiv', 'traj',...
    ['logRT_trajectories']);
print(figdir, '-dtiff');
close;

%% SIM indiv: plot binary predictions
figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
for m = 1:size(res.main.ModSpace,2)
    subplot(4,2,m)
    plot(res.sim.sub(1,m).data.traj.muhat(:,1), 'color', options.col.tnub)
    hold on
    for n = 2:100
        plot(res.sim.sub(n,m).data.traj.muhat(:,1), 'color', options.col.tnub)
    end
    ylabel('$\hat{\mu}_{1}$', 'Interpreter', 'Latex')
    xlabel('trials')
    txt = ['model ' num2str(m)];
    title(txt)
end
figdir = fullfile(saveDir, 'figures', 'sim', 'indiv', 'traj',...
    ['binary_predictions']);
print(figdir, '-dtiff');
close;

%% SIM indiv: plot ehgf prior pred dens (empirical priors)
for m = 1:size(res.main.ModSpace,2)
    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    for j = 1:3
        subplot(3,2,7-2*j)
        plot(res.sim.sub(1,m).data.traj.muhat(:,j), 'color', options.col.tnub)
        hold on
        for n = 2:100
            plot(res.sim.sub(n,m).data.traj.muhat(:,j), 'color', options.col.tnub)
        end
        xlabel('trials')
        ytxt = ['$\hat{\mu}_{', num2str(j), '}$'];
        ylabel(ytxt, 'Interpreter', 'Latex')
        txt = ['muhat_' num2str(j)];
        title(txt)
    end
    for k = 1:3
        subplot(3,2,8-2*k)
        plot(res.sim.sub(1,m).data.traj.sahat(:,k), 'color', options.col.tnub)
        hold on
        for n = 2:100
            plot(res.sim.sub(n,m).data.traj.sahat(:,k), 'color', options.col.tnub)
        end
        xlabel('trials')
        ytxt = ['$\hat{\sigma}_{', num2str(k), '}$'];
        ylabel(ytxt, 'Interpreter', 'Latex')
        txt = ['sahat_' num2str(k)];
        title(txt)
    end
    figdir = fullfile(saveDir, 'figures', 'sim', 'indiv', 'traj',...
        ['ehgf_bin_prior_pred_dens_mod', num2str(m)]);
    print(figdir, '-dtiff');
    close;
end

%% SIM (prior pred check): plot #switches (prior pred distr)
for m = 1:size(res.main.ModSpace,2)
    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    
    % main
    subplot(2,1,1)
    % pilot prior samples
    scatter(zeros(size(res.sim.sub(1,m).n_switch_all)),res.sim.sub(1,m).n_switch_all,...
        '.', 'CData', options.col.grn)
    hold on
    for n = 1:length(res.main.ppc.ppc_sub)
        % posterior samples
        scatter(n*ones(size(res.main.ppc.mod(m).sub(n).n_switch_all)),res.main.ppc.mod(m).sub(n).n_switch_all,...
            '.', 'CData', options.col.tnub)
        % empirical data
        ax = scatter(n,res.main.SPIRL(res.main.ppc.ppc_sub(n)).n_switch, 'x',...
            'LineWidth', 2, 'CData', options.col.tnuy);
    end
    xlim([-1 length(res.main.ppc.ppc_sub)+1])
    ax.Parent.XTick = 0:length(res.main.ppc.ppc_sub);
    ax.Parent.XTickLabel = {'pilot prior', res.main.ppc.ppc_sub};
    ylim([0 max(res.main.trials)])
    ylabel('# switches (binary predictions)')
    xlabel('main subject')
    legend({'$\hat{y}_{bin,PriorPred}$', '$\hat{y}_{bin,PosteriorPred}$', '$y_{bin}$'}, 'Interpreter', 'Latex')
    title(['main data set - model ', num2str(m)])
    
    % pilots
    subplot(2,1,2)
    hold on
    for n = 1:length(res.main.ppc.ppc_sub_pilot)
        % empirical data
        ax = scatter(n,res.pilot.SPIRL(res.main.ppc.ppc_sub_pilot(n)).n_switch, 'x',...
            'LineWidth', 2, 'CData', options.col.tnuy);
    end
    xlim([-1 length(res.main.ppc.ppc_sub_pilot)+1])
    ax.Parent.XTick = 0:length(res.main.ppc.ppc_sub);
    ax.Parent.XTickLabel = {'init prior', res.main.ppc.ppc_sub};
    ylim([0 max(res.pilot.trials)])
    ylabel('# switches (binary predictions)')
    xlabel('pilot subject')
    legend({'$\hat{y}_{bin,PriorPred}$', '$y_{bin}$'}, 'Interpreter', 'Latex')
    title(['pilot data set - model ', num2str(m)])

    figdir = fullfile(saveDir, 'figures', 'main', 'ppc', 'bin',...
        ['MAIN_PILOTS_number_of_switches_', num2str(length(res.main.ppc.ppc_sub)), 'sub_',...
        num2str(options.main.ppc.nDraws_per_sub), 'draws_mod', num2str(m)]);
    print(figdir, '-dtiff');
    close;
end

%% REC: plot param rec
for m = 1:size(res.main.ModSpace,2)
    npars = length(res.main.ModSpace(m).prc_idx)+length(res.main.ModSpace(m).obs_idx);
    nx = ceil(sqrt(npars));
    ny = round(sqrt(npars));
    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    for j = 1:npars
        subplot(nx, ny, j)
        if j > length(res.main.ModSpace(m).prc_idx) %% all obs param
            k = j-length(res.main.ModSpace(m).prc_idx);
            scatter(res.rec.param.obs(m).sim(:,k), res.rec.param.obs(m).est(:,k), 15, 'k', 'filled');
            pcc = res.rec.param.obs(m).pcc(k);
            hline = refline(1,0);
            hline.Color = 'k';
            if k == 1
                title('log(\zeta)')
            elseif k == size(res.main.ModSpace(m).obs_idx, 2)
                title('log(\Sigma)')
            elseif k == 2
                title('\beta_0')
            elseif k == 3
                title('\beta_1')
            elseif k == 4
                title('\beta_2')
            elseif k == 5
                title('\beta_3')
            elseif k == 6
                title('\beta_4')
            end
        else
            scatter(res.rec.param.prc(m).sim(:,j), res.rec.param.prc(m).est(:,j), 15, 'k', 'filled');
            pcc = res.rec.param.prc(m).pcc(j);
            hline = refline(1,0);
            hline.Color = 'k';
            if j == 1
                title('\omega_2')
            elseif j == 2
                title('\omega_3')
            end
        end        
        xlabel('Simulated values');
        ylabel('Recovered values');
        str = sprintf('r = %1.2f', pcc);
        textXpos = min(get(gca, 'xlim')) + (max(get(gca, 'xlim')) - min(get(gca, 'xlim')))*0.05;
        textYpos = max(get(gca, 'ylim')) - (max(get(gca, 'ylim')) - min(get(gca, 'ylim')))*0.05;
        T = text(textXpos, textYpos, str);
        set(T, 'fontsize', 12, 'verticalalignment', 'top', 'horizontalalignment', 'left');
    end
    figdir = fullfile(saveDir, 'figures', 'sim', 'indiv', 'rec',...
        ['param_rec_m', num2str(m)]);
        print(figdir, '-dtiff');
        close;
end

%% REC: plot model ident (Classification)
% title
t2 = sprintf('(chance threshold (0.90-CI) = %.2f)', res.rec.class.chancethr);
t3 = sprintf('Balanced Acc = %.2f', res.rec.class.balacc);
comp_tit = {'Classification'; t2; t3};
% axis labels
mod_names = {};
mod_nr = {};
for m = 1:size(res.main.ModSpace,2)
    mod_names{m} = res.main.ModSpace(m).name;
    mod_nr{m} = ['m', num2str(m)];
end

figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
bwr = @(n)interp1([1 2], [0.9 0.9 0.9; 0.3 0.5 1], linspace(1, 2, n), 'linear');
imagesc(res.rec.class.percLMEwinner)
colormap(bwr(64));
colorbar;
set(gca, 'clim', [0 1])
set(gca, 'Fontsize', 14);
title(comp_tit)
xlabel('Recovered');
ylabel('Simulated');
ax = gca;
ax.XTick = [1:size(res.main.ModSpace,2)];
ax.XTickLabel = mod_nr;
ax.YTick = [1:size(res.main.ModSpace,2)];
ax.YTickLabel = mod_names;

pos=get(gca,'position');
[rows,cols]=size(res.rec.class.percLMEwinner);
nums = flip(res.rec.class.percLMEwinner',2);
width=pos(3)/(cols);
height=pos(4)/(rows);
for i=1:cols
      for j=1:rows               
        annotation('textbox',[pos(1)+width*(i-1),pos(2)+height*(j-1),width,height], ...
        'string',num2str(nums(i,j), '%.2f'),'LineStyle','none','HorizontalAlignment','center',...
        'VerticalAlignment','middle');
      end
end

figdir = fullfile(saveDir, 'figures', 'sim', 'indiv', 'rec',...
    ['model_ident_classification']);
print(figdir, '-dtiff');
close;

%% REC: plot model ident (rfx bms, PXP)
% title
comp_tit = {'rfx bms pxp'};
% axis labels
mod_names = {};
mod_nr = {};
for m = 1:size(res.main.ModSpace,2)
    mod_names{m} = res.main.ModSpace(m).name;
    mod_nr{m} = ['m', num2str(m)];
end

figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
bwr = @(n)interp1([1 2], [0.9 0.9 0.9; 0.3 0.5 1], linspace(1, 2, n), 'linear');
imagesc(res.rec.bmc.rfx.pxp)
colormap(bwr(64));
colorbar;
set(gca, 'clim', [0 1])
set(gca, 'Fontsize', 14);
title(comp_tit)
xlabel('Recovered');
ylabel('Simulated');
ax = gca;
ax.XTick = [1:size(res.main.ModSpace,2)];
ax.XTickLabel = mod_nr;
ax.YTick = [1:size(res.main.ModSpace,2)];
ax.YTickLabel = mod_names;

pos=get(gca,'position');
[rows,cols]=size(res.rec.bmc.rfx.pxp);
nums = flip(res.rec.bmc.rfx.pxp',2);
width=pos(3)/(cols);
height=pos(4)/(rows);
for i=1:cols
      for j=1:rows               
        annotation('textbox',[pos(1)+width*(i-1),pos(2)+height*(j-1),width,height], ...
        'string',num2str(nums(i,j), '%.2f'),'LineStyle','none','HorizontalAlignment','center',...
        'VerticalAlignment','middle');
      end
end

figdir = fullfile(saveDir, 'figures', 'sim', 'indiv', 'rec',...
    ['model_ident_rfxBMS_pxp']);
print(figdir, '-dtiff');
close;

%% REC: plot AVG rt trajectories & fits (same model sim + est)
figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
for m = 1:size(res.main.ModSpace,2)
    for n = 1:options.pilots.nS
        y_sim_mat(:,n) = res.rec.est(m,n,m).data.y(:,2);
        yhat_mat(:,n) = res.rec.est(m,n,m).data.optim.yhat(:,2);
    end
    mean_logRT_sim = mean(y_sim_mat, 2, 'omitnan');
    var_logRT_sim = var(y_sim_mat, 0, 2, 'omitnan');
    logupper_sim = mean_logRT_sim + sqrt(var_logRT_sim);
    loglower_sim = mean_logRT_sim - sqrt(var_logRT_sim);
    mean_yhat = mean(yhat_mat, 2, 'omitnan');
    var_yhat = var(yhat_mat, 0, 2, 'omitnan');
    upper_yhat = mean_yhat + sqrt(var_yhat);
    lower_yhat = mean_yhat - sqrt(var_yhat);
    
    subplot(4,2,m)
    plot(res.pilot.trials, mean_logRT_sim, 'LineWidth', 2, 'color', options.col.tnub)
    hold on
    fill([res.pilot.trials, fliplr(res.pilot.trials)], [(logupper_sim)', fliplr((loglower_sim)')], ...
             'b', 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
    plot(res.pilot.trials, mean_yhat, 'LineWidth', 2)
    fill([res.pilot.trials, fliplr(res.pilot.trials)], [(upper_yhat)', fliplr((lower_yhat)')], ...
             'r', 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
    xlim([1 160])
    ylabel('logRT [ms]')
    xlabel('trials')
    txt = ['model ' num2str(m)];
    title(txt)
end
legend('$log(y_{rt})$', '$Var(log(y_{rt}))$', '$log(\hat{y}_{rt})$', '$Var(log(\hat{y}_{rt}))$', 'Interpreter','latex', 'Position', [0.94 0.48 0.03 0.07])
figdir = fullfile(saveDir, 'figures', 'sim', 'indiv', 'rec',...
    ['sim_rec_avg_logRT']);
print(figdir, '-dtiff');
close;

%% REC: plot rt trajectories (same model sim & est)
for m = 1:size(res.main.ModSpace,2)
    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    for sub = 1:20
            subplot(4,5,sub)
            plot(res.rec.est(m,sub,m).data.y(:,2), 'color', options.col.tnub)
            hold on
            plot(res.rec.est(m,sub,m).data.optim.yhat(:,2), 'r')
            xlim([0 160])
            ylim([2 10])
            xlabel('trials')
            legend('$log({y}_{rt})$', '$log(\hat{y}_{rt})$', 'Interpreter', 'Latex', 'Position', [0.94 0.48 0.03 0.07])
            txt = ['sub ' num2str(sub)];
            title(txt)
    end
    figdir = fullfile(saveDir, 'figures', 'sim', 'indiv', 'rec',...
            ['sim_rec_logRT_20sub_model', num2str(m)]);
        print(figdir, '-dtiff');
        close;
end

%% REC: plot binary (hgf) & continuous trajectories together (same model sim & est)
for m = 1:size(res.main.ModSpace,2)
    for sub = 1:5 options.sim.indiv.nS
        spirl_tapas_ehgf_binary_combObs_plotTraj(res.rec.est(m,sub,m).data)
        figdir = fullfile(saveDir, 'figures', 'sim', 'indiv', 'rec', 'comb_traj',...
                ['binary_model', num2str(m), '_sub', num2str(sub)]);
            print(figdir, '-dtiff');
            close;
    end    
end

%% REC: plot AVG posterior param correlations
for m = 1:size(res.main.ModSpace,2)
    post_param_corr = zeros(size(res.rec.est(m,1,m).data.optim.Corr));
    avg_c = res.rec.est(m,1,m).data;
    for sub = 1:options.sim.indiv.nS
        post_param_corr = post_param_corr + res.rec.est(m,sub,m).data.optim.Corr;
    end
    avg_c.optim.Corr = post_param_corr./options.sim.indiv.nS;
    tapas_fit_plotCorr(avg_c)
    % axis labels
    ax = gca;
    ax.XTick = 1:(length(res.main.ModSpace(m).prc_idx)+length(res.main.ModSpace(m).obs_idx));
    ax.YTick = 1:(length(res.main.ModSpace(m).prc_idx)+length(res.main.ModSpace(m).obs_idx));
    if size(ax.XTick,2) == 5
        ax.XTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\Sigma'};
        ax.YTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\Sigma'};
    elseif size(ax.XTick,2) == 6
        ax.XTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\beta_1', '\Sigma'};
        ax.YTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\beta_1', '\Sigma'};
    elseif size(ax.XTick,2) == 7
        ax.XTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\beta_1', '\beta_2', '\Sigma'};
        ax.YTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\beta_1', '\beta_2', '\Sigma'};
    elseif size(ax.XTick,2) == 8
        ax.XTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\beta_1', '\beta_2', '\beta_3', '\Sigma'};
        ax.YTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\beta_1', '\beta_2', '\beta_3', '\Sigma'};
    elseif size(ax.XTick,2) == 9
        ax.XTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\beta_1', '\beta_2', '\beta_3', '\beta_4', '\Sigma'};
        ax.YTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\beta_1', '\beta_2', '\beta_3', '\beta_4', '\Sigma'};
    end
    figdir = fullfile(saveDir, 'figures', 'sim', 'indiv', 'rec',...
        ['post_param_corr_AVG_mod', num2str(m)]);
    print(figdir, '-dtiff');
    close;
end

%% REC: plot LogLl decomposed (sim mod = est mod)
for m = 1:size(res.main.ModSpace,2)
    clear mod(m).LogLlsplit;
    mod(m).LogLlsplit = NaN(options.sim.indiv.nS,2);
    for sub = 1:options.sim.indiv.nS
        mod(m).LogLlsplit(sub,:) = sum(res.rec.est(m,sub,m).data.optim.trialLogLlsplit, 'omitnan');
    end
    figure('Name', 'logLl contributions');
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    bar(mod(m).LogLlsplit)
    ylabel('log likelihood')
    xlabel('synthetic subjects')
    legend({'bin. logLl', 'cont. logLl'}, 'Location', 'Southeast')
    ylim([-200 200])
    figdir = fullfile(saveDir, 'figures', 'sim', 'indiv', 'rec',...
        ['LogLl_split_mod', num2str(m)]);
    print(figdir, '-dtiff');
    close;
end

%% REC: plot LME decomposed (sim mod = est mod)
for m = 1:size(res.main.ModSpace,2)
    clear mod(m).LME_decomp;
    mod(m).LME_decomp = NaN(options.sim.indiv.nS,5);
    mod(m).LME_decomp(:,1:2) = mod(m).LogLlsplit;
    for sub = 1:options.sim.indiv.nS
        mod(m).LME_decomp(sub,3) = res.rec.est(m,sub,m).data.optim.decompLME.logjoint + res.rec.est(m,sub,m).data.optim.negLl;
        mod(m).LME_decomp(sub,4) = res.rec.est(m,sub,m).data.optim.decompLME.postpredcorr;
        mod(m).LME_decomp(sub,5) = res.rec.est(m,sub,m).data.optim.decompLME.freepars;
    end
    figure('Name', 'LME decomp');
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    bar(mod(m).LME_decomp)
    xlabel('synthetic subjects')
    legend({'logLl bin', 'logLl cont', 'log Prior', 'post. corr.', 'free pars'},...
        'Position', [0.94 0.48 0.03 0.07])
    ylim([-200 200])
    figdir = fullfile(saveDir, 'figures', 'sim', 'indiv', 'rec',...
        ['decompLME_mod', num2str(m)]);
    print(figdir, '-dtiff');
    close;
end

%% FAM SIM: double-check family sizes
F_size_2 = NaN(size(res.sim.fam.elem));
for i = 1:size(res.sim.fam.elem,1)
    for j = 1:size(res.sim.fam.elem,2)
        F_size_2(i,j) = size(res.sim.fam.elem(i,j).F,2);
    end
end

figure
histogram(F_size_2)
ylabel('frequency')
xlabel('size F matrix (nSub)')

figdir = fullfile(saveDir, 'figures', 'sim', 'family',...
    ['hist_F_matrix_size_nSub']);
print(figdir, '-dtiff');
close;

%% FAM SIM: Ef - indiv n sim (family A)
% famA=1-famB, sub=60, 'classic'
figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
i = 1;
for famB = unique(res.sim.fam.t.freq_family_B)'
    for nS = unique(res.sim.fam.t.n_subject)'
        subplot(ceil(sqrt(length(unique(res.sim.fam.t.freq_family_B)))),ceil(sqrt(length(unique(res.sim.fam.t.freq_family_B)))),i)
        ax = bar(1-res.sim.fam.t.Ef(res.sim.fam.t.freq_family_B==famB & res.sim.fam.t.n_subject == nS & string(res.sim.fam.t.type) == 'classic'));
        ax.FaceColor = [0 0 1-famB];
        h = refline(0, 1-famB);
        h.Color = [0 0 0];
        ylim([0 1]);
        i = i+1;
    end
end
figdir = fullfile(saveDir, 'figures', 'sim', 'family',...
    ['fam_rec_Ef_classic_', num2str(options.sim.fam.opts(1).n_simulation*options.sim.fam.opts(1).n_jobs_sim), 'sim']);
print(figdir, '-dtiff');
close;

%% FAM SIM: xp - indiv n sim (family A)
% famA=1-famB, sub=60, 'classic'
figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
i = 1;
for famB = unique(res.sim.fam.t.freq_family_B)'
    for nS = unique(res.sim.fam.t.n_subject)'
        subplot(ceil(sqrt(length(unique(res.sim.fam.t.freq_family_B)))),ceil(sqrt(length(unique(res.sim.fam.t.freq_family_B)))),i)        
        ax = bar(1-res.sim.fam.t.xp(res.sim.fam.t.freq_family_B==famB & res.sim.fam.t.n_subject == nS & string(res.sim.fam.t.type) == 'classic'));
        ax.FaceColor = [0 1 1-famB];
        h = refline(0, 1-famB);
        h.Color = [0 0 0];
        ylim([0 1]);
        i = i+1;
    end
end
figdir = fullfile(saveDir, 'figures', 'sim', 'family',...
    ['fam_rec_xp_classic_', num2str(options.sim.fam.opts(1).n_simulation*options.sim.fam.opts(1).n_jobs_sim), 'sim']);
print(figdir, '-dtiff');
close;

%% FAM SIM: Ef - avg & indiv n sim (family A)
% famA=1-famB, sub=60, 'classic'
sub_idx = options.sim.fam.opts(1).n_subject;
fam_freq = unique(res.sim.fam.t.freq_family_B(ismember(res.sim.fam.t.n_subject, sub_idx)));

Ef_famA = [];
fam_freq_A = [];
for i = 1:numel(fam_freq)
    Ef_famA = [Ef_famA; res.sim.fam.t.Ef_famA(ismember(res.sim.fam.t.n_subject, sub_idx) & res.sim.fam.t.freq_family_B==fam_freq(i))];
    fam_freq_A = [fam_freq_A; (1-fam_freq(i))*ones(size(res.sim.fam.t.Ef_famA(ismember(res.sim.fam.t.n_subject, sub_idx) & res.sim.fam.t.freq_family_B==fam_freq(i))))];
    avgEf(i) = mean(res.sim.fam.t.Ef_famA(ismember(res.sim.fam.t.n_subject, sub_idx) & res.sim.fam.t.freq_family_B==fam_freq(i)));
    stdEf(i) = std(res.sim.fam.t.Ef_famA(ismember(res.sim.fam.t.n_subject, sub_idx) & res.sim.fam.t.freq_family_B==fam_freq(i)));
end
upper = avgEf + stdEf;
lower = avgEf - stdEf;

% avg Ef & std Ef
figure
hold on
plot(1-fam_freq, avgEf, 'LineWidth', 2, 'Color', options.col.tnub);
ax = axis;
fill([1-fam_freq', fliplr((1-fam_freq'))], [(upper), fliplr((lower))], ...
         options.col.tnub, 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
hold off
xlabel('true frequency family A')
ylabel('expected frequency family A')
legend('avg Ef', 'std Ef', 'Location', 'SouthEast')
figdir = fullfile(saveDir, 'figures', 'sim', 'family',...
    ['fam_rec_avgEf_vs_famfreq']);
print(figdir, '-dtiff');
close;

% Ef: indiv simulations
figure
s1 = scatter(fam_freq_A, Ef_famA, [], options.col.tnub, '.');
xlabel('true frequency family A')
ylabel('expected frequency family A')
figdir = fullfile(saveDir, 'figures', 'sim', 'family',...
    ['fam_rec_Ef_vs_famfreq']);
print(figdir, '-dtiff');
close;

%% FAM SIM: XP - avg & indiv n sim (family A)
% famA=1-famB, sub=60, 'classic'
sub_idx = options.sim.fam.opts(1).n_subject;
fam_freq = unique(res.sim.fam.t.freq_family_B(ismember(res.sim.fam.t.n_subject, sub_idx)));

xp_famA = [];
fam_freq_A = [];
for i = 1:numel(fam_freq)
    xp_famA = [xp_famA; res.sim.fam.t.xp_famA(ismember(res.sim.fam.t.n_subject, sub_idx) & res.sim.fam.t.freq_family_B==fam_freq(i))];
    fam_freq_A = [fam_freq_A; (1-fam_freq(i))*ones(size(res.sim.fam.t.xp_famA(ismember(res.sim.fam.t.n_subject, sub_idx) & res.sim.fam.t.freq_family_B==fam_freq(i))))];
    avgXP(i) = mean(res.sim.fam.t.xp_famA(ismember(res.sim.fam.t.n_subject, sub_idx) & res.sim.fam.t.freq_family_B==fam_freq(i)));
    stdXP(i) = std(res.sim.fam.t.xp_famA(ismember(res.sim.fam.t.n_subject, sub_idx) & res.sim.fam.t.freq_family_B==fam_freq(i)));
end
upper = avgXP + stdXP;
lower = avgXP - stdXP;

% avg XP & std XP
figure
hold on
plot(1-fam_freq, avgXP, 'LineWidth', 2, 'Color', options.col.tnub);
ax = axis;
fill([1-fam_freq', fliplr((1-fam_freq'))], [(upper), fliplr((lower))], ...
         options.col.tnub, 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
hold off
xlabel('true frequency family A')
ylabel('XP family A')
legend('avg XP', 'std XP', 'Location', 'SouthEast')
figdir = fullfile(saveDir, 'figures', 'sim', 'family',...
    ['fam_rec_XPavg_vs_famfreq']);
print(figdir, '-dtiff');
close;

% XP: indiv simulations
figure
s2 = scatter(fam_freq_A, xp_famA, [], options.col.tnub, '.');
xlabel('true frequency family A')
ylabel('XP family A')
figdir = fullfile(saveDir, 'figures', 'sim', 'family',...
    ['fam_rec_XP_vs_famfreq']);
print(figdir, '-dtiff');
close;

%% MAIN: plot model-agnostic RT analysis
spirl_modelagnostic_rt_analysis(res.main.SPIRL, res.main.logRT_mat,...
    res.main.trials, res.main.traj, options, 'main', saveDir);

%% MAIN: plot stay behaviour (binary)
% stay behaviour of pilot data set from line 56
[res.main.prev_corr, res.main.p_stay_avg, res.main.p_stay_var] = ...
    spirl_stay_behaviour(res.main.SPIRL, res.main.trials, options, 'main', saveDir);

% visualize switch-stay behaviour (main & pilot data set)
figure
% set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0.2 1 0.65]);
h1 = errorbar(res.main.prev_corr, res.main.p_stay_avg, sqrt(res.main.p_stay_var), 'o-');
h1.Color = options.col.tnub;
hold on
h2 = errorbar(res.main.prev_corr, res.pilot.p_stay_avg, sqrt(res.pilot.p_stay_var), 'o-');
h2.Color = options.col.grn;
ax1 = gca;
ax1.XTick = [0 1];
ax1.XTickLabel = {'incorrect' 'correct'};
xlabel('previous prediction')
ylabel('p(stay)')
xlim([-0.1 1.1])
ylim([0 1])
legend({'main data set', 'pilot data'}, 'Location', 'Southeast')

figdir = fullfile(saveDir, 'figures', 'main', 'model_agnostic',...
    ['stay_behaviour_corr_incorr_pilot_main']);
print(figdir, '-dtiff');
close;

%% MAIN: plot BMS results - family-level (primary RQ)
% XP (exceedance probabilities)
figure
bar(res.main.fam.out.families.ep)
ax1 = gca;
ax1.XTick = [1 2];
ax1.XTickLabel = {'Informed RT family' 'Uninformed family'};
ylabel('exceedance probability')
ylim([0 1])

figdir = fullfile(saveDir, 'figures', 'main',...
    ['main_BMS_family_XP']);
print(figdir, '-dtiff');
close;

% Ef (expected frequencies)
figure
bar(res.main.fam.out.families.Ef)
ax1 = gca;
ax1.XTick = [1 2];
ax1.XTickLabel = {'Informed RT family' 'Uninformed family'};
ylabel('expected frequency')
ylim([0 1])

figdir = fullfile(saveDir, 'figures', 'main',...
    ['main_BMS_family_Ef']);
print(figdir, '-dtiff');
close;

%% MAIN: plot BMS results - individual model level (secondary RQ)
% PXP (protected exceedance probabilities)
figure
bar(res.main.indiv.out.pxp)
ax1 = gca;
ax1.XTick = [1 2 3 4 5 6 7];
ax1.XTickLabel = {'M1' 'M2' 'M3' 'M4' 'M5' 'M6' 'M7'};
ylabel('protected exceedance probability')
ylim([0 1])

figdir = fullfile(saveDir, 'figures', 'main',...
    ['main_BMS_indiv_model_PXP']);
print(figdir, '-dtiff');
close;

% Ef (expected frequencies)
figure
bar(res.main.indiv.out.Ef)
ax1 = gca;
ax1.XTick = [1 2 3 4 5 6 7];
ax1.XTickLabel = {'M1' 'M2' 'M3' 'M4' 'M5' 'M6' 'M7'};
ylabel('expected frequency')
ylim([0 1])

figdir = fullfile(saveDir, 'figures', 'main',...
    ['main_BMS_indiv_model_Ef']);
print(figdir, '-dtiff');
close;

%% MAIN: plot difference between MAP estimates & init prior means: beta0-4
m=1;
figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
npars = 4;
for j = 1:npars
    subplot(3,2,j)
    k = j+2;
    raincloud_plot(res.main.param.obs(m).est(:,k), 'box_on', 1, 'color', options.col.tnub, 'alpha', 0.5,...
        'box_dodge', 1, 'box_dodge_amount', .15, 'dot_dodge_amount', .15,...
        'box_col_match', 0);
    hold on
    xline(res.pilot.ModSpace(m).obs_config.priormus(k)) % initial priors
    hold off
    if k == 1
        title('log(\zeta)')
    elseif k == size(res.pilot.priors.mod(m).obs_est, 2)
        title('log(\Sigma)')
    elseif k == 2
        title('\beta_0')
    elseif k == 3
        title('\beta_1')
    elseif k == 4
        title('\beta_2')
    elseif k == 5
        title('\beta_3')
    elseif k == 6
        title('\beta_4')
    end
    if res.main.param.ttest(j).h == 1
        hold on
        plot(median(res.main.param.obs(m).est(:,k)),0,'k*')
        hold off
    end
end

figdir = fullfile(saveDir, 'figures', 'main',...
    ['MAP_est_vs_init_priormu_be0-4_mod', num2str(m)]);
print(figdir, '-dtiff');
print(figdir, '-dsvg');
close;

%% MAIN: MAP estimates vs. init prior means (M1)
m=1;
npars = 5;

for i = 1:npars
    k = i+1;
    param_est{i,1} = res.main.param.obs(m).est(:,k);
end
figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
% plot MAP estimates of beta params (M1)
h = spirl_rm_raincloud(param_est, options.col.tnub); % view is flipped at the end of this function!!!
hold on
for i = 1:npars
    k = i+1;
    % plot initial prior mean of param k
    p = plot([res.pilot.ModSpace(m).obs_config.priormus(k) res.pilot.ModSpace(m).obs_config.priormus(k)],...
        [min(h.s{i}.YData)-0.5 max(h.s{i}.YData)+0.5],...
        'color', options.col.tnuy, 'LineWidth', 2);
end
uistack(p, 'bottom') % get empirical binary data to bottom
ax = gca;
ax.YTickLabel = {'be0', 'be1', 'be2', 'be3', 'be4'};
mrgn = 15;
ylim([ax.YTick(1)-mrgn ax.YTick(end)+mrgn])
% xlim([0.4 1]) % y-axis
hold off

figdir = fullfile(saveDir, 'figures', 'main',...
    ['MAP_est_vs_init_priormu_be0-4_rm_raincloud_mod_', num2str(m)]);
print(figdir, '-dtiff');
print(figdir, '-dsvg');
close;

%% MAIN: plot binary (hgf) & continuous trajectories together
for m = 1:size(res.main.ModSpace,2)
    for sub = 1:options.main.nS
        spirl_tapas_ehgf_binary_combObs_plotTraj(res.main.est(m,sub))
        figdir = fullfile(saveDir, 'figures', 'main', 'fits', 'comb',...
            ['binary_model', num2str(m), '_sub', num2str(sub)]);
        print(figdir, '-dtiff');
        close;
    end    
end

%% MAIN: plot AVG rt trajectories & fits
mean_logRT = mean(res.main.logRT_mat, 2, 'omitnan');
var_logRT = var(res.main.logRT_mat, 0, 2, 'omitnan');
logupper = mean_logRT + sqrt(var_logRT);
loglower = mean_logRT - sqrt(var_logRT);
figure
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
for m = 1:size(res.main.ModSpace,2)
    for n = 1:options.main.nS
        yhat_mat(:,n) = res.main.est(m,n).optim.yhat(:,2);
    end
    mod(m).mean_yhat = mean(yhat_mat, 2, 'omitnan');
    mod(m).var_yhat = var(yhat_mat, 0, 2, 'omitnan');
    mod(m).upper_yhat = mod(m).mean_yhat + sqrt(mod(m).var_yhat);
    mod(m).lower_yhat = mod(m).mean_yhat - sqrt(mod(m).var_yhat);
    
    subplot(4,2,m)
    plot(res.main.trials, mean_logRT, 'LineWidth', 2, 'color', options.col.tnub)
    hold on
    fill([res.main.trials, fliplr(res.main.trials)], [(logupper)', fliplr((loglower)')], ...
             'b', 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
    plot(res.main.trials, mod(m).mean_yhat, 'LineWidth', 2)
    fill([res.main.trials, fliplr(res.main.trials)], [(mod(m).upper_yhat)', fliplr((mod(m).lower_yhat)')], ...
             'r', 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
    xlim([1 160])
    ylabel('logRT [ms]')
    xlabel('trials')
    txt = ['model ' num2str(m)];
    title(txt)
end
legend('$log(y_{rt})$', '$Var(log(y_{rt}))$', '$log(\hat{y}_{rt})$', '$Var(log(\hat{y}_{rt}))$', 'Interpreter','latex', 'Position', [0.94 0.48 0.03 0.07])
figdir = fullfile(saveDir, 'figures', 'main', 'fits', 'logRT',...
    ['avg_logRT']);
print(figdir, '-dtiff');
close;

%% MAIN: plot AVG rt trajectories, fits & regressors
for m = 1:size(res.main.ModSpace,2) %%%
    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0.3 0 0.45 1]);
    subplot(2,1,1)
    plot(res.main.trials, mean_logRT, 'LineWidth', 2, 'color', options.col.tnub)
    hold on
    fill([res.main.trials, fliplr(res.main.trials)], [(logupper)', fliplr((loglower)')], ...
             'b', 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
    plot(res.main.trials, mod(m).mean_yhat, 'LineWidth', 2)
    fill([res.main.trials, fliplr(res.main.trials)], [(mod(m).upper_yhat)', fliplr((mod(m).lower_yhat)')], ...
             'r', 'EdgeAlpha', 0, 'FaceAlpha', 0.15);
    legend('$log(y_{rt})$', '$Var(log(y_{rt}))$', '$log(\hat{y}_{rt})$', '$Var(log(\hat{y}_{rt}))$', 'Interpreter','latex')
    xlim([1 160])
    ylabel('logRT [ms]')
    title('avg log rt fits')
    
    % AVG regressors
    subplot(2,1,2)
    hold on
    if m == 1
        for n = 1:options.main.nS
            m1hreg = res.main.est(m,n).traj.muhat(:,1);
            poo = m1hreg.^res.main.est(m,n).u.*(1-m1hreg).^(1-res.main.est(m,n).u); % probability of observed outcome
            surp = -log2(poo);
            surp_shifted_mat(:,n) = [1; surp(1:(length(surp)-1))];
            sh1_mat(:,n) = res.main.est(m,n).traj.sahat(:,1);
            sh2_mat(:,n) = res.main.est(m,n).traj.sahat(:,2);
            mh3_mat(:,n) = res.main.est(m,n).traj.muhat(:,3);            
        end
        plot(res.main.trials, mean(surp_shifted_mat, 2, 'omitnan'), 'Color', [0 0.1 0.6]);
        plot(res.main.trials, mean(sh1_mat, 2, 'omitnan'), 'Color', [0 0.5 0])
        plot(res.main.trials, mean(sh2_mat, 2, 'omitnan'), 'Color', [0.5 0.5 0])
        plot(res.main.trials, mean(exp(mh3_mat), 2, 'omitnan'), 'Color', [0.5 0 0])
        legend({'$S_{shifted}$', '$\hat{\sigma}_{1}$', '$\hat{\sigma}_{2}$', '$exp(\hat{\mu}_{3})$'}, 'Interpreter', 'Latex')
        axis('tight')
    elseif m == 2
        for n = 1:options.main.nS
            sa2_mat(:,n) = res.main.est(m,n).traj.sa(:,2);
            mh3_mat(:,n) = res.main.est(m,n).traj.muhat(:,3);
        end
        plot(res.main.trials, mean(sa2_mat, 2, 'omitnan'), 'Color', [0 0.5 0])
        plot(res.main.trials, mean(mh3_mat, 2, 'omitnan'), 'Color', [0 1 0])
        legend({'$\sigma_{2}$', '$\hat{\mu}_{3}$'}, 'Interpreter', 'Latex')
        axis('tight')
    elseif m == 3
        for n = 1:options.main.nS
            sh1_mat(:,n) = res.main.est(m,n).traj.sahat(:,1);
            inf_shift_mat(:,n) = [0.1; res.main.est(m,n).traj.sa(1:(length(res.main.trials)-1),2)];
            env_mat(:,n) = res.main.est(m,n).traj.sahat(:,2) - inf_shift_mat(:,n);
        end
        plot(res.main.trials, mean(sh1_mat, 2, 'omitnan'), 'Color', [0 0.5 0])
        plot(res.main.trials, mean(inf_shift_mat, 2, 'omitnan'), 'Color', [0.5 0.5 0])
        plot(res.main.trials, mean(env_mat, 2, 'omitnan'), 'Color', [0.5 0 0])
        legend({'$\hat{\sigma}_{1}$', '${\sigma}_{2}$', '$exp(\kappa_{2}\hat{\mu}_{3}+\omega_{2})$'}, 'Interpreter', 'Latex')
    elseif m == 4
        for n = 1:options.main.nS
            inf_shift_mat(:,n) = [0.1; res.main.est(m,n).traj.sa(1:(length(res.main.trials)-1),2)];
            env_mat(:,n) = res.main.est(m,n).traj.sahat(:,2) - inf_shift_mat(:,n);
        end
        plot(res.main.trials, mean(inf_shift_mat, 2, 'omitnan'), 'Color', [0.5 0.5 0])
        plot(res.main.trials, mean(env_mat, 2, 'omitnan'), 'Color', [0.5 0 0])
        legend({'${\sigma}_{2}$', '$exp(\kappa_{2}\hat{\mu}_{3}+\omega_{2})$'}, 'Interpreter', 'Latex')
    elseif m == 5
        for n = 1:options.main.nS
            be0(:,n) = res.main.est(m,n).p_obs.p(2);
        end
        plot(res.main.trials, ones(size(res.main.trials))*mean(be0, 2, 'omitnan'), 'Color', [0 0.5 0])
        legend({'$\beta_{0}$'}, 'Interpreter', 'Latex')
    elseif m == 6
        for n = 1:options.main.nS
            be0(:,n) = res.main.est(m,n).p_obs.p(2);
            be1(:,n) = res.main.est(m,n).p_obs.p(3);
        end
        lin_decay = mean(be1, 2, 'omitnan').*res.main.trials./length(res.main.trials);
        plot(res.main.trials, mean(be0, 2, 'omitnan')+lin_decay, 'Color', [0 0.5 0])
        ylim([5.2 6.8])
        legend({'$k$'}, 'Interpreter', 'Latex')
    elseif m == 7
        for n = 1:options.main.nS
            be0_corr(:,n) = res.main.est(m,n).p_obs.p(2);
            be0_incorr(:,n) = res.main.est(m,n).p_obs.p(3);
        end
        plot(res.main.trials, ones(size(res.main.trials))*mean(be0_corr, 2, 'omitnan'), 'Color', [0 0.5 0])
        plot(res.main.trials, ones(size(res.main.trials))*mean(be0_incorr, 2, 'omitnan'), 'Color', [0.5 0.5 0])
        ylim([5.2 6.8])
        legend({'$\beta_{0,correct}$', '${\beta}_{0,incorrect}$'}, 'Interpreter', 'Latex')        
    end
    ylabel('logRT [ms]')
    xlabel('trials')
    txt = ['regressors model ' num2str(m)];
    title(txt)
    
    figdir = fullfile(saveDir, 'figures', 'main', 'fits', 'logRT',...
        ['avg_logRT_regressors_mod', num2str(m)]);
    print(figdir, '-dtiff');
    close;
end

%% MAIN: plot rt trajectories & fits (single-sub)
for m = 1:size(res.main.ModSpace,2)
    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    for sub = 1:options.main.nS
        subplot(7,9,sub)
        plot(res.main.est(m,sub).y(:,2), 'color', options.col.tnub)
        hold on
        plot(res.main.est(m,sub).optim.yhat(:,2), 'r')
        xlim([0 160])
        ylim([2 10])
        ylabel('logRT [ms]')
        xlabel('trials')
        txt = ['pilot sub ' num2str(sub)];
        title(txt)
    end
    legend('$log(y_{rt})$', '$log(\hat{y}_{rt})$', 'Interpreter','latex', 'Position', [0.94 0.48 0.03 0.07])
    figdir = fullfile(saveDir, 'figures', 'main', 'fits', 'logRT',...
        ['logRT_model', num2str(m)]);
    print(figdir, '-dtiff');
    close;
end

%% MAIN: plot rt fits + regressors (single-sub)
for m = 1:size(res.main.ModSpace,2)
    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    for n = 1:options.main.nS
        subplot(7,9,n)
        plot(res.main.est(m,n).y(:,2), 'color', options.col.tnub)
        hold on
        plot(res.main.est(m,n).optim.yhat(:,2), 'r')
        if m == 1
            m1hreg = res.main.est(m,n).traj.muhat(:,1);
            poo = m1hreg.^res.main.est(m,n).u.*(1-m1hreg).^(1-res.main.est(m,n).u); % probability of observed outcome
            surp = -log2(poo);
            surp_shifted = [1; surp(1:(length(surp)-1))];
            plot(res.main.trials, surp_shifted, 'Color', [0 0.1 0.6]);
            plot(res.main.trials, res.main.est(m,n).traj.sahat(:,1), 'Color', [0 0.5 0])
            plot(res.main.trials, res.main.est(m,n).traj.sahat(:,2), 'Color', [0.5 0.5 0])
            plot(res.main.trials, res.main.est(m,n).traj.muhat(:,3), 'Color', [0.5 0 0])
        elseif m == 2
            plot(res.main.trials, res.main.est(m,n).traj.sa(:,2), 'Color', [0 0.5 0])
            plot(res.main.trials, res.main.est(m,n).traj.muhat(:,3), 'Color', [0 1 0])
        elseif m == 3
            inf_shift = [0.1; res.main.est(m,n).traj.sa(1:(length(res.main.trials)-1),2)];
            env = res.main.est(m,n).traj.sahat(:,2) - inf_shift;
            plot(res.main.trials, res.main.est(m,n).traj.sahat(:,1), 'Color', [0 0.5 0])
            plot(res.main.trials, inf_shift, 'Color', [0.5 0.5 0])
            plot(res.main.trials, env, 'Color', [0.5 0 0])
        elseif m == 4
            inf_shift = [0.1; res.main.est(m,n).traj.sa(1:(length(res.main.trials)-1),2)];
            env = res.main.est(m,n).traj.sahat(:,2) - inf_shift;
            plot(res.main.trials, inf_shift, 'Color', [0.5 0.5 0])
            plot(res.main.trials, env, 'Color', [0.5 0 0])
        elseif m == 5
            be0 = res.main.est(m,n).p_obs.p(2);
            plot(res.main.trials, ones(size(res.main.trials))*be0, 'Color', [0 0.5 0])
        elseif m == 6
            be0 = res.main.est(m,n).p_obs.p(2);
            lin_decay = res.main.est(m,n).p_obs.p(3).*res.main.trials./length(res.main.trials);
            plot(res.main.trials, be0+lin_decay, 'Color', [0 0.5 0])
        elseif m == 7
            be0_corr = res.main.est(m,n).p_obs.p(2);
            be0_incorr = res.main.est(m,n).p_obs.p(3);
            plot(res.main.trials, ones(size(res.main.trials))*be0_corr, 'Color', [0 0.5 0])
            plot(res.main.trials, ones(size(res.main.trials))*be0_incorr, 'Color', [0.5 0.5 0])
        end
        axis('tight')
        ylabel('logRT [ms]')
        xlabel('trials')
        txt = ['main sub ' num2str(n)];
        title(txt)
    end
    if m == 1
        legend({'$log(y_{rt})$', '$log(\hat{y}_{rt})$', '$S_{shifted}$', '$\hat{\sigma}_{1}$', '$\hat{\sigma}_{2}$', '$exp(\hat{\mu}_{3})$'}, 'Interpreter','latex', 'Position', [0.94 0.48 0.03 0.07])
    elseif m == 2
        legend({'$log(y_{rt})$', '$log(\hat{y}_{rt})$', '$\sigma_{2}$', '$\hat{\mu}_{3}$'}, 'Interpreter','latex', 'Position', [0.94 0.48 0.03 0.07])
    elseif m == 3
        legend({'$log(y_{rt})$', '$log(\hat{y}_{rt})$', '$\hat{\sigma}_{1}$', '${\sigma}_{2}$', '$exp(\kappa_{2}\hat{\mu}_{3}+\omega_{2})$'}, 'Interpreter','latex', 'Position', [0.94 0.48 0.03 0.07])
    elseif m == 4
        legend({'$log(y_{rt})$', '$log(\hat{y}_{rt})$', '${\sigma}_{2}$', '$exp(\kappa_{2}\hat{\mu}_{3}+\omega_{2})$'}, 'Interpreter','latex', 'Position', [0.94 0.48 0.03 0.07])
    elseif m == 5
        legend({'$log(y_{rt})$', '$log(\hat{y}_{rt})$', '$\beta_{0}$'}, 'Interpreter','latex', 'Position', [0.94 0.48 0.03 0.07])
    elseif m == 6
        legend({'$log(y_{rt})$', '$log(\hat{y}_{rt})$', '$k$'}, 'Interpreter','latex', 'Position', [0.94 0.48 0.03 0.07])
    elseif m == 7
        legend({'$log(y_{rt})$', '$log(\hat{y}_{rt})$', '$\beta_{0,correct}$', '${\beta}_{0,incorrect}$'}, 'Interpreter','latex', 'Position', [0.94 0.48 0.03 0.07])
    end
    figdir = fullfile(saveDir, 'figures', 'main', 'fits', 'logRT',...
        ['logRT_regressors_model', num2str(m)]);
    print(figdir, '-dtiff');
    close;
end

%% MAIN: convergence of opt algo
n_max = 10;
figs = ceil(options.main.nS/n_max);

for m = 1:size(res.main.ModSpace,2)
    % obj fct
    for f = 1:figs
        figure
        set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
        for n_idx = 1:n_max
            n_sub = n_idx + n_max*(f-1);
            if n_sub <= options.main.nS
                subplot(ceil(n_max/2), 2, n_idx)
                plot(0, res.main.est(m,n_sub).optim.iter.val(1), 'o',...
                    'color', options.col.tnub) %init value
                hold on
                plot([0:options.opt_config.maxIter], res.main.est(m,n_sub).optim.iter.val,...
                    'color', options.col.tnub)
                for i = 1:size(res.main.est(m,n_sub).optim.iter.rst, 2)
                    xline(res.main.est(m,n_sub).optim.iter.rst(i), '--')
                end
                xlim([0 options.opt_config.maxIter])
                xlabel('opt algo iterations')
                ylabel('nlj')
                txt = ['main sub ' num2str(n_sub)];
                title(txt)
            end
            n_low = n_sub-(n_max-1);
            if n_sub > options.main.nS
                n_sub = options.main.nS;
            end
        end
        figdir = fullfile(saveDir, 'figures', 'main',...
            'fits', 'opt_algo',...
            ['nlj_optim_mod', num2str(m), '_sub', num2str(n_low), '-', num2str(n_sub)]);
        print(figdir, '-dtiff');
        close;
    end
    
    % params
    for f = 1:figs
        figure
        set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
        for n_idx = 1:n_max
            n_sub = n_idx + n_max*(f-1);
            if n_sub <= options.main.nS
                subplot(ceil(n_max/2), 2, n_idx)
                plot(zeros(size(res.main.est(m,n_sub).optim.iter.x(1,:))),...
                    res.main.est(m,n_sub).optim.iter.x(1,:), 'o')%,...
                hold on
                plot(repmat([0:options.opt_config.maxIter]', 1, size(res.main.est(m,n_sub).optim.iter.x,2)),...
                    res.main.est(m,n_sub).optim.iter.x)%,...
                for i = 1:size(res.main.est(m,n_sub).optim.iter.rst, 2)
                    xline(res.main.est(m,n_sub).optim.iter.rst(i), '--')
                end
                xlim([0 options.opt_config.maxIter])
                xlabel('opt algo iterations')
                ylabel('parameter values')
                txt = ['main sub ' num2str(n_sub)];
                title(txt)
            end
            n_low = n_sub-(n_max-1);
            if n_sub > options.main.nS
                n_sub = options.main.nS;
            end
        end
        figdir = fullfile(saveDir, 'figures', 'main',...
            'fits', 'opt_algo',...
            ['param_optim_mod', num2str(m), '_sub', num2str(n_low), '-', num2str(n_sub)]);
        print(figdir, '-dtiff');
        close;
    end
end

%% MAIN: plot LogLl decomposed
mod = struct;
for m = 1:size(res.main.ModSpace,2)
    mod(m).LogLlsplit = NaN(options.main.nS,2);
    for sub = 1:options.main.nS
        mod(m).LogLlsplit(sub,:) = sum(res.main.est(m,sub).optim.trialLogLlsplit, 'omitnan');
    end
    figure('Name', 'logLl contributions');
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    bar(mod(m).LogLlsplit)
    ylabel('log likelihood')
    xlabel('main subjects')
    legend({'bin. logLl', 'cont. logLl'}, 'Location', 'Southeast')
    ylim([-200 200])
    figdir = fullfile(saveDir, 'figures', 'main', 'fits',...
        ['LogLl_split_mod', num2str(m)]);
    print(figdir, '-dtiff');
    close;
end

%% MAIN: plot LME decomposed
for m = 1:size(res.main.ModSpace,2)
    mod(m).LME_decomp = NaN(options.main.nS,5);
    mod(m).LME_decomp(:,1:2) = mod(m).LogLlsplit;
    for sub = 1:options.main.nS
        mod(m).LME_decomp(sub,3) = res.main.est(m,sub).optim.decompLME.logjoint + res.main.est(m,sub).optim.negLl;
        mod(m).LME_decomp(sub,4) = res.main.est(m,sub).optim.decompLME.postpredcorr;
        mod(m).LME_decomp(sub,5) = res.main.est(m,sub).optim.decompLME.freepars;
    end
    figure('Name', 'logLl contributions');
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    bar(mod(m).LME_decomp)
    xlabel('main subjects')
    legend({'logLl bin', 'logLl cont', 'log Prior', 'post. corr.', 'free pars'},...
        'Position', [0.94 0.48 0.03 0.07])
    ylim([-200 200])
    figdir = fullfile(saveDir, 'figures', 'main', 'fits',...
        ['decompLME_mod', num2str(m)]);
    print(figdir, '-dtiff');
    close;
end

%% MAIN: plot AVG posterior param correlations
for m = 1:size(res.main.ModSpace,2)
    post_param_corr = zeros(size(res.main.est(m,1).optim.Corr));
    avg_c = res.main.est(m,1);
    for sub = 1:options.main.nS
        post_param_corr = post_param_corr + res.main.est(m,sub).optim.Corr;
    end
    avg_c.optim.Corr = post_param_corr./options.main.nS;
    tapas_fit_plotCorr(avg_c)
    % axis labels
    ax = gca;
    ax.XTick = 1:(length(res.main.ModSpace(m).prc_idx)+length(res.main.ModSpace(m).obs_idx));
    ax.YTick = 1:(length(res.main.ModSpace(m).prc_idx)+length(res.main.ModSpace(m).obs_idx));
    if size(ax.XTick,2) == 5
        ax.XTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\Sigma'};
        ax.YTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\Sigma'};
    elseif size(ax.XTick,2) == 6
        ax.XTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\beta_1', '\Sigma'};
        ax.YTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\beta_1', '\Sigma'};
    elseif size(ax.XTick,2) == 7
        ax.XTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\beta_1', '\beta_2', '\Sigma'};
        ax.YTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\beta_1', '\beta_2', '\Sigma'};
    elseif size(ax.XTick,2) == 8
        ax.XTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\beta_1', '\beta_2', '\beta_3', '\Sigma'};
        ax.YTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\beta_1', '\beta_2', '\beta_3', '\Sigma'};
    elseif size(ax.XTick,2) == 9
        ax.XTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\beta_1', '\beta_2', '\beta_3', '\beta_4', '\Sigma'};
        ax.YTickLabel = {'\omega_2', '\omega_3', '\zeta', '\beta_0', '\beta_1', '\beta_2', '\beta_3', '\beta_4', '\Sigma'};
    end
    figdir = fullfile(saveDir, 'figures', 'main', 'fits',...
        ['post_param_corr_AVG_mod', num2str(m)]);
    print(figdir, '-dtiff');
    close;
end

%% MAIN (PPC indiv): plot continouous RTs, MODEL 1
% (generated logRT trajs, empirical data, MAP predictions)
m = 1;
res.main.ppc.ppc_sub_shuffled = ...
    res.main.ppc.ppc_sub(randperm(length(res.main.ppc.ppc_sub)));
n_plots = ceil(length(res.main.ppc.ppc_sub)/8);
for p = 1:n_plots
    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    for s = 1:8
        s_idx = s + 8*(p-1);
        if s_idx <= length(res.main.ppc.ppc_sub)
            n = res.main.ppc.ppc_sub_shuffled(s_idx);
            subplot(4,2,s)
            % plot empirical logRT traj of post_sub n
            a=plot(res.main.est(m,res.main.ppc.ppc_sub(n)).y(:,2), 'r', 'LineWidth', 2);
            hold on
            % plot logRT PREDICTIONS generated using MAP values
            plot(res.main.ppc.mod(m).sub(n).map_sim.yhat(:,2),...
                'LineWidth', 2, 'color', options.col.tnub)
            % plot simulated logRTs from draws from posterior
            for i = 1:options.main.ppc.nDraws_per_sub
                try
                    plot(res.main.ppc.mod(m).sub(n,i).sim.y(:,2), 'color', [options.col.tnub 0.1]) % alpha=0.1
                end
            end
            % RT limits by experiment
            plot(1:160, ones(160,1)*log(1700), '--k')
            plot(1:160, ones(160,1)*log(100), '--k')

            legend({'emp. RTs', 'MAP pred.', 'post. sim'},...
                'Position', [0.50 0.48 0.03 0.07])
            ylim([4 8])
            ylabel('y_{rt,sim}')
            xlabel('trials')
            txt = ['sub ' num2str(n)];
            title(txt)

            uistack(a, 'top') % get empirical RTs to top
        end
    end
    figdir = fullfile(saveDir, 'figures', 'main', 'ppc', 'cont',...
        ['m1_logRT_MAP_pred_plot', num2str(p), '_', num2str(options.main.ppc.nDraws_per_sub), 'draws']);
    print(figdir, '-dtiff');
    close;
end

%% MAIN (PPC indiv): plot generated logRT trajectories (& empirical data) + MAP
options.rng.idx = 1;
for n = 1:length(res.main.ppc.ppc_sub)
    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    for m = 1:size(res.main.ModSpace,2)

        subplot(4,2,m)
        try
            plot(res.main.ppc.mod(m).sub(n,i).sim.y(:,2), 'color', [0 0.7 0.9])
        end
        hold on
        for i = 2:options.main.ppc.nDraws_per_sub
            try
                plot(res.main.ppc.mod(m).sub(n,i).sim.y(:,2), 'color', [0 0.7 0.9])
            end
        end
        
        % plot logRT generated using MAP values
        plot(res.main.ppc.mod(m).sub(n).map_sim.y(:,2),...
            'LineWidth', 2, 'color', options.col.tnub)

        % plot empirical logRT traj of post_sub n
        plot(res.main.est(m,res.main.ppc.ppc_sub(n)).y(:,2), 'r', 'LineWidth', 2)

        plot(1:160, ones(160,1)*log(1700), '--k')
        plot(1:160, ones(160,1)*log(100), '--k')
        ylim([4 8])
        ylabel('y_{rt,sim}')
        xlabel('trials')
        txt = ['model ' num2str(m)];
        title(txt)
    end
    figdir = fullfile(saveDir, 'figures', 'main', 'ppc', 'cont',...
        ['logRT_trajectories_MAP_sub', num2str(res.main.ppc.ppc_sub(n)), '_', num2str(options.main.ppc.nDraws_per_sub), 'draws']);
    print(figdir, '-dtiff');
    close;
end

%% MAIN (PPC indiv): plot generated logRT trajectories (& empirical data) + MAP (PREDICTIONS)
for n = 1:length(res.main.ppc.ppc_sub)
    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    for m = 1:size(res.main.ModSpace,2)      
        subplot(4,2,m)
        try
            plot(res.main.ppc.mod(m).sub(n,i).sim.y(:,2), 'color', [0 0.7 0.9])
        end
        hold on
        for i = 2:options.main.ppc.nDraws_per_sub
            try
                plot(res.main.ppc.mod(m).sub(n,i).sim.y(:,2), 'color', [0 0.7 0.9])
            end
        end
        
        % plot logRT PREDICTIONS generated using MAP values
        plot(res.main.ppc.mod(m).sub(n).map_sim.yhat(:,2),...
            'LineWidth', 2, 'color', options.col.tnub)

        % plot empirical logRT traj of post_sub n
        plot(res.main.est(m,res.main.ppc.ppc_sub(n)).y(:,2), 'r', 'LineWidth', 2)

        plot(1:160, ones(160,1)*log(1700), '--k')
        plot(1:160, ones(160,1)*log(100), '--k')
        ylim([4 8])
        ylabel('y_{rt,sim}')
        xlabel('trials')
        txt = ['model ' num2str(m)];
        title(txt)
    end
    figdir = fullfile(saveDir, 'figures', 'main', 'ppc', 'cont',...
        ['logRT_MAP_PREDICTIONS_sub', num2str(res.main.ppc.ppc_sub(n)), '_', num2str(options.main.ppc.nDraws_per_sub), 'draws']);
    print(figdir, '-dtiff');
    close;
end

%% MAIN (PPC indiv): plot generated logRT trajectories (& empirical data) + MEAN over posterior samples
for n = 1:length(res.main.ppc.ppc_sub)
    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    for m = 1:size(res.main.ModSpace,2)
        subplot(4,2,m)
        try
            plot(res.main.ppc.mod(m).sub(n,i).sim.y(:,2), 'color', [0 0.7 0.9])
        end
        hold on
        for i = 2:options.main.ppc.nDraws_per_sub
            try
                plot(res.main.ppc.mod(m).sub(n,i).sim.y(:,2), 'color', [0 0.7 0.9])
            end
        end
        
        % plot mean logRT over posterior samples
        plot(mean(res.main.ppc.mod(m).sub(n).logRT_mat,2, 'omitnan'),...
            'LineWidth', 2, 'color', options.col.tnub)

        % plot empirical logRT traj of post_sub n
        plot(res.main.est(m,res.main.ppc.ppc_sub(n)).y(:,2), 'r', 'LineWidth', 2)

        plot(1:160, ones(160,1)*log(1700), '--k')
        plot(1:160, ones(160,1)*log(100), '--k')
        ylim([4 8])
        ylabel('y_{rt,sim}')
        xlabel('trials')
        txt = ['model ' num2str(m)];
        title(txt)
    end
    figdir = fullfile(saveDir, 'figures', 'main', 'ppc', 'cont',...
        ['logRT_trajectories_MeanOverPostSamples_sub', num2str(res.main.ppc.ppc_sub(n)), '_', num2str(options.main.ppc.nDraws_per_sub), 'draws']);
    print(figdir, '-dtiff');
    close;
end

%% MAIN (PPC indiv): plot generated logRT PREDICTIONS (& empirical data) + MEAN PRED over posterior samples
for n = 1:length(res.main.ppc.ppc_sub)
    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    for m = 1:size(res.main.ModSpace,2)
        subplot(4,2,m)
        try
            plot(res.main.ppc.mod(m).sub(n,i).sim.yhat(:,2), 'color', [0 0.7 0.9])
        end
        hold on
        for i = 2:options.main.ppc.nDraws_per_sub
            try
                plot(res.main.ppc.mod(m).sub(n,i).sim.yhat(:,2), 'color', [0 0.7 0.9])
            end
        end
        
        % plot mean logRT over posterior samples
        plot(mean(res.main.ppc.mod(m).sub(n).yhat_mat,2, 'omitnan'),...
            'LineWidth', 2, 'color', options.col.tnub)

        % plot empirical logRT traj of post_sub n
        plot(res.main.est(m,res.main.ppc.ppc_sub(n)).y(:,2), 'r', 'LineWidth', 2)

        plot(1:160, ones(160,1)*log(1700), '--k')
        plot(1:160, ones(160,1)*log(100), '--k')
        ylim([4 8])
        ylabel('y_{rt,sim}')
        xlabel('trials')
        txt = ['model ' num2str(m)];
        title(txt)
    end
    figdir = fullfile(saveDir, 'figures', 'main', 'ppc', 'cont',...
        ['logRT_predictions_MeanOverPostSamples_sub', num2str(res.main.ppc.ppc_sub(n)), '_', num2str(options.main.ppc.nDraws_per_sub), 'draws']);
    print(figdir, '-dtiff');
    close;
end

%% MAIN (PPC indiv): plot generated logRT trajectories (& empirical data)
for n = 1:length(res.main.ppc.ppc_sub)
    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    for m = 1:size(res.main.ModSpace,2)
        subplot(4,2,m)
        try
            plot(res.main.ppc.mod(m).sub(n,i).sim.y(:,2), 'color', options.col.tnub)
        end
        hold on
        for i = 2:options.main.ppc.nDraws_per_sub
            try
                plot(res.main.ppc.mod(m).sub(n,i).sim.y(:,2), 'color', options.col.tnub)
            end
        end

        % plot empirical logRT traj of post_sub n
        plot(res.main.est(m,res.main.ppc.ppc_sub(n)).y(:,2), 'r', 'LineWidth', 2)

        plot(1:160, ones(160,1)*log(1700), '--k')
        plot(1:160, ones(160,1)*log(100), '--k')
        ylim([4 8])
        ylabel('y_{rt,sim}')
        xlabel('trials')
        txt = ['model ' num2str(m)];
        title(txt)
    end
    figdir = fullfile(saveDir, 'figures', 'main', 'ppc', 'cont',...
        ['logRT_trajectories_sub', num2str(res.main.ppc.ppc_sub(n)), '_', num2str(options.main.ppc.nDraws_per_sub), 'draws']);
    print(figdir, '-dtiff');
    close;
end

%% MAIN (PPC indiv bin): plot ajdusted correctness model 1 (RAINCLOUD)
m=1;
n_max = 10;
n_fig = ceil(length(res.main.ppc.ppc_sub)/n_max);
for f = 1:n_fig

    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    hold on

    sub_label = [];
    for s = 1:n_max
        n_idx = s + n_fig*(f-1);
        if n_idx <= length(res.main.ppc.ppc_sub)
            n = res.main.ppc.ppc_sub_shuffled(n_idx);
            sub_label = [sub_label n];
            adj_corr{s,1} = res.main.ppc.mod(m).sub(n).adj_correct';
        end
    end
    
    % plot data sampled from posterior of sub n
    h = spirl_rm_raincloud(adj_corr, options.col.tnub); % view is flipped at the end of this function!!!
    hold on
    for n = 1:n_max
        % plot empirical data of sub n
        p = plot([res.main.SPIRL(res.main.ppc.ppc_sub(n)).adj_correct res.main.SPIRL(res.main.ppc.ppc_sub(n)).adj_correct],...
            [min(h.s{n}.YData)-1 max(h.s{n}.YData)+1],...
            'color', options.col.tnuy, 'LineWidth', 2);
    end

    uistack(p, 'bottom') % get empirical binary data to bottom
    
    ax = gca;
    ax.YTickLabel = {sub_label};
    mrgn = 15;
    ylim([ax.YTick(1)-mrgn ax.YTick(end)+mrgn])
    xlim([0.4 1]) % y-axis
    xlabel('adjusted correctness (binary predictions)')
    ylabel('subject')
    legend({'empirical $y_{bin}$', 'density ($\hat{y}_{bin,pp}$ samples)', '$\hat{y}_{bin,pp}$ samples'}, 'Interpreter', 'Latex',...
        'Position', [0.78 0.16 0.08 0.07])
    title(['model ', num2str(m)])

    figdir = fullfile(saveDir, 'figures', 'main', 'ppc', 'bin',...
        ['adj_correctness_raincloud_', num2str(length(res.main.ppc.ppc_sub)), 'sub_',...
        num2str(options.main.ppc.nDraws_per_sub), 'draws_mod', num2str(m),...
        '_fig', num2str(f)]);
%     print(figdir, '-deps'); % option to save as vector graphic (uncomment if desired
    print(figdir, '-dtiff');
    close;
    clear adj_corr;    
end

%% MAIN (PPC indiv bin): plot adjusted correctness
for m = 1:size(res.main.ModSpace,2)
    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    for n = 1:length(res.main.ppc.ppc_sub)
        scatter(n*ones(size(res.main.ppc.mod(m).sub(n).adj_correct)),res.main.ppc.mod(m).sub(n).adj_correct,...
            '.', 'CData', options.col.tnub)
        hold on
        ax = scatter(n,res.main.SPIRL(res.main.ppc.ppc_sub(n)).adj_correct, 'x',...
            'LineWidth', 2, 'CData', options.col.tnuy);
    end
    xlim([0 length(res.main.ppc.ppc_sub)+1])
    ax.Parent.XTick = 1:length(res.main.ppc.ppc_sub);
    ax.Parent.XTickLabel = {res.main.ppc.ppc_sub};
    ylim([0 1])
    ylabel('adjusted correctness (binary predictions)')
    xlabel('subject')
    legend({'$\hat{y}_{bin,pp}$', '$y_{bin}$'}, 'Interpreter', 'Latex')
    title(['model ', num2str(m)])

    figdir = fullfile(saveDir, 'figures', 'main', 'ppc', 'bin',...
        ['adj_correctness_', num2str(length(res.main.ppc.ppc_sub)), 'sub_',...
        num2str(options.main.ppc.nDraws_per_sub), 'draws_mod', num2str(m)]);
    print(figdir, '-dtiff');
    close;
end

%% MAIN (PPC indiv binary): plot stay behaviour
for n = 1:length(res.main.ppc.ppc_sub)
    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    for m = 1:size(res.main.ModSpace,2)
        subplot(2,4,m)        
        k = plot(res.main.ppc.mod(m).sub(n).prev_corr', res.main.ppc.mod(m).sub(n).p_stay_all',...
            '.-', 'color', options.col.tnub, 'DisplayName', 'posterior samples');
        hold on
        txt2 = ['empirical sub ' num2str(n)];
        plot([0 1]', res.main.SPIRL(res.main.ppc.ppc_sub(n)).p_stay',...
            '.-', 'LineWidth', 2, 'color', options.col.tnuy, 'DisplayName',txt2);
        ax1 = gca;
        ax1.XTick = [0 1];
        ax1.XTickLabel = {'incorrect' 'correct'};
        xlabel('previous prediction')
        ylabel('p(stay)')
        ylim([0 1])
        xlim([-0.1 1.1])
        txt = ['model ' num2str(m)];
        title(txt)
    end
    for i = 2:size(res.main.ppc.mod(m).sub(n).p_stay_all, 1)
        set( get( get( k(i), 'Annotation'), 'LegendInformation' ), 'IconDisplayStyle', 'off' );
    end
    legend('Position', [0.81 0.25 0.03 0.07])
    figdir = fullfile(saveDir, 'figures', 'main', 'ppc', 'bin',...
        ['stay_prob_sub', num2str(res.main.ppc.ppc_sub(n)), '_', num2str(options.main.ppc.nDraws_per_sub), 'draws']);
    print(figdir, '-dtiff');
    close;
end

%% MAIN (PPC indiv binary): plot #switches
for m = 1:size(res.main.ModSpace,2)
    figure
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
    % prior samples
    scatter(zeros(size(res.sim.sub(1,m).n_switch_all)),res.sim.sub(1,m).n_switch_all,...
        '.', 'CData', options.col.grn)
    hold on
    for n = 1:length(res.main.ppc.ppc_sub)
        % posterior samples
        scatter(n*ones(size(res.main.ppc.mod(m).sub(n).n_switch_all)),res.main.ppc.mod(m).sub(n).n_switch_all,...
            '.', 'CData', options.col.tnub)
        % empirical data
        ax = scatter(n,res.main.SPIRL(res.main.ppc.ppc_sub(n)).n_switch, 'x',...
            'LineWidth', 2, 'CData', options.col.tnuy);
    end
    xlim([-1 length(res.main.ppc.ppc_sub)+1])
    ax.Parent.XTick = 0:length(res.main.ppc.ppc_sub);
    ax.Parent.XTickLabel = {'prior', res.main.ppc.ppc_sub};
    ylim([0 max(res.main.trials)])
    ylabel('# switches (binary predictions)')
    xlabel('subject')
    legend({'$\hat{y}_{bin,PriorPred}$', '$\hat{y}_{bin,PosteriorPred}$', '$y_{bin}$'}, 'Interpreter', 'Latex')
    title(['model ', num2str(m)])

    figdir = fullfile(saveDir, 'figures', 'main', 'ppc', 'bin',...
        ['number_of_switches_', num2str(length(res.main.ppc.ppc_sub)), 'sub_',...
        num2str(options.main.ppc.nDraws_per_sub), 'draws_mod', num2str(m)]);
    print(figdir, '-dtiff');
    close;
end

%% MAIN (PPC indiv cont): logRT histogram
for n = 1:length(res.main.ppc.ppc_sub)
    for m = 1%:size(res.main.ModSpace,2)
        figure
        set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
        subplot(13,8,1)
        ax1 = histogram(res.main.est(m,res.main.ppc.ppc_sub(n)).y(:,2));
        ax1.FaceColor = options.col.tnuy;
        xlim([4 8])
        txt3 = ['cont data sub ' num2str(res.main.ppc.ppc_sub(n))];
        title(txt3)
        for i = 1:options.main.ppc.nDraws_per_sub
            subplot(13,8,i+1)
            try
                ax1 = histogram(res.main.ppc.mod(m).sub(n,i).sim.y(:,2));
                ax1.FaceColor = options.col.tnub;
                xlim([4 8])
            end
        end
        figdir = fullfile(saveDir, 'figures', 'main', 'ppc', 'cont',...
            ['logRT_histogram_sub', num2str(res.main.ppc.ppc_sub(n)), '_model', num2str(m), '_', num2str(options.main.ppc.nDraws_per_sub), 'draws']);
        print(figdir, '-dtiff');
        close;
    end
end

%% print message
disp('All figures created and saved.')

end
function [] = job_runner_4_sim_setup(EULER)
% [] = job_runner_4_sim_setup(EULER)
%
% Create synthetic data for the SPIRL Task under the pilot priors.
%
% INPUT
%   EULER        binary           Binary indicator variable
%
%   OPTIONAL:
%
% OUTPUT    
%   argout       type
%
% _________________________________________________________________________
% Author: Alex Hess
%
% Copyright (C) 2023 Translational Neuromodeling Unit
%                    Institute for Biomedical Engineering
%                    University of Zurich & ETH Zurich
%
% This file is released under the terms of the GNU General Public Licence
% (GPL), version 3. You can redistribute it and/or modify it under the
% terms of the GNU General Public License as published by the Free Software
% Foundation, either version 3 of the License, or (at your option) any
% later version.
%
% This file is distributed in the hope that it will be useful, but WITHOUT
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
% more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <https://www.gnu.org/licenses/>.
% _________________________________________________________________________

%% setup path
saveDir = spirl_setup_paths(EULER);

%% load analysis specifications
options = load(fullfile(saveDir, 'results', ['options']));
rng(123, 'twister');
options.rng.settings = rng;

%% load pilot priors
pilot = load(fullfile(saveDir, 'results', 'pilots', ['pilot_priors']));

%% load model space for analysis of the main data set
main = load(fullfile(saveDir, 'results', 'main', ['model_space']));

%% create res struct
res = struct();
res.pilot = pilot;
res.main = main;

%% create synthetic data

% pre-allocate variables
sim = struct();
input = struct();

% loop over synthetic subjects & model space
for n = 1:options.sim.indiv.nS
   
    for m = 1:size(res.main.ModSpace,2)
        
        % sample free parameter values
        input.prc.transInp = res.main.ModSpace(m).prc_config.priormus;
        input.obs.transInp = res.main.ModSpace(m).obs_config.priormus;
        for j = 1:size(res.main.ModSpace(m).prc_idx,2)
            input.prc.transInp(res.main.ModSpace(m).prc_idx(j)) = ...
                normrnd(res.main.ModSpace(m).prc_config.priormus(res.main.ModSpace(m).prc_idx(j)),...
                abs(sqrt(res.main.ModSpace(m).prc_config.priorsas(res.main.ModSpace(m).prc_idx(j)))));
        end
        for k = 1:size(res.main.ModSpace(m).obs_idx,2)
            input.obs.transInp(res.main.ModSpace(m).obs_idx(k)) = ...
                normrnd(res.main.ModSpace(m).obs_config.priormus(res.main.ModSpace(m).obs_idx(k)),...
                abs(sqrt(res.main.ModSpace(m).obs_config.priorsas(res.main.ModSpace(m).obs_idx(k)))));
        end
        
        % create simulation input vectors (native space)
        c.c_prc = res.main.ModSpace(m).prc_config;
        input.prc.nativeInp = res.main.ModSpace(m).prc_config.transp_prc_fun(c, input.prc.transInp);
        c.c_obs = res.main.ModSpace(m).obs_config;
        input.obs.nativeInp = res.main.ModSpace(m).obs_config.transp_obs_fun(c, input.obs.transInp);
        
        % simulate responses
        stable = 0;
        while stable == 0
            me = [];
            try
                sim.sub(n,m).data = tapas_simModel(res.pilot.traj.u,...
                    res.main.ModSpace(m).prc,...
                    input.prc.nativeInp,...
                    res.main.ModSpace(m).obs,...
                    input.obs.nativeInp,...
                    options.rng.settings.State(options.rng.idx, 1));
                stable = 1;
            catch me
                fprintf('simulation failed for Model %1.0f, synth. Sub %1.0f \n', [m, n]);
                fprintf('Prc Param Values: \n');
                input.prc.nativeInp
                fprintf('Obs Param Values: \n');
                input.obs.nativeInp
                % re-sample prc param values
                for j = 1:size(res.main.ModSpace(m).prc_idx,2)
                    input.prc.transInp(res.main.ModSpace(m).prc_idx(j)) = ...
                        normrnd(res.main.ModSpace(m).prc_config.priormus(res.main.ModSpace(m).prc_idx(j)),...
                        abs(sqrt(res.main.ModSpace(m).prc_config.priorsas(res.main.ModSpace(m).prc_idx(j)))));
                end
                input.prc.nativeInp = res.main.ModSpace(m).prc_config.transp_prc_fun(c, input.prc.transInp);
            end
        end

        % save sim input
        sim.sub(n,m).input = input;
        
        % Update the rng state idx
        options.rng.idx = options.rng.idx+1;
        if options.rng.idx == (length(options.rng.settings.State)+1)
            options.rng.idx = 1;
        end
    end
end

% reset rng idx
options.rng.idx = 1;


%% save simulated data
save_path = fullfile(saveDir, 'results', 'sim', 'indiv', ['simulated_data']);
save(save_path, '-struct', 'sim');

disp('simulated data successfully created.')

end